package com.coycama.comerciales.ui.home.nearbyClients

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.services.GpsReceiver
import com.coycama.comerciales.services.LocationService
import com.coycama.comerciales.utils.*
import com.coycama.comerciales.utils.Constants.ACTION_START_OR_RESUME_SERVICE
import com.coycama.comerciales.utils.Constants.MAP_ZOOM
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_nearby_clients.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

class NearbyClientsFragment : Fragment(), OnMapReadyCallback, EasyPermissions.PermissionCallbacks,
    KodeinAware, NearbyClientItemListener {

    override val kodein by kodein()

    private val factory: NearbyClientsViewModelFactory by instance()
    private lateinit var viewModel: NearbyClientsViewModel

    private lateinit var listener: NearbyClientItemListener

    private lateinit var mMap: GoogleMap
    private var hasPermissions = false
    val REQUEST_CODE_LOCATION_PERMISSION = 0

    private lateinit var gpsReceiver: GpsReceiver
    private val gpsFilter = IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)

    private var nearbyClients = mutableListOf<Client>()
    private var clientsMarkers = mutableListOf<Marker>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_nearby_clients, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // toolbar setup
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_search_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Mapa"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }

        // handle permissions
        requestPermissions()

        // check GPS
        gpsReceiver = GpsReceiver()
        activity?.registerReceiver(gpsReceiver, gpsFilter)
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsReceiver.isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        // map setup
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment?
        if (gpsReceiver.isGpsEnabled) {
            mapFragment?.getMapAsync(this)
        }

        // bind viewmodel
        viewModel = ViewModelProvider(this, factory).get(NearbyClientsViewModel::class.java)
        viewModel.getLoggedInUser().observe(viewLifecycleOwner, {
            viewModel.activeUser = it
            bindUI()
        })
        listener = this

    }

    private fun bindUI() = Coroutines.main {
        nearby_clients_list_progressbar.show()
        try {
            viewModel.clients.await().observe(viewLifecycleOwner, {
                nearbyClients = it.toMutableList()
                initRecyclerView(it.toClientItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        nearby_clients_list_progressbar.hide()
        addClientsMarkersToMap()
    }

    private fun initRecyclerView(clientItems: List<NearbyClientItem>) {
        val clientAdapter = GroupieAdapter().apply {
            addAll(clientItems)
        }
        nearby_clients_list_recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = clientAdapter
        }

    }

    private fun List<Client>.toClientItem(): List<NearbyClientItem> {
        return this.map {
            NearbyClientItem(it, listener)
        }
    }

    private suspend fun addClientsMarkersToMap() {
        for (client in nearbyClients) {
            val location by lazyDeferred {
                LocationService.getLocationFromAddress(
                    requireContext(),
                    client.Direccion + ", " + client.Provincia + ", " + client.CodPos
                )
            }
            location.await()?.let {
                client.latLgn = it
                val marker = mMap.addMarker(
                    MarkerOptions()
                        .position(it)
                        .title(client.NomComercial)
                        .icon(BitmapDescriptorFactory.defaultMarker())
                )
                clientsMarkers.add(marker)
            }
        }
    }

    /**
     * Manipulates the map once available.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (hasPermissions) {
            mMap.isMyLocationEnabled = true
        }
        sendCommandToService(ACTION_START_OR_RESUME_SERVICE)
        subscribeToLocationObs()
    }

    private fun subscribeToLocationObs() {
        LocationService.lastLocation.observe(viewLifecycleOwner, {
            moveToCurrentLocation(it)
        })
    }

    private fun moveToCurrentLocation(latLng: LatLng) {
        mMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM)
        )
    }

    // needed for foreground service
    private fun sendCommandToService(action: String) {
        Intent(requireContext(), LocationService::class.java).also {
            it.action = action
            requireContext().startService(it)
        }
    }

    private fun requestPermissions() {
        if (PermissionsUtility.hasLocationPermissions(requireContext())) {
            hasPermissions = true
            return
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            EasyPermissions.requestPermissions(
                this,
                "Para utilizar el mapa, necesitas dar permisso de localización",
                REQUEST_CODE_LOCATION_PERMISSION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        } else {
            EasyPermissions.requestPermissions(
                this,
                "Para utilizar el mapa, necesitas dar permisso de localización",
                REQUEST_CODE_LOCATION_PERMISSION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        hasPermissions = true
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            requestPermissions()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onInitRouteClicked(client: Client) {
        TODO("Not yet implemented")
    }

    override fun onClientNameClicked(client: Client) {
        mMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(client.latLgn, MAP_ZOOM)
        )
        clientsMarkers[nearbyClients.indexOf(client)].showInfoWindow()
0    }

    override fun onResume() {
        activity?.registerReceiver(gpsReceiver, gpsFilter)
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        activity?.unregisterReceiver(gpsReceiver)
    }
}