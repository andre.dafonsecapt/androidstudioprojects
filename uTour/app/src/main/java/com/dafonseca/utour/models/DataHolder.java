package com.dafonseca.utour.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataHolder {

    private static final DataHolder holder = new DataHolder();
    private String email;
    private int idUser;
    private boolean isAdmin;

    public static DataHolder getInstance() {
        return holder;
    }
}
