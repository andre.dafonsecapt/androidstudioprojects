package com.coycama.comerciales.ui.home.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.coycama.comerciales.R
import com.coycama.comerciales.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.activity_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ProfileFragment : Fragment(), ProfileListener, KodeinAware {

    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance()
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        viewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        viewModel.profileListener = this
        return binding.root
    }

    override fun onClientsClicked() {
        findNavController().navigate(R.id.profilePageToClientsListPage)
    }

    override fun onNearbyClientsClicked() {
        findNavController().navigate(R.id.profilePageToNearbyClientsPage)
    }

    override fun onResume() {
        activity?.home_toolbar_title?.text = ""
        activity?.home_logo_imageview?.visibility = View.VISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.VISIBLE
        super.onResume()
    }

}