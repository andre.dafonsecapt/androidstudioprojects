package com.coycama.comerciales.ui.home.clients

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.home.profile.ProfileViewModel
import com.coycama.comerciales.ui.login.LoginViewModel

@Suppress("UNCHECKED_CAST")
class ClientsViewModelFactory (
    private val userRepository : UserRepository,
    private val clientsRepository: ClientsRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClientsViewModel(clientsRepository, userRepository) as T
    }
}