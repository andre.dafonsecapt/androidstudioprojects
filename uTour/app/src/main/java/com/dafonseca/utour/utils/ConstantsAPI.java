package com.dafonseca.utour.utils;

public class ConstantsAPI {

    public static final String URL_USER = "api/user";
    public static final String URL_LOGIN = "api/login";
    public static final String URL_MAIL = "api/mail";
    public static final String URL_PROFILE = "api/profile";
    public static final String URL_STATS = "api/stats";
    public static final String URL_PROFILE_USER = "api/profile/{idUser}";
    public static final String URL_TOURS = "api/tours";
    public static final String URL_TOURS_USER = "api/tours/user/{idUser}";
    public static final String URL_STOPS = "api/stops";
    public static final String URL_FEEDBACK = "api/feedback";
    public static final String URL_FEEDBACK_TOUR = "api/feedback/tour/{idTour}";
    public static final String BASE_URL = "http://10.0.2.2:8080/uTour/";
//    http://dam2.tomcat.iesquevedo.es/uTour/
//   "http://10.0.2.2:8080/uTour/"
}
