package com.dafonseca.pratica5.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.pratica5.R;
import com.dafonseca.pratica5.models.Usuario;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RegistroActivity extends AppCompatActivity {

    private EditText etUsuarios;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuarios = findViewById(R.id.etUsuarios);

        List<Usuario> usuarios = getUsuariosFromXML();
        StringBuffer listUsuarios = new StringBuffer();
        for (Usuario u : usuarios){
            listUsuarios.append(u).append('\n');
        }
        etUsuarios.setText(listUsuarios);

    }


    private List<Usuario> getUsuariosFromXML() {
        List<Usuario> usuarios = new ArrayList<>();
        try {
            // create list of users from XML using XMLPullParser
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            InputStream is = openFileInput("xmlFile.xml");
            parser.setInput(is, null);

            Usuario usuarioActivo = null;
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() == XmlPullParser.START_TAG) {
                    switch (parser.getName()){
                        case "usuario" :
                            usuarioActivo = new Usuario();
                            usuarios.add(usuarioActivo);
                            break;
                        case "nombre":
                            usuarioActivo.setNombre(parser.nextText());
                            break;
                        case "visitas":
                            usuarioActivo.setVisitas(Integer.parseInt(parser.nextText()));
                            break;
                        case "tiempoUso":
                            usuarioActivo.setTiempoUso(parser.nextText());
                            break;
                    }
                }
                parser.next();
            }
            is.close();
        } catch (Exception e){
            System.out.println(e);
        }
        return usuarios;
    }

    public void volver(View view){
        finish();
    }
}
