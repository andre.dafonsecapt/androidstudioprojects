package com.coycama.comerciales.ui.home.visits

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Visit
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_visits_list.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class VisitsListFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val factory: VisitsListViewModelFactory by instance()
    private lateinit var viewModel: VisitsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_visits_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(VisitsListViewModel::class.java)
        viewModel.getLoggedInUser().observe(viewLifecycleOwner, {
            viewModel.activeUser = it
            bindUI()
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Visitas"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }
    }

    private fun bindUI() = Coroutines.main {
        visits_list_progressbar.show()
        try {
            viewModel.visits.await().observe(viewLifecycleOwner, {
                initRecyclerView(it.toVisitItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        visits_list_progressbar.hide()
    }

    private fun initRecyclerView(visitItems: List<VisitItem>) {
        val visitsAdapter = GroupieAdapter().apply {
            addAll(visitItems)
        }
        visits_list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = visitsAdapter
        }
    }


    private fun List<Visit>.toVisitItem(): List<VisitItem> {
        return this.map {
            VisitItem(it)
        }
    }

}