package com.dafonseca.utour.utils;

public class ConstantsClient {

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_IMAGE_GALLERY = 2;
    public static final int CAMERA_PERMISSION_CODE = 51;
    public static final int CREATE_TOUR_CODE = 444;
    public static final int CREATE_STOP_CODE = 64;
    public static final int UPDATE_STOP_CODE = 4236;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    public static final int REQUEST_RECORD_AUDIO_PERMISSION = 568;
    public static final float DEFAULT_ZOOM = 15f;
    public static final String AUDIO_RECORDING_PATH = "/stopAudio.3gp";
}
