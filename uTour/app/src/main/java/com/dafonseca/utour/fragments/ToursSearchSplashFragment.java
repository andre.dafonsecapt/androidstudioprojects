package com.dafonseca.utour.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dafonseca.utour.R;
import com.dafonseca.utour.activities.ToursDetailsActivity;
import com.dafonseca.utour.adapters.TourAdapter;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.TourServices;
import com.dafonseca.utour.viewmodels.MainViewModel;

import java.util.ArrayList;
import java.util.List;

import fj.data.Either;
import lombok.SneakyThrows;

import static android.app.Activity.RESULT_OK;
import static com.dafonseca.utour.utils.ConstantsClient.CREATE_TOUR_CODE;

public class ToursSearchSplashFragment extends Fragment implements TourAdapter.ItemClickListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private TourAdapter tourAdapter;
    private TourServices tourServices;
    private RecyclerView recyclerView;
    private List<Tour> tours;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tours_search_splash, container, false);

        setViews(rootView);
        setTitle();
        initObjects();
        setRecyclerAdapter(rootView);
        setListeners();
        launchGetToursTask();

        return rootView;
    }

    private void setViews(View rootView) {
        swipeRefreshLayout = rootView.findViewById(R.id.tours_all_swipe_refresh);
        recyclerView = rootView.findViewById(R.id.tours_all_splash_recycler);
    }

    private void setTitle() {
        MainViewModel viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        viewModel.getAppTitle().setValue(getString(R.string.title_tours_search));
    }

    private void initObjects() {
        tourServices = new TourServices();
        tours = new ArrayList<>();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
    }

    private void setRecyclerAdapter(View rootView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        tourAdapter = new TourAdapter(tours, getContext());
        tourAdapter.setClickListener(this);
        recyclerView.setAdapter(tourAdapter);
    }

    private void setListeners() {
        swipeRefreshLayout.setOnRefreshListener(this::launchGetToursTask);
    }

    private void launchGetToursTask() {
        GetToursTask task = new GetToursTask();
        task.execute();
    }

    @Override
    public void onItemClick(View view, int position) {
        Tour tourSelected = tourAdapter.getItem(position);
        Intent i = new Intent(getContext(), ToursDetailsActivity.class);
        i.putExtra("tourSelected", tourSelected);
        startActivity(i);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CREATE_TOUR_CODE) {
            if (data != null) {
                launchGetToursTask();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class GetToursTask extends AsyncTask<Void, Void, Either<ApiError, List<Tour>>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Tour>> doInBackground(Void... voids) {
            return tourServices.getAllTours(DataHolder.getInstance().getIdUser());
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Tour>> result) {
            swipeRefreshLayout.setRefreshing(false);
            if (result.isRight()) {
                tours = result.right().value();
                tourAdapter.addItems(tours);
                if (DataHolder.getInstance().isAdmin()) {
                    tourAdapter.setLongClickable();
                    tourAdapter.initTourServices();
                }
            } else {
                Toast.makeText(getContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
