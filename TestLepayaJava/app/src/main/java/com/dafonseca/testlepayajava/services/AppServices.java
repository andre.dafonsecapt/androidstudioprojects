package com.dafonseca.testlepayajava.services;

import android.content.Context;

import com.dafonseca.testlepayajava.dao.AppDAO;
import com.dafonseca.testlepayajava.models.Trainer;

import java.util.List;

public class AppServices {

    private AppDAO appDAO;

    public AppServices() {
        appDAO = new AppDAO();
    }

    public List<Trainer> getTrainersFromJSON(Context context){
        return appDAO.getTrainersFromJSON(context);
    }
}
