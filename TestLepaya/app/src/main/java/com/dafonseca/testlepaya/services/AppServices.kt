package com.dafonseca.testlepaya.services

import android.content.Context
import com.dafonseca.testlepaya.dao.AppDAO
import com.dafonseca.testlepaya.models.Trainer

class AppServices {
    private val appDAO: AppDAO

    fun getTrainersFromJSON(context: Context): List<Trainer> {
        return appDAO.getTrainersFromJSON(context)
    }

    fun getTrainersFromLocalJSON(context: Context): List<Trainer> {
        return appDAO.getTrainersFromLocalJSON(context)
    }

    fun writeToLocalFile(context: Context, stringToWrite: String){
        appDAO.writeToLocalFile(context, stringToWrite)
    }

    init {
        appDAO = AppDAO()
    }
}