package com.coycama.comerciales.data.models

import android.os.Parcelable
import android.view.View
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Client(
    val Cliente: String,
    val CodPos: String,
    val Comercial: String,
    val Direccion: String,
    val FechaUltimaIncidencia: String,
    val FechaUltimoEnvio: String,
    val FechaUltimoPedido: String,
    val NomComercial: String,
    val NomFiscal: String,
    val PagoPendiente: String,
    val Poblacion: String,
    val Provincia: String,
    val SubCliente: String,
    val TieneUltimaIncidencia: String,
    val TieneUltimoEnvio: String,
    val TieneUltimoPedido: String,
    val UltimaIncidencia: String,
    val UltimoEnvio: String,
    val UltimoPedido: String,
    val ultimaVisita: Visit?,
    var showOptions: Int = View.INVISIBLE,
    var latLgn : LatLng
) : Parcelable