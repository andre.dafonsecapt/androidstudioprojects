package com.dafonseca.utour.services;

import com.dafonseca.utour.dao.UserDAO;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.UserGetDTO;
import com.dafonseca.utour.models.UserPostDTO;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import retrofit2.Call;
import retrofit2.Response;

public class UserServices {

    private final UserDAO dao;

    public UserServices() {
        this.dao = new UserDAO();
    }


    public Either<ApiError, String> login(UserPostDTO user) throws IOException {
        Call<String> call = dao.login(user);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> resetPwd(String email) throws IOException {
        Call<String> call = dao.resetPwd(email);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> changePwd(UserPostDTO user) throws IOException {
        Call<String> call = dao.changePwd(user);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, List<UserGetDTO>> getUsers() throws IOException {
        Call<List<UserGetDTO>> call = dao.getUsers();
        Response<List<UserGetDTO>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addUser(UserPostDTO user) throws IOException {
        Call<String> call = dao.addUser(user);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateUser(UserGetDTO user) throws IOException {
        Call<String> call = dao.updateUser(user);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> deleteUser(int idUser) throws IOException {
        Call<String> call = dao.deleteUser(idUser);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }
}
