package com.coycama.comerciales.ui.home.nearbyClients

import com.coycama.comerciales.data.models.Client

interface NearbyClientItemListener {

    fun onInitRouteClicked(client: Client)
    fun onClientNameClicked(client: Client)
}