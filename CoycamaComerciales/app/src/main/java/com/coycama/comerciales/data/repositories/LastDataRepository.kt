package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.LastData
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByClientSubClientRequest
import com.coycama.comerciales.utils.Constants.LAST_INCIDENTS_TABLE_NAME
import com.coycama.comerciales.utils.Constants.LAST_ORDERS_TABLE_NAME
import com.coycama.comerciales.utils.Constants.LAST_SHIPMENTS_TABLE_NAME
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LastDataRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val lastOrders = MutableLiveData<List<LastData>>()
    private val lastShipments = MutableLiveData<List<LastData>>()
    private val lastIncidents = MutableLiveData<List<LastData>>()

    suspend fun getLastDataByClientSubClient(
        clientSubClient: GetByClientSubClientRequest,
        dataTable: String
    ): LiveData<List<LastData>> {
        var liveData = MutableLiveData<List<LastData>>()
        when (dataTable) {
            LAST_ORDERS_TABLE_NAME -> {
                liveData =  withContext(Dispatchers.IO) {
                    fetchLastDataByClientSubClient(clientSubClient, dataTable)
                    lastOrders
                }
            }
            LAST_SHIPMENTS_TABLE_NAME -> {
                liveData = withContext(Dispatchers.IO) {
                    fetchLastDataByClientSubClient(clientSubClient, dataTable)
                    lastShipments
                }
            }
            LAST_INCIDENTS_TABLE_NAME -> {
                liveData = withContext(Dispatchers.IO) {
                    fetchLastDataByClientSubClient(clientSubClient, dataTable)
                    lastIncidents
                }
            }
        }
        return liveData
    }

    private suspend fun fetchLastDataByClientSubClient(
        clientSubClient: GetByClientSubClientRequest,
        dataTable: String
    ) {
        when (dataTable) {
            LAST_ORDERS_TABLE_NAME -> {
                val response = apiRequest { api.getLastOrdersByClientSubClient(clientSubClient) }
                lastOrders.postValue(response.records)
            }
            LAST_SHIPMENTS_TABLE_NAME -> {
                val response = apiRequest { api.getLastShipmentsByClientSubClient(clientSubClient) }
                lastShipments.postValue(response.records)
            }
            LAST_INCIDENTS_TABLE_NAME -> {
                val response = apiRequest { api.getLastIncidentsByClientSubClient(clientSubClient) }
                lastIncidents.postValue(response.records)
            }
        }
    }
}