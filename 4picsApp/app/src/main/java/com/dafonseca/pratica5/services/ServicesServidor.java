package com.dafonseca.pratica5.services;

import com.dafonseca.pratica5.dao.DaoServidor;
import com.dafonseca.pratica5.models.Puntuacion;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class ServicesServidor {

    private DaoServidor daoServidor;

    public ServicesServidor() {
        daoServidor = new DaoServidor();
    }

    public List<Puntuacion> recuperarPuntuaciones() throws IOException {

        Response<List<Puntuacion>> resp = daoServidor.recuperarPuntuaciones().execute();
        if (resp.isSuccessful()) {
            return resp.body();
        } else {
            return null;
        }
    }

    public String guardarPuntuaciones(List<Puntuacion> puntuaciones) throws IOException {

        Response<String> resp = daoServidor.guardarPuntuaciones(puntuaciones).execute();
        if (resp.isSuccessful()) {
            return resp.body();
        } else {
            return null;
        }
    }
}
