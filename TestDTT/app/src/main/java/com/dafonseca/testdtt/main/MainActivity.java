package com.dafonseca.testdtt.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.dafonseca.testdtt.R;
import com.dafonseca.testdtt.map.MapActivity;
import com.dafonseca.testdtt.services.AppServices;

public class MainActivity extends AppServices implements MainContract.View {

    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter = new MainPresenter(this);
        setMainToolbar();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.menuPrivacy) {
            privacyBtnAction(null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void mainBtnAction(View view) {

        mainPresenter.handleMainButton();
    }

    public void privacyBtnAction(View view) {

        mainPresenter.handlePrivacyInfoButton();
    }


    /////       View Methods       //////
    @Override
    public void showMapScreen() {

        Intent i = new Intent(this, MapActivity.class);
        startActivity(i);
    }

    @Override
    public void showPrivacyAlert() {

        // set SpannableString to use in alert dialog. set a clickable span on the word 'privacybeleid' so it starts a action view intent
        SpannableString ss = new SpannableString(getString(R.string.privacy_message));
        ss.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View v) {
                String url = "http://rsr.nl/index.php?page=privacy-wetgeving";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        }, 38, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        // create alert dialog
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        alertDialogBuilder.setTitle(getString(R.string.privacybeleid));
        alertDialogBuilder.setMessage(ss);
        alertDialogBuilder.setPositiveButton(getString(R.string.bevestig),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        closeOptionsMenu();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        TextView tvAlertDialogMessage = alertDialog.findViewById(android.R.id.message);
        if (tvAlertDialogMessage != null) {
            tvAlertDialogMessage.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }
}
