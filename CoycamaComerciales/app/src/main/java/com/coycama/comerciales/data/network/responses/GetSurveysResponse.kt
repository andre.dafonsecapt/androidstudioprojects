package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.Survey

data class GetSurveysResponse(
    val code: Int,
    val records: List<Survey>
)