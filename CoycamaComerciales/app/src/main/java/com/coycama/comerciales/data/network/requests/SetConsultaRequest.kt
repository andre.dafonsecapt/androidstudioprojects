package com.coycama.comerciales.data.network.requests

import com.google.gson.annotations.SerializedName

data class SetConsultaRequest(
    @SerializedName("Comercial") var comercial: Int,
    @SerializedName("Cliente") var client: Int,
    @SerializedName("SubCliente") var subclient: Int
)
