package com.dafonseca.praticafragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class FragmentoRadioGroup extends Fragment {

    private View rootView;
    private RadioGroup rg;
    private TextView tvMessage;

    public static FragmentoRadioGroup newInstance() {
        return new FragmentoRadioGroup();
    }

    /*La interfaz del fragmento*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragmento_fragmentoradiogroup, container, false);

        rg = rootView.findViewById(R.id.radioGroup);
        tvMessage = rootView.findViewById(R.id.tvMessage);

        rg.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group,
                                                 int checkedId) {
                        switch (rg.getCheckedRadioButtonId()) {
                            case R.id.iosB:
                                tvMessage.setText(R.string.iosMessage);
                                break;
                            case R.id.androidB:
                                tvMessage.setText(R.string.androidMessage);
                                break;
                            case R.id.windowsB:
                                tvMessage.setText(R.string.windowsMessage);
                                break;
                            case R.id.symbianB:
                                tvMessage.setText(R.string.symbianMessage);
                                break;
                            case R.id.otroB:
                                tvMessage.setText(R.string.otroMessage);
                                break;
                        }
                    }
                }
        );

        return rootView;
    }
}
