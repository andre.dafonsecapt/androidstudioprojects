package com.coycama.comerciales.data.models

import org.threeten.bp.LocalDate


data class PendingPayment(
    val Cliente: String,
    val Factura: String,
    var FechaFac: String,
    var FechaVto: String,
    val Importe: String,
    val SubCliente: String
)