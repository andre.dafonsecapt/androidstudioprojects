package com.dafonseca.testdtt.models;

public class MyAddress {

    private String firstLine;
    private String locality;
    private String postalCode;
    private String adminArea;
    private String subAdminArea;
    private String countryName;

    public MyAddress() {
    }

    public void setFirstLine(String firstLine) {
        this.firstLine = firstLine;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setAdminArea(String adminArea) {
        this.adminArea = adminArea;
    }

    public void setSubAdminArea(String subAdminArea) {
        this.subAdminArea = subAdminArea;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        if (!firstLine.equals("null")) {
            sb.append(firstLine);
        }
        if (!locality.equals("null")) {
            sb.append(locality).append(", ");
        }
        if (!postalCode.equals("null")) {
            sb.append(postalCode).append(", ");
        }
        if (!adminArea.equals("null")) {
            sb.append(adminArea).append(", ");
        }
        if (!subAdminArea.equals("null")) {
            sb.append(subAdminArea).append(", ");
        }
        if (!countryName.equals("null")) {
            sb.append(countryName);
        }
        return sb.toString();
    }
}
