package com.dafonseca.ciclodevidaejercicio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate ( Bundle savedInstanceState ) {
        super . onCreate ( savedInstanceState ) ;
        setContentView (R. layout . activity_main ) ;
        android.util.Log.i(TAG, " onCreate " ) ;
    }
    protected void onStart ( ) {
        super . onStart ( ) ;
        android.util.Log.i(TAG, " onStart " ) ;
    }
    protected void onRestart ( ) {
        super.onRestart();
        android.util.Log.i(TAG, " onRestart ");
    }
    protected void onResume( ) {
        super . onResume ( ) ;
        android.util.Log.i(TAG, " onResume " ) ;
    }
    protected void onPause ( ) {
        super . onPause ( ) ;
        android.util.Log.i(TAG, " onPause " ) ;
    }
    protected void onStop ( ) {
        super . onStop ( ) ;
        android.util.Log.i(TAG, " onStop " ) ;
    }
    protected void onDestroy ( ) {
        super . onDestroy ( ) ;
        android.util.Log.i(TAG, " onDestroy " ) ;
    }
    private static final String TAG = "CicloDeVida " ;
}
