package com.coycama.comerciales.ui.home.clients

import android.util.Log
import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import com.coycama.comerciales.data.network.requests.SetConsultaRequest
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.coycama.comerciales.utils.lazyDeferred

class ClientsViewModel(
    private val clientsRepository: ClientsRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    var activeUser: User? = null

    fun getLoggedInUser() = userRepository.getUser()

    val clients by lazyDeferred {
        val request = GetByComercialRequest(0)
        if (activeUser != null) {
            request.comercial = activeUser!!.id!!
        }
        clientsRepository.getClientsByComercial(request)
    }

    fun setConsulta(client: Client) {
        val consultaRequest = SetConsultaRequest(
            comercial = activeUser?.id!!,
            client = client.Cliente.toInt(),
            subclient = client.SubCliente.toInt()
        )
        Coroutines.main {
            try {
                clientsRepository.setConsulta(consultaRequest)
            } catch (e: ApiException) {
                Log.e("API ERROR", e.message!!)
            } catch (e: NoInternetException) {
                Log.e("API ERROR", e.message!!)
            }
        }
    }
}