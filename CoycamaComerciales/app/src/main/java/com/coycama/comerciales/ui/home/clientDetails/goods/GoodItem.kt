package com.coycama.comerciales.ui.home.clientDetails.goods

import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Good
import com.coycama.comerciales.data.models.Sale
import com.coycama.comerciales.databinding.ItemGoodBinding
import com.coycama.comerciales.databinding.ItemSaleBinding
import com.coycama.comerciales.myFramework.ktExtensions.round
import com.xwray.groupie.viewbinding.BindableItem

class GoodItem (
    private val good: Good
        ) : BindableItem<ItemGoodBinding>() {

    override fun bind(viewBinding: ItemGoodBinding, position: Int) {
        val valueDbl = good.Facturacion.toDouble().round(3)
        good.Facturacion = valueDbl.toString()
        viewBinding.good = good
    }

    override fun getLayout() = R.layout.item_good

    override fun initializeViewBinding(view: View): ItemGoodBinding {
        return ItemGoodBinding.bind(view)
    }


}