package com.dafonseca.testdtt.adapters;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.dafonseca.testdtt.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final Activity context;

    public CustomInfoWindowAdapter(Activity activity) {
        this.context = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = context.getLayoutInflater().inflate(R.layout.custom_info_contents, null);

        TextView tvTitle = view.findViewById(R.id.tvWinInfoTitle);
        TextView tvSubTitle = view.findViewById(R.id.tvWinInfoSubtitle);

        tvTitle.setText(marker.getTitle());
        tvSubTitle.setText(marker.getSnippet());

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }
}
