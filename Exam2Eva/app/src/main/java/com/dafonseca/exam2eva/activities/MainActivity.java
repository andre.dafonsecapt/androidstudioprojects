package com.dafonseca.exam2eva.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dafonseca.exam2eva.R;
import com.dafonseca.exam2eva.services.ServicesApp;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private ServicesApp sa = new ServicesApp();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void pressBtn1(View view){
        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
    }

    public void pressBtn2(View view){
        Intent i = new Intent(this, ThirdActivity.class);
        startActivity(i);
    }

    public void pressBtn3(View view){
        TaskGuardarXML taskGuardarXml = new TaskGuardarXML();
        taskGuardarXml.execute(this);
    }

    public void pressBtn4(View view){
        String response;
        if (sa.restaurarSociosFromXML(this)){
            response = getString(R.string.responseOKRestaurar);
        } else {
            response = getString(R.string.responseNORestaurar);
        }
        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
    }

    class TaskGuardarXML extends AsyncTask<Activity, Void, String> {


        @Override
        protected String doInBackground(Activity... activities) {

            String response;
            if (sa.writeSociosToXML(activities[0])){
                response = getString(R.string.responseOKguardarXML);
            } else {
                response = getString(R.string.responseNOguardarXML);
            }
            return response;
        }

        @Override
        protected void onPostExecute(String resultado) {

            Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();
        }
    }
}
