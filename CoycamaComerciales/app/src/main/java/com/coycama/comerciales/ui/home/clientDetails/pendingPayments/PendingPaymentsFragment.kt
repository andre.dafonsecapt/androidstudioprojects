package com.coycama.comerciales.ui.home.clientDetails.pendingPayments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.PendingPayment
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_client_goods.*
import kotlinx.android.synthetic.main.fragment_pending_payments.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class PendingPaymentsFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val factory: PendingPaymentsViewModelFactory by instance()
    private lateinit var viewModel: PendingPaymentsViewModel

    private val args by navArgs<PendingPaymentsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pending_payments, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(PendingPaymentsViewModel::class.java)
        viewModel.clientId = args.client.Cliente.toInt()
        bindUI()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Pagos Pendientes"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }
    }

    private fun bindUI() = Coroutines.main {
        pending_payments_progressbar.show()
        try {
            viewModel.payments.await().observe(viewLifecycleOwner, {
                initRecyclerView(it.toPendingPaymentItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        pending_payments_progressbar.hide()
    }

    private fun initRecyclerView(paymentsItems: List<PendingPaymentItem>) {
        val paymentsAdapter = GroupieAdapter().apply {
            addAll(paymentsItems)
        }
        pending_payments_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = paymentsAdapter
        }
    }


    private fun List<PendingPayment>.toPendingPaymentItem(): List<PendingPaymentItem> {
        return this.map {
            PendingPaymentItem(it)
        }
    }

}