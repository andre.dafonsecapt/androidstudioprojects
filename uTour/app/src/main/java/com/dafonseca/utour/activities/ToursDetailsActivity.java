package com.dafonseca.utour.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.adapters.FeedbackAdapter;
import com.dafonseca.utour.adapters.StopAdapter;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Feedback;
import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.FeedbackServices;
import com.dafonseca.utour.services.StopServices;
import com.dafonseca.utour.utils.ImageTools;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import lombok.SneakyThrows;

import static com.dafonseca.utour.utils.ConstantsClient.DEFAULT_ZOOM;

public class ToursDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, StopAdapter.ItemClickListener {

    private ScrollView scrollView;
    private TextInputEditText etName;
    private TextInputEditText etDescription;
    private TextInputEditText etCity;
    private TextInputEditText etDuration;
    private TextView tvTourDetailsTitle;
    private CardView cardViewStops;
    private CardView cardViewFeeback;
    private List<Marker> markers;
    private StopAdapter adapterStops;
    private FeedbackAdapter adapterFeedback;
    private ProgressBar progressBarStops;
    private ProgressBar progressBarFeeback;
    private StopServices stopServices;
    private FeedbackServices feedbackServices;
    private Tour tour;
    private ImageTools imageTools;
    private GoogleMap googleMap;
    private FrameLayout transparentMapFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tours_details);
        setViews();
        initObjects();
        setListeners();
        setFields();
        initRecyclerViews();
        executeTasks();
    }

    private void setViews() {
        scrollView = findViewById(R.id.tours_details_scrollview);
        tvTourDetailsTitle = findViewById(R.id.tours_details_title);
        transparentMapFrame = findViewById(R.id.tours_details_map_frame);
        etName = findViewById(R.id.tours_details_name);
        etDescription = findViewById(R.id.tours_details_description);
        etCity = findViewById(R.id.tours_details_city);
        etDuration = findViewById(R.id.tours_details_duration);
        cardViewStops = findViewById(R.id.tours_details_stops_cardview);
        progressBarStops = findViewById(R.id.tours_details_stops_progbar);
        cardViewFeeback = findViewById(R.id.tours_details_feedback_cardview);
        progressBarFeeback = findViewById(R.id.tours_details_feeback_progbar);
    }

    private void initObjects() {
        stopServices = new StopServices();
        feedbackServices = new FeedbackServices();
        markers = new ArrayList<>();
        adapterStops = new StopAdapter(new ArrayList<>(), this);
        adapterFeedback = new FeedbackAdapter();
        imageTools = new ImageTools();
    }

    private void setListeners() {
        transparentMapFrame.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ScrollView to intercept touch events.
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    // Disable touch on transparent view
                    return false;

                case MotionEvent.ACTION_UP:
                    // Allow ScrollView to intercept touch events.
                    scrollView.requestDisallowInterceptTouchEvent(false);
                    return true;

                case MotionEvent.ACTION_MOVE:
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    return false;

                default:
                    return true;
            }
        });
    }

    private void setFields() {
        Tour tourSelected = getIntent().getParcelableExtra("tourSelected");
        if (tourSelected != null) {
            this.tour = tourSelected;
            tvTourDetailsTitle.setText(tourSelected.getTourname());
            etName.setText(tourSelected.getTourname());
            etDescription.setText(tourSelected.getDescription());
            etCity.setText(tourSelected.getCity());
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                Date dt = sdf.parse(String.valueOf(tourSelected.getDuration()));
                sdf = new SimpleDateFormat("HH:mm");
                String durationString = sdf.format(dt);
                durationString += " (hh:mm)";
                etDuration.setText(durationString);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    private void initRecyclerViews() {
        // stops recycler view
        LinearLayoutManager stopsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView stopsRecyclerView = findViewById(R.id.tours_details_stops_recyclerview);
        stopsRecyclerView.setLayoutManager(stopsLayoutManager);
        stopsRecyclerView.setAdapter(adapterStops);
        adapterStops.setClickListener(this);

        // feedback recycler view
        LinearLayoutManager feedbackLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView feedbackRecyclerView = findViewById(R.id.tours_details_feedback_recyclerview);
        feedbackRecyclerView.setLayoutManager(feedbackLayoutManager);
        feedbackRecyclerView.setAdapter(adapterFeedback);
    }

    private void executeTasks() {
        GetStopsTask getStopsTask = new GetStopsTask();
        getStopsTask.execute();
        GetFeedbackTask getFeedbackTask = new GetFeedbackTask();
        getFeedbackTask.execute();
    }

    /////       GoogleMaps Methods       /////

    private void initializeMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.tours_details_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        setMarkers();
    }

    private void setMarkers() {
        for (Stop s : adapterStops.getData()) {
            LatLng latLng = new LatLng(s.getLatitude(), s.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(s.getStopname())
                    .icon(BitmapDescriptorFactory.fromBitmap(imageTools.getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.map_marker), 200)));
            Marker marker = googleMap.addMarker(markerOptions);
            markers.add(marker);
        }
        // calculate the bounds of all the markers
        if (markers.size() > 1) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int padding = 200; // offset from edges of the map in pixels
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
        } else {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(markers.get(0).getPosition(), DEFAULT_ZOOM));
        }
    }

    public void onTakeTourBtnClick(View view) {
        Intent i = new Intent(this, ToursOnTourActivity.class);
        i.putExtra("tourSelected", tour);
        startActivity(i);
    }

    @Override
    public void onItemClick(View view, int position) {
        Stop stopSelected = adapterStops.getItem(position);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(stopSelected.getLatitude(), stopSelected.getLongitude()), DEFAULT_ZOOM));
        markers.get(position).showInfoWindow();
    }

    /////       AsyncTasks Methods       /////

    @SuppressLint("StaticFieldLeak")
    class GetStopsTask extends AsyncTask<Void, Void, Either<ApiError, List<Stop>>> {

        @Override
        protected void onPreExecute() {
            progressBarStops.setVisibility(View.VISIBLE);
            cardViewStops.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Stop>> doInBackground(Void... voids) {
            return stopServices.getStopsByIdTour(tour.getIdTour());
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Stop>> result) {
            progressBarStops.setVisibility(View.INVISIBLE);
            cardViewStops.setAlpha(1f);
            if (result.isRight()) {
                adapterStops.setItems(result.right().value());
                initializeMap();
            } else {
                Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class GetFeedbackTask extends AsyncTask<Void, Void, Either<ApiError, List<Feedback>>> {

        @Override
        protected void onPreExecute() {
            progressBarFeeback.setVisibility(View.VISIBLE);
            cardViewFeeback.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Feedback>> doInBackground(Void... voids) {
            return feedbackServices.getFeedbackByIdTour(tour.getIdTour());
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Feedback>> result) {
            progressBarFeeback.setVisibility(View.INVISIBLE);
            cardViewFeeback.setAlpha(1f);
            if (result.isRight()) {
                adapterFeedback.setItems(result.right().value());
            } else {
                Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
