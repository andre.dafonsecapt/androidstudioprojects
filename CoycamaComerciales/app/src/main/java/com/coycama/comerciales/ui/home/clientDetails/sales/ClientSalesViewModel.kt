package com.coycama.comerciales.ui.home.clientDetails.sales

import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import com.coycama.comerciales.data.repositories.SalesRepository
import com.coycama.comerciales.utils.lazyDeferred

class ClientSalesViewModel(
    private val salesRepository: SalesRepository
) : ViewModel() {

    var clientId = 0

    val sales by lazyDeferred {
        salesRepository.getSalesByClient(GetByClientRequest(clientId))
    }
}