package com.dafonseca.utour.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.utour.R;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.UserPostDTO;
import com.dafonseca.utour.services.UserServices;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fj.data.Either;
import lombok.SneakyThrows;

public class LoginActivity extends AppCompatActivity {

    private ScrollView scrollView;
    private TextInputEditText etEmail;
    private TextInputEditText etPwd;
    private TextView tvWrongCred;
    private Button btnLogin;
    private ProgressBar progressBar;
    private UserServices userServices;
    private UserPostDTO userPostDTO;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setViews();
        setListeners();
        userServices = new UserServices();
    }

    private void setViews() {
        scrollView = findViewById(R.id.login_scrollview);
        etEmail = findViewById(R.id.login_email);
        etPwd = findViewById(R.id.login_pwd);
        tvWrongCred = findViewById(R.id.login_wrong_cred);
        tvWrongCred.setVisibility(View.GONE);
        btnLogin = findViewById(R.id.login_btn);
        progressBar = findViewById(R.id.login_progbar);
    }

    private void setListeners() {
        setFocusListener(etEmail);
        setFocusListener(etPwd);
        etPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // empty method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvWrongCred.setVisibility(View.GONE);
                btnLogin.setEnabled(!Objects.requireNonNull(etEmail.getText()).toString().isEmpty() && !Objects.requireNonNull(etPwd.getText()).toString().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // empty method
            }
        });
    }

    private void setFocusListener(EditText etText) {
        etText.setOnFocusChangeListener((v, hasFocus) -> btnLogin.setEnabled(!Objects.requireNonNull(etEmail.getText()).toString().isEmpty() && !Objects.requireNonNull(etPwd.getText()).toString().isEmpty()));
    }

    public void onForgotPasswordClick(View view) {
        // password patter
        final EditText etEmailView = new EditText(this);
        final Pattern validEmailRegex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        etEmailView.setHint("example@hotmail.com");
        // alert dialog builder
        new AlertDialog.Builder(this)
                .setTitle(R.string.forgot_pwd_alert_title)
                .setMessage(R.string.forgot_pwd_alert_msg)
                .setView(etEmailView)
                .setPositiveButton(R.string.forgot_pwd_alert_send, (dialog, whichButton) -> {
                    String emailStr = etEmailView.getText().toString();
                    Matcher matcher = validEmailRegex.matcher(emailStr);
                    if (matcher.find()) {
                        ResetPwdTask task = new ResetPwdTask();
                        task.execute(emailStr);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.forgot_pwd_alert_invalidemail_msg, Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Cancel", (dialog, whichButton) -> dialog.dismiss())
                .show();
    }

    public void onLoginBtnClick(View view) {
        // hide keyboard as button is pressed
        ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(etPwd.getWindowToken(), 0);
        // create user to POST
        userPostDTO = new UserPostDTO();
        userPostDTO.setEmail(Objects.requireNonNull(etEmail.getText()).toString());
        userPostDTO.setPassword(Objects.requireNonNull(etPwd.getText()).toString());
        //launch task
        LoginTask task = new LoginTask();
        task.execute();
    }

    @SuppressLint("StaticFieldLeak")
    class ResetPwdTask extends AsyncTask<String, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.5f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(String... strings) {
            return userServices.resetPwd(strings[0]);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class LoginTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            tvWrongCred.setVisibility(View.GONE);
            scrollView.setAlpha(0.5f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return userServices.login(userPostDTO);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            if (result != null && result.isRight()) {
                Toast.makeText(getApplicationContext(), "Logged in with success!", Toast.LENGTH_SHORT).show();
                // parse result String into idUser and to check if user is admin with admin = 1
                String resultString = result.right().value().replace("\"", "");
                String[] resultStringArray = resultString.split(":");
                int idUser = Integer.parseInt(resultStringArray[0]);
                int isAdmin = Integer.parseInt(resultStringArray[1]);
                // set values in dataholder for user Id and email so that they can be used throughout the app
                DataHolder.getInstance().setIdUser(idUser);
                DataHolder.getInstance().setEmail(userPostDTO.getEmail());
                DataHolder.getInstance().setAdmin(isAdmin == 1);
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            } else {
                if (result != null) {
                    tvWrongCred.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}
