package com.coycama.comerciales.ui.register

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.R
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.databinding.ActivityRegisterBinding
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class RegisterActivity : AppCompatActivity(), RegisterListener, KodeinAware {

    override val kodein by kodein()

    // lifecycle elements
    private val factory: RegisterViewModelFactory by instance()
    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bindViewModel()

        transformToolbar()
    }

    private fun bindViewModel() {
        val binding: ActivityRegisterBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_register
        )
        viewModel = ViewModelProvider(this, factory).get(RegisterViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.registerListener = this
    }

    private fun transformToolbar(){
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        register_progressBar.show()
    }

    override fun onSuccess(user: User) {
        register_progressBar.hide()
        Intent(this, HomeActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(it)
        }
    }

    override fun onFailure(message: String) {
        register_progressBar.hide()
        toast(message)
    }
}