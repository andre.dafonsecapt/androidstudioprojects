package com.dafonseca.pratica3.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class SendViewModel extends ViewModel {

    private MutableLiveData<String> destino;
    private MutableLiveData<String> asunto;
    private MutableLiveData<String> mensaje;

    public SendViewModel() {
        destino = new MutableLiveData<>();
        asunto = new MutableLiveData<>();
        mensaje = new MutableLiveData<>();
    }

    public LiveData<String> getDestinoText() {
        return destino;
    }

    public LiveData<String> getAsuntoText() {
        return asunto;
    }

    public LiveData<String> getMensajeText() {
        return mensaje;
    }
}
