package com.dafonseca.testlepayajava.dao;

import android.content.Context;

import com.dafonseca.testlepayajava.models.Trainer;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AppDAO {

    public List<Trainer> getTrainersFromJSON(Context context){

        String trainersJsonString = readJSON(context);

        Gson gson = new Gson();

        Trainer[] trainers = gson.fromJson(trainersJsonString, Trainer[].class);

        return Arrays.asList(trainers);
    }


    private String readJSON(Context context){
        String json = null;
        try {
            InputStream is = context.getAssets().open("trainers.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return json;
    }

}
