package com.coycama.comerciales.ui.home.clientDetails.surveys.list

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Survey
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.ui.home.clientDetails.pendingPayments.PendingPaymentsFragmentArgs
import com.coycama.comerciales.ui.home.clientDetails.sales.ClientSalesFragmentDirections
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_client_sales.*
import kotlinx.android.synthetic.main.fragment_client_sales.view.*
import kotlinx.android.synthetic.main.fragment_client_survey_list.*
import kotlinx.android.synthetic.main.fragment_client_survey_list.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ClientSurveysListFragment : Fragment(), KodeinAware, ClientSurveysListListener {

    override val kodein by kodein()

    private val factory: ClientSurveysListViewModelFactory by instance()
    private lateinit var viewModel: ClientSurveysListViewModel

    private lateinit var listener: ClientSurveysListListener

    private val args by navArgs<ClientSurveysListFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_client_survey_list, container, false)
        setListeners(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(ClientSurveysListViewModel::class.java)
        viewModel.clientId = args.client.Cliente.toInt()
        bindUI()

        listener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Ficha Cliente"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }
    }

    private fun bindUI() = Coroutines.main {
        client_survey_list_card_title.text = args.client.NomComercial
        client_survey_list_progressbar.show()
        try {
            viewModel.surveys.await().observe(viewLifecycleOwner, {
                initRecyclerView(it.toSurveyItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        client_survey_list_progressbar.hide()
    }

    private fun initRecyclerView(surveyItems: List<SurveyItem>) {
        val surveyAdapter = GroupieAdapter().apply {
            addAll(surveyItems)
        }
        client_survey_list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = surveyAdapter
        }
    }


    private fun List<Survey>.toSurveyItem(): List<SurveyItem> {
        return this.map {
            SurveyItem(it)
        }
    }

    private fun setListeners(view: View) {
        view.client_survey_list_details_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientSurveysListFragmentDirections.clientSurveyListPageToClientDetailsPage(
                    args.client
                )
            )
        }

        view.client_survey_list_goods_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientSurveysListFragmentDirections.clientSurveyListPageToClientGoodsPage(
                    args.client
                )
            )
        }

        view.client_survey_list_sales_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientSurveysListFragmentDirections.clientSurveyListPageToClientSalesPage(
                    args.client
                )
            )
        }
    }

    override fun onSurveyClicked() {
        // implement custom dialog with survey info
    }

}