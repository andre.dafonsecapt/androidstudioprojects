package com.dafonseca.utour.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.utour.R;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.UserPostDTO;
import com.dafonseca.utour.services.UserServices;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import fj.data.Either;
import lombok.SneakyThrows;

public class RegistrationActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etEmail;
    private TextInputEditText etPwd;
    private TextInputEditText etConfPWd;
    private TextView tvWrongPwd;
    private Button btnReg;
    private ProgressBar progressBar;
    private UserServices userServices;
    private UserPostDTO userPostDTO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setViews();
        setListeners();
        userServices = new UserServices();
    }

    private void setViews() {
        ImageView imgReg = findViewById(R.id.reg_img);
        imgReg.setBackgroundResource(R.drawable.reg_banner);
        etName = findViewById(R.id.reg_name);
        etEmail = findViewById(R.id.reg_email);
        etPwd = findViewById(R.id.reg_pwd);
        etConfPWd = findViewById(R.id.reg_confirmpwd);
        tvWrongPwd = findViewById(R.id.reg_wrong_pwd);
        tvWrongPwd.setVisibility(View.GONE);
        btnReg = findViewById(R.id.reg_btn);
        progressBar = findViewById(R.id.reg_progbar);
    }

    private void setListeners() {
        etName.addTextChangedListener(new FormTextWatcher());
        etEmail.addTextChangedListener(new FormTextWatcher());
        etPwd.addTextChangedListener(new FormTextWatcher());
        etConfPWd.addTextChangedListener(new FormTextWatcher());
    }

    public void onRegisterBtnClick(View view) {
        ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(etConfPWd.getWindowToken(), 0);
        userPostDTO = new UserPostDTO();
        userPostDTO.setUsername(etName.getText().toString());
        userPostDTO.setEmail(etEmail.getText().toString());
        userPostDTO.setPassword(Objects.requireNonNull(etPwd.getText()).toString());
        RegisterUserTask task = new RegisterUserTask();
        task.execute();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.email_register_msg)
                .setPositiveButton(R.string.msg_ok, (dialog, id) -> finish());
        builder.create();
        builder.show();
    }

    @SuppressLint("StaticFieldLeak")
    class RegisterUserTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return userServices.addUser(userPostDTO);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            if (result.isRight()) {
                Toast.makeText(getApplicationContext(), "User Registered with success!", Toast.LENGTH_LONG).show();
                showAlertDialog();
            } else {
                Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    // custom class that implements TextWatcher to check if form is valid everytime the text changes
    class FormTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            btnReg.setEnabled(!etName.getText().toString().isEmpty()
                    && !etEmail.getText().toString().isEmpty()
                    && !Objects.requireNonNull(etPwd.getText()).toString().isEmpty()
                    && !Objects.requireNonNull(etConfPWd.getText()).toString().isEmpty() && isValidPwd());

            if (!Objects.requireNonNull(etPwd.getText()).toString().isEmpty() && !Objects.requireNonNull(etConfPWd.getText()).toString().isEmpty()) {
                if (isValidPwd()) {
                    tvWrongPwd.setVisibility(View.GONE);
                    etPwd.setBackgroundResource(R.drawable.auth_et_background);
                    etConfPWd.setBackgroundResource(R.drawable.auth_et_background);
                } else {
                    etPwd.setBackgroundResource(R.drawable.auth_et_error_background);
                    etConfPWd.setBackgroundResource(R.drawable.auth_et_error_background);
                    tvWrongPwd.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }

        // check if both password are correct and a minimum length of 6
        private boolean isValidPwd() {
            return Objects.requireNonNull(etPwd.getText()).toString().equals(Objects.requireNonNull(etConfPWd.getText()).toString()) && etPwd.getText().toString().length() >= 6;
        }
    }
}
