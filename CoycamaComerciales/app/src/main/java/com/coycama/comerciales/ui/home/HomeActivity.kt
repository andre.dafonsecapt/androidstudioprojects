package com.coycama.comerciales.ui.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.coycama.comerciales.R
import com.coycama.comerciales.ui.home.profile.ProfileFragmentDirections
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(home_toolbar)

        navController = Navigation.findNavController(this, R.id.home_fragment)
        nav_bot_view.setupWithNavController(navController)
        nav_bot_view.background = null
        nav_bot_view.menu.getItem(1).isEnabled = false

        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.home_fragment),
            home_drawer_layout
        )
    }

    fun onHomeBtnClicked(view: View) {
        val navHostFragment: NavHostFragment = supportFragmentManager.findFragmentById(R.id.home_fragment) as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        val graph = inflater.inflate(R.navigation.nav_graph)
        graph.startDestination = R.id.profilePage
        navController.graph = graph
    }

    fun onPersonIconClicked(view: View){
        Navigation.findNavController(this, R.id.home_fragment).navigate(ProfileFragmentDirections.profilePageToVisitsListPage())
    }
}