package com.dafonseca.utour.alertDialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dafonseca.utour.R;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Feedback;
import com.dafonseca.utour.services.FeedbackServices;
import com.dafonseca.utour.services.ProfileServices;

import fj.data.Either;
import lombok.SneakyThrows;

public class CustomFeedbackAlertDialog extends Dialog implements android.view.View.OnClickListener {

    private final Activity activity;
    private RelativeLayout relativeLayout;
    private RatingBar ratingBar;
    private EditText etOpinion;
    private Button btnCancel;
    private Button btnSubmit;
    private ProgressBar progressBar;
    private FeedbackServices feedbackServices;
    private ProfileServices profileServices;
    private final Feedback feedback;
    private final int idTour;

    public CustomFeedbackAlertDialog(Activity activity, int idTour) {
        super(activity);
        this.activity = activity;
        this.idTour = idTour;
        this.feedback = new Feedback();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_feedback_alert_dialog);
        setViews();
        initObjects();
        setListeners();
    }

    private void setViews() {
        relativeLayout = findViewById(R.id.custom_feedback_alert_layout);
        ratingBar = findViewById(R.id.custom_feedback_alert_ratingBar);
        etOpinion = findViewById(R.id.custom_feedback_alert_opinion);
        btnCancel = findViewById(R.id.custom_feedback_alert_cancel_btn);
        btnSubmit = findViewById(R.id.custom_feedback_alert_submit_btn);
        progressBar = findViewById(R.id.custom_feedback_alert_progbar);
    }

    private void initObjects() {
        feedbackServices = new FeedbackServices();
        profileServices = new ProfileServices();
    }

    private void setListeners() {
        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.custom_feedback_alert_cancel_btn:
                dismiss();
                break;
            case R.id.custom_feedback_alert_submit_btn:
                feedback.setRating((int) ratingBar.getRating());
                feedback.setOpinion(etOpinion.getText().toString());
                feedback.setFk_idTour(idTour);
                SubmitFeedbackTask task = new SubmitFeedbackTask();
                task.execute();
                break;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class SubmitFeedbackTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            relativeLayout.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return feedbackServices.addFeedback(feedback);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            relativeLayout.setAlpha(1f);
            String resultString;
            if (result.isRight()) {
                IncrementToursRatedTask addToursRated = new IncrementToursRatedTask();
                addToursRated.execute();
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(activity, resultString, Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class IncrementToursRatedTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // empty method
        }

        @SneakyThrows
        @Override
        protected Void doInBackground(Void... voids) {
            return profileServices.incrementToursRated(DataHolder.getInstance().getIdUser());
        }

        @Override
        protected void onPostExecute(Void result) {
            // empty method
        }
    }
}
