package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.Sale
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SalesRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val sales = MutableLiveData<List<Sale>>()

    suspend fun getSalesByClient(clientId: GetByClientRequest) : LiveData<List<Sale>> {
        return withContext(Dispatchers.IO){
            fetchSalesByClient(clientId)
            sales
        }
    }

    private suspend fun fetchSalesByClient(clientId: GetByClientRequest){
        val response = apiRequest { api.getSalesByClient(clientId) }
        sales.postValue(response.records)
    }
}