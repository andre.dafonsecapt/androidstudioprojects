package com.dafonseca.exam2eva.models;

public class Socio {

    private String nombre;
    private String tipoCuota;
    private String alergias;
    private String telefono;

    public Socio() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoCuota() {
        return tipoCuota;
    }

    public void setTipoCuota(String tipoCuota) {
        this.tipoCuota = tipoCuota;
    }

    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Nombre: ").append(nombre);
        sb.append(" - TipoCuota: ").append(tipoCuota);
        sb.append(" - Alergias: ").append(alergias);
        sb.append(" - Telefono:").append(telefono);
        return sb.toString();
    }
}
