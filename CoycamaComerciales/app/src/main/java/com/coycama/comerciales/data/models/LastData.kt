package com.coycama.comerciales.data.models

data class LastData(
    val Cdad: String,
    val Cliente: String,
    val Codigo: String,
    val Descripcion: String,
    val Fecha: String,
    val Importe: String,
    val Linea: String,
    val MayorImporte: String,
    val NetoUnitario: String,
    val Pedido: String?,
    val SubCliente: String
)