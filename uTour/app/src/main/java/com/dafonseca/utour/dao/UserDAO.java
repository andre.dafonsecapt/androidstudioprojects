package com.dafonseca.utour.dao;

import com.dafonseca.utour.models.UserGetDTO;
import com.dafonseca.utour.models.UserPostDTO;
import com.dafonseca.utour.retrofit.UserAPI;
import com.dafonseca.utour.singleton.RetrofitSingleton;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

public class UserDAO implements UserAPI {

    private final UserAPI userAPI;
    private final Gson gson;

    public UserDAO() {
        userAPI = RetrofitSingleton.getInstance().getRetrofit().create(UserAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<String> login(UserPostDTO user) {
        return userAPI.login(user);
    }

    @Override
    public Call<String> resetPwd(String email) {
        return userAPI.resetPwd(email);
    }

    @Override
    public Call<String> changePwd(UserPostDTO user) {
        return userAPI.changePwd(user);
    }

    @Override
    public Call<List<UserGetDTO>> getUsers() {
        return userAPI.getUsers();
    }

    @Override
    public Call<String> addUser(UserPostDTO user) {
        return userAPI.addUser(user);
    }

    @Override
    public Call<String> updateUser(UserGetDTO user) {
        return userAPI.updateUser(user);
    }

    @Override
    public Call<String> deleteUser(int idUser) {
        return userAPI.deleteUser(idUser);
    }

}
