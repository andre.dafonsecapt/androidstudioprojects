package com.coycama.comerciales.ui.home.clientDetails.sales

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Sale
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.ui.home.clientDetails.ClientDetailsFragmentDirections
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_client_sales.*
import kotlinx.android.synthetic.main.fragment_client_sales.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ClientSalesFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val factory: ClientSalesViewModelFactory by instance()
    private lateinit var viewModel: ClientSalesViewModel

    private val args by navArgs<ClientSalesFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_client_sales, container, false)
        setListeners(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(ClientSalesViewModel::class.java)
        viewModel.clientId = args.client.Cliente.toInt()
        bindUI()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Ficha Cliente"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }
    }

    private fun setListeners(view: View) {
        view.client_sales_details_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientSalesFragmentDirections.clientSalesPageToClientDetailsPage(
                    args.client
                )
            )
        }

        view.client_sales_surveys_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientSalesFragmentDirections.clientSalesPageToClientSurveyListPage(
                    args.client
                )
            )
        }

        view.client_sales_goods_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientSalesFragmentDirections.clientSalesPageToClientGoodsPage(
                    args.client
                )
            )
        }
    }

    private fun bindUI() = Coroutines.main {
        client_sales_progressbar.show()
        try {
            viewModel.sales.await().observe(viewLifecycleOwner, {
                initRecyclerView(it.toSaleItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        client_sales_progressbar.hide()
    }

    private fun initRecyclerView(saleItems: List<SaleItem>) {
        val salesAdapter = GroupieAdapter().apply {
            addAll(saleItems)
        }
        client_sales_recycler_view.apply {
            layoutManager = GridLayoutManager(context, 3)
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL))
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = salesAdapter
        }
    }


    private fun List<Sale>.toSaleItem(): List<SaleItem> {
        return this.map {
            SaleItem(it)
        }
    }

}