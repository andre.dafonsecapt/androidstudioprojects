package com.dafonseca.utour.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.activities.ToursEditActivity;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.BitmapCache;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.TourServices;
import com.dafonseca.utour.utils.ImageTools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import lombok.SneakyThrows;

public class TourAdapter extends RecyclerView.Adapter<TourAdapter.ViewHolder> {

    private List<Tour> data;
    private ItemClickListener clickListener;
    private final ImageTools imageTools;
    private TourServices tourServices;
    private final Context context;

    public TourAdapter(List<Tour> data, Context context) {
        this.data = data;
        imageTools = new ImageTools();
        this.context = context;
    }

    public void initTourServices() {
        tourServices = new TourServices();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_tours, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (data.get(position).getImage() != null && !data.get(position).getImage().isEmpty()) {
            holder.image.setImageBitmap(imageTools.convertEncodedStringToBitmap(data.get(position).getImage()));
        } else {
            holder.image.setImageDrawable(context.getDrawable(R.drawable.img_placeholder));
        }
        holder.name.setText(data.get(position).getTourname());
        holder.city.setText(data.get(position).getCity());
        holder.rating.setRating(data.get(position).getRating());
        String duration = "Duration: ";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("mm");
            Date dt = sdf.parse(String.valueOf(data.get(position).getDuration()));
            sdf = new SimpleDateFormat("HH:mm");
            duration += sdf.format(dt);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        holder.duration.setText(duration);
        if (data.get(position).isLongClickable()) {
            holder.setLongClickListener();
        }
        boolean isExpanded = data.get(position).isExpanded();
        if (isExpanded) {
            holder.expandableLayout.setVisibility(View.VISIBLE);
        } else {
            holder.expandableLayout.setVisibility(View.GONE);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public void setItems(List<Tour> data) {
        this.data = data;
    }

    public Tour getItem(int position) {
        return data.get(position);
    }

    private int getItemIdTour(int position) {
        return data.get(position).getIdTour();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Tour> getData() {
        return data;
    }

    public void addItems(List<Tour> tours) {
        this.data = tours;
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    public void setLongClickable() {
        for (Tour t : data) {
            t.setLongClickable(true);
        }
        notifyDataSetChanged();
    }

    private void expandView(int position) {
        Tour tour = data.get(position);
        tour.setExpanded(!tour.isExpanded());
        notifyItemChanged(position);
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final CardView cardView;
        private final ImageView image;
        private final TextView name;
        private final TextView city;
        private final RatingBar rating;
        private final TextView duration;
        private final LinearLayout expandableLayout;
        private final ImageButton btnEdit;
        private final ImageButton btnDelete;
        private final Context context;
        private final ProgressBar progressBar;

        ViewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            cardView = itemView.findViewById(R.id.row_tours_cardview);
            image = itemView.findViewById(R.id.row_tours_image);
            name = itemView.findViewById(R.id.row_tours_name);
            city = itemView.findViewById(R.id.row_tours_city);
            rating = itemView.findViewById(R.id.row_tours_ratingBar);
            duration = itemView.findViewById(R.id.row_tours_duration);
            expandableLayout = itemView.findViewById(R.id.row_tours_expandableLayout);
            btnEdit = itemView.findViewById(R.id.row_tours_btn_edit);
            btnDelete = itemView.findViewById(R.id.row_tours_btn_delete);
            progressBar = itemView.findViewById(R.id.row_tours_progbar);
            itemView.setOnClickListener(itemView1 -> {
                if (clickListener != null)
                    clickListener.onItemClick(itemView1,
                            getAdapterPosition());
            });
        }

        private void setLongClickListener() {
            itemView.setOnLongClickListener(v -> {
                expandView(getAdapterPosition());
                return true;
            });
            btnEdit.setOnClickListener(v -> {
                Intent i = new Intent(v.getContext(), ToursEditActivity.class);
                Tour tourSelected = getItem(getAdapterPosition());
                i.putExtra("tourSelected", tourSelected);
                BitmapCache.getInstance().getLru().put("tourImage", ((BitmapDrawable) image.getDrawable()).getBitmap());
                itemView.getContext().startActivity(i);
            });
            btnDelete.setOnClickListener(v -> new AlertDialog.Builder(v.getContext())
                    .setMessage(v.getContext().getString(R.string.row_tours_delete_tour_msg))
                    .setPositiveButton(v.getContext().getString(R.string.btn_yes),
                            (arg0, arg1) -> {
                                DeleteTourTask task = new DeleteTourTask();
                                task.execute();
                            })
                    .setNegativeButton(v.getContext().getString(R.string.btn_cancel),
                            (arg0, arg1) -> arg0.cancel())
                    .show());
        }

        @SuppressLint("StaticFieldLeak")
        class DeleteTourTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                cardView.setAlpha(0.5f);
            }

            @SneakyThrows
            @Override
            protected Either<ApiError, String> doInBackground(Void... voids) {
                return tourServices.deleteTour(getItemIdTour(getAdapterPosition()));
            }

            @Override
            protected void onPostExecute(Either<ApiError, String> result) {
                progressBar.setVisibility(View.INVISIBLE);
                cardView.setAlpha(1f);
                String resultString;
                if (result.isRight()) {
                    resultString = result.right().value();
                    removeItem(getAdapterPosition());
                } else {
                    resultString = result.left().value().getMessage();
                }
                Toast.makeText(context, resultString, Toast.LENGTH_LONG).show();
            }
        }
    }
}
