package com.coycama.comerciales.MVVMApplication

import android.app.Application
import com.coycama.comerciales.data.db.AppDatabase
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.NetworkConnectionInterceptor
import com.coycama.comerciales.data.repositories.*
import com.coycama.comerciales.ui.forgotPassword.ForgotPasswordViewModelFactory
import com.coycama.comerciales.ui.home.clientDetails.ClientDetailsViewModelFactory
import com.coycama.comerciales.ui.home.clientDetails.goods.ClientGoodsViewModelFactory
import com.coycama.comerciales.ui.home.clientDetails.pendingPayments.PendingPaymentsViewModelFactory
import com.coycama.comerciales.ui.home.clientDetails.sales.ClientSalesViewModelFactory
import com.coycama.comerciales.ui.home.clientDetails.surveys.list.ClientSurveysListViewModelFactory
import com.coycama.comerciales.ui.home.clients.ClientsViewModelFactory
import com.coycama.comerciales.ui.home.nearbyClients.NearbyClientsViewModelFactory
import com.coycama.comerciales.ui.home.profile.ProfileViewModelFactory
import com.coycama.comerciales.ui.home.visits.VisitsListViewModelFactory
import com.coycama.comerciales.ui.login.LoginViewModelFactory
import com.coycama.comerciales.ui.register.RegisterViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

// class used for dependency injection, it binds and manages the instantiations of classes to be used in other classes
class MVVMApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MVVMApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { UserRepository(instance(), instance()) }
        bind() from singleton { ClientsRepository(instance()) }
        bind() from singleton { ComercialRepository(instance()) }
        bind() from singleton { SalesRepository(instance()) }
        bind() from singleton { GoodsRepository(instance()) }
        bind() from singleton { PaymentsRepository(instance()) }
        bind() from singleton { SurveysRepository(instance()) }
        bind() from singleton { LastDataRepository(instance()) }
        bind() from provider { LoginViewModelFactory(instance()) }
        bind() from provider { RegisterViewModelFactory(instance()) }
        bind() from provider { ForgotPasswordViewModelFactory(instance()) }
        bind() from provider { ProfileViewModelFactory(instance()) }
        bind() from provider { ClientsViewModelFactory(instance(), instance()) }
        bind() from provider { NearbyClientsViewModelFactory(instance(), instance()) }
        bind() from provider { ClientDetailsViewModelFactory(instance()) }
        bind() from provider { VisitsListViewModelFactory(instance(), instance()) }
        bind() from provider { ClientSalesViewModelFactory(instance()) }
        bind() from provider { ClientGoodsViewModelFactory(instance()) }
        bind() from provider { PendingPaymentsViewModelFactory(instance()) }
        bind() from provider { ClientSurveysListViewModelFactory(instance()) }

    }
}