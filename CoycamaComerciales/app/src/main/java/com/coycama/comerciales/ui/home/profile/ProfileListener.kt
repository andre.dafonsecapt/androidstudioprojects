package com.coycama.comerciales.ui.home.profile

interface ProfileListener {

    fun onClientsClicked()
    fun onNearbyClientsClicked()
}