package com.coycama.comerciales.ui.home.clientDetails.surveys.list

interface ClientSurveysListListener {

    fun onSurveyClicked()
}