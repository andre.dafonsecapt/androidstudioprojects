package com.dafonseca.utour.singleton;

import com.dafonseca.utour.utils.ConstantsAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitSingleton {

    private static RetrofitSingleton retrofitConnection = null;
    private final Retrofit retrofit;
    private final Gson gson;

    private RetrofitSingleton() {
        gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
//                .registerTypeAdapter(Tour.class, new TourSerializer())
//                .registerTypeAdapter(Tour.class, new TourDeserializer())
                .create();
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        OkHttpClient clientOK = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(ConstantsAPI.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create()) // For parsing JSONP or String
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(clientOK)
                .build();
    }

    public static RetrofitSingleton getInstance() {
        if (retrofitConnection == null) {
            retrofitConnection = new RetrofitSingleton();
        }
        return retrofitConnection;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public Gson getGson() {
        return gson;
    }
}
