package com.coycama.comerciales.ui.home.nearbyClients

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.UserRepository

@Suppress("UNCHECKED_CAST")
class NearbyClientsViewModelFactory(
    private val userRepository: UserRepository,
    private val clientsRepository: ClientsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearbyClientsViewModel(clientsRepository, userRepository) as T
    }
}