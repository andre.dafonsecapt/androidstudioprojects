package com.dafonseca.testdtt.map;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.dafonseca.testdtt.R;
import com.dafonseca.testdtt.adapters.CustomInfoWindowAdapter;
import com.dafonseca.testdtt.services.AppServices;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppServices implements OnMapReadyCallback, MapContract.View {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final int REQUEST_PHONE_CALL = 1;

    //vars
    private NetworkReceiver networkReceiver;
    private GPSReceiver gpsReceiver;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mapPresenter = new MapPresenter(this);
        networkReceiver = new NetworkReceiver();
        gpsReceiver = new GPSReceiver();
        locationPermissionGranted = false;
        activityStarted = false;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        setMapToolbar();

        // need to check if gps is activated when activity is created
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mapPresenter.handleListeners(getString(R.string.gps));
        } else {
            locationPermissionGranted = checkPermissions();
            if (locationPermissionGranted) {
                initializeMap();
            }
        }
    }

    /////       Toolbar Methods       /////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.menuPrivacy);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    /////       GoogleMaps Methods       /////

    private void initializeMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (locationPermissionGranted && (alertDialog == null || !alertDialog.isShowing())) {
            getDeviceLocation();
        }
    }

    /////       Permissions Methods       /////
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        locationPermissionGranted = false;

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        locationPermissionGranted = false;
                        return;
                    }
                }
                locationPermissionGranted = true;
                //initialize our map
                initializeMap();
            }
        }
    }

    /////       Buttons Methods       //////
    public void dialogBtnAction(View view) {

        mapPresenter.handleCustomDialogCallButton();
    }

    public void startIntentCall(View view) {
        mapPresenter.handleCallButton();
    }

    /////       View Methods       //////
    @Override
    public void showAlert(final String propertyToActivate) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        alertDialogBuilder.setTitle(propertyToActivate + " " + getString(R.string.alert_dialog_title));
        alertDialogBuilder.setMessage(getString(R.string.alert_dialog_part1) + " " + propertyToActivate + " " + getString(R.string.alert_dialog_part2));
        alertDialogBuilder.setPositiveButton(getString(R.string.aanzetten),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (propertyToActivate.equals(getString(R.string.gps))) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        } else {
                            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    }
                });
        alertDialogBuilder.setNegativeButton(getString(R.string.annuleren),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void showCustomDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_alert_dialog);
        Button closeBtn = dialog.findViewById(R.id.annulerenBtn);
        // if button is clicked, close the custom dialog
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button callBtn = dialog.findViewById(R.id.callBtn);
        // if button is clicked, check for phone call permission and then call the service number
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapPresenter.handleCallButton();
            }
        });
        dialog.show();

        // set windows attributes for custom alert dialog - transparency and bottom gravity
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams wlp = window.getAttributes();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            wlp.gravity = Gravity.BOTTOM;
            window.setAttributes(wlp);
        }
    }

    @Override
    public void showCallIntent() {
        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+319007788990"));
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
        } else {
            startActivity(intentCall);
        }
    }

    @Override
    public void showInfoWindow(MarkerOptions myMarker, LatLng latLng, float DEFAULT_ZOOM) {

        //Set Custom InfoWindow Adapter
        CustomInfoWindowAdapter adapter = new CustomInfoWindowAdapter(this);
        mMap.setInfoWindowAdapter(adapter);
        // Add Marker to map and show window info
        marker = mMap.addMarker(myMarker);
        marker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
    }

    /////       LifeCycle Methods       //////
    @Override
    protected void onResume() {
        //register broadcast receivers to detect any changes made
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(gpsReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        super.onResume();
    }

    @Override
    protected void onStop() {
        // unregister broadcast receivers to save battery and not activate app
        unregisterReceiver(networkReceiver);
        unregisterReceiver(gpsReceiver);
        locationManager.removeUpdates(this);
        super.onStop();
    }

    /////       Broadcast Listeners Classes       //////

    // receiver detects changes to network
    public class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {

                boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                if (noConnectivity) {
                    mapPresenter.handleListeners(getString(R.string.netwerk));
                } else {
                    if (alertDialog != null && alertDialog.isShowing() && activityStarted) {
                        alertDialog.cancel();
                    }
                    locationPermissionGranted = checkPermissions();
                    if (locationPermissionGranted) {
                        initializeMap();
                    }
                }
            }
        }
    }

    // receiver detects changes to gps
    public class GPSReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {

                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    if (alertDialog == null || !alertDialog.isShowing()) {
                        mapPresenter.handleListeners(getString(R.string.gps));
                    }
                } else {
                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.cancel();
                    }
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            locationPermissionGranted = checkPermissions();
                            if (locationPermissionGranted) {
                                initializeMap();
                            }
                        }
                    }, 1500); // wait one second before getting location
                }
            }
        }
    }
}
