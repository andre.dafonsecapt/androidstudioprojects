package com.coycama.comerciales.ui.home.nearbyClients

import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.utils.lazyDeferred

class NearbyClientsViewModel(
    private val clientsRepository: ClientsRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    var activeUser: User? = null

    fun getLoggedInUser() = userRepository.getUser()

    val clients by lazyDeferred {
        val request = GetByComercialRequest(0)
        if (activeUser != null) {
            request.comercial = activeUser!!.id!!
            clientsRepository.getClientsByComercial(request)
        } else {
            clientsRepository.getClientsByComercial(request)
        }
    }
}