package com.coycama.comerciales.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

const val CURRENT_USER_ID = 0


@Entity
data class User(

    var id : Int? = null,
    var name: String? = null,
    var email : String? = null,

){
    @PrimaryKey(autoGenerate = false)
    var uid: Int = CURRENT_USER_ID
}