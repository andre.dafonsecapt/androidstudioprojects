package com.coycama.comerciales.data.repositories

import com.coycama.comerciales.data.db.AppDatabase
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.responses.AuthResponse

class UserRepository(
    private val api: MyApi,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun checkEmail(email: String): AuthResponse {
        return apiRequest { api.checkEmail(email) }
    }

    suspend fun userLogin(email: String, pwd: String): AuthResponse {
        return apiRequest { api.userLogin(email, pwd) }
    }

    suspend fun userRegister(name: String, email: String, pwd: String): AuthResponse {
        return apiRequest { api.userRegister(name, email, pwd) }
    }

    suspend fun saveUser(user: User) = db.getUserDao().insert(user)

    fun getUser() = db.getUserDao().getUser()

    suspend fun userForgotPassword(email: String): AuthResponse {
        return apiRequest { api.userForgotPassword(email) }
    }

}