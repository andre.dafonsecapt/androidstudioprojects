package com.dafonseca.pratica3.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.dafonseca.pratica3.R;

public class FragmentImagen extends Fragment {

    private ImageView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_imagen, container, false);

        image = rootView.findViewById(R.id.imageView);

        return rootView;
    }

    public void showImage(String image, String dificultad) {
        switch (image+dificultad) {
            case "nivel1facil":
                this.image.setImageResource(R.drawable.nivel1facil);
                break;
            case "nivel2facil":
                this.image.setImageResource(R.drawable.nivel2facil);
                break;
            case "nivel3facil":
                this.image.setImageResource(R.drawable.nivel3facil);
                break;
            case "nivel1medio":
                this.image.setImageResource(R.drawable.nivel1medio);
                break;
            case "nivel2medio":
                this.image.setImageResource(R.drawable.nivel2medio);
                break;
            case "nivel3medio":
                this.image.setImageResource(R.drawable.nivel3medio);
                break;
            case "nivel1dificil":
                this.image.setImageResource(R.drawable.nivel1dificil);
                break;
            case "nivel2dificil":
                this.image.setImageResource(R.drawable.nivel2dificil);
                break;
            case "nivel3dificil":
                this.image.setImageResource(R.drawable.nivel3dificil);
                break;
        }
    }
}
