package com.dafonseca.utour.services;

import com.dafonseca.utour.dao.FeedbackDAO;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Feedback;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import retrofit2.Call;
import retrofit2.Response;

public class FeedbackServices {

    private final FeedbackDAO dao;

    public FeedbackServices() {
        this.dao = new FeedbackDAO();
    }

    public Either<ApiError, List<Feedback>> getFeedbacks() throws IOException {
        Call<List<Feedback>> call = dao.getFeedbacks();
        Response<List<Feedback>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, List<Feedback>> getFeedbackByIdTour(int idTour) throws IOException {
        Call<List<Feedback>> call = dao.getFeedbackByIdTour(idTour);
        Response<List<Feedback>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addFeedback(Feedback feedback) throws IOException {
        Call<String> call = dao.addFeedback(feedback);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateFeedback(Feedback feedback) throws IOException {
        Call<String> call = dao.updateFeedback(feedback);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> deleteFeedback(int idFeedback) throws IOException {
        Call<String> call = dao.deleteFeedback(idFeedback);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }
}
