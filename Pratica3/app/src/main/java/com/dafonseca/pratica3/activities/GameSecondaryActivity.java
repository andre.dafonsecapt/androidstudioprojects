package com.dafonseca.pratica3.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.pratica3.R;
import com.dafonseca.pratica3.fragments.FragmentImagen;

public class GameSecondaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamesecondary);

        FragmentImagen fragmentImagen = (FragmentImagen) getSupportFragmentManager().findFragmentById(R.id.fragmentImagen);

        if (fragmentImagen != null) {

            fragmentImagen.showImage(getIntent().getStringExtra("nivel"), getIntent().getStringExtra("dificultad"));
        }
    }


    public void onResponder(View view){
        EditText e = findViewById(R.id.etRespuesta);
        String r = e.getText().toString();
        Intent datos = new Intent();
        datos.putExtra("respuesta", r);
        datos.putExtra("position", getIntent().getIntExtra("position", -1));
        setResult(RESULT_OK, datos);
        finish();
    }
}
