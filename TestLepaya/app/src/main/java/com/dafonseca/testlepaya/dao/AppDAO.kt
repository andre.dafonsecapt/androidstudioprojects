package com.dafonseca.testlepaya.dao

import android.content.Context
import com.dafonseca.testlepaya.models.Trainer
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

class AppDAO {

    fun getTrainersFromJSON(context: Context): List<Trainer> {
        val trainersJsonString = readJSONFromAssets(context)
        val gson = Gson()
        val trainers: Array<Trainer> = gson.fromJson<Array<Trainer>>(
            trainersJsonString,
            Array<Trainer>::class.java
        )
        return Arrays.asList<Trainer>(*trainers)
    }

    fun getTrainersFromLocalJSON(context: Context): List<Trainer> {
        val gson = Gson()
        val bufferedReader: BufferedReader = File(context.filesDir,"trainersLocal.json").bufferedReader()
        val inputString = bufferedReader.use { it.readText() }
        val trainers: Array<Trainer> = gson.fromJson<Array<Trainer>>(
            inputString,
            Array<Trainer>::class.java
        )
        return Arrays.asList<Trainer>(*trainers)
    }

    private fun readJSONFromAssets(context: Context): String? {
        var json: String? = null
        try {
            val inputStream = context.assets.open("trainers.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, charset("UTF-8"))
        } catch (ex: IOException) {
            Logger.getLogger(this.javaClass.name)
                .log(Level.SEVERE, null, ex)
            return null
        }
        return json
    }

    fun writeToLocalFile(context: Context, stringToWrite: String){
        try {
            context.openFileOutput("trainersLocal.json", Context.MODE_PRIVATE).use {
                it.write(stringToWrite.toByteArray())
            }
        } catch (e: Exception){
            Logger.getLogger(this.javaClass.name)
                .log(Level.SEVERE, null, e)
        }
    }
}