package com.dafonseca.testdtt.splash;

public class SplashPresenter implements SplashContract.Presenter{

    private final SplashContract.View splashView;

    SplashPresenter(SplashContract.View splashView) {
        this.splashView = splashView;
    }

    ////        Presenter Methods       /////
    @Override
    public void handleSplashTimer() {
        splashView.showMainScreen();
    }
}
