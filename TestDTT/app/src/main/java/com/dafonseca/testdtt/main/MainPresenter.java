package com.dafonseca.testdtt.main;

public class MainPresenter implements MainContract.Presenter {

    private final MainContract.View mainView;

    MainPresenter(MainContract.View mainView) {
        this.mainView = mainView;
    }

    ////        Presenter Methods       /////
    @Override
    public void handleMainButton() {
        mainView.showMapScreen();
    }

    @Override
    public void handlePrivacyInfoButton() {
        mainView.showPrivacyAlert();
    }
}
