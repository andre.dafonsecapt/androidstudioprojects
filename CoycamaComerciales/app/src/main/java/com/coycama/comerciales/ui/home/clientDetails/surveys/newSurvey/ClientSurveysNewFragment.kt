package com.coycama.comerciales.ui.home.clientDetails.surveys.newSurvey

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.coycama.comerciales.R

class ClientSurveysNewFragment : Fragment() {

    companion object {
        fun newInstance() = ClientSurveysNewFragment()
    }

    private lateinit var viewModel: ClientSurveysNewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_client_survey_new, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ClientSurveysNewViewModel::class.java)
        // TODO: Use the ViewModel
    }

}