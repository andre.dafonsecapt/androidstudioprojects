package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.Good
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GoodsRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val goods = MutableLiveData<List<Good>>()

    suspend fun getGoodsByClient(clientId: GetByClientRequest) : LiveData<List<Good>> {
        return withContext(Dispatchers.IO){
            fetchGoodsByClient(clientId)
            goods
        }
    }

    private suspend fun fetchGoodsByClient(clientId: GetByClientRequest){
        val response = apiRequest { api.getGoodsByClient(clientId) }
        goods.postValue(response.records)
    }
}