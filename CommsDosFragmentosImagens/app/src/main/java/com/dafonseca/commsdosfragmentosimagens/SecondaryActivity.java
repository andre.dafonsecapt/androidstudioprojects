package com.dafonseca.commsdosfragmentosimagens;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondaryActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        FragmentReceiver fragmentReceiver = (FragmentReceiver) getSupportFragmentManager().findFragmentById(R.id.fragmentReceiver);

        if (fragmentReceiver != null) {

            fragmentReceiver.showImage(getIntent().getStringExtra("imageName"));
        }

    }
}
