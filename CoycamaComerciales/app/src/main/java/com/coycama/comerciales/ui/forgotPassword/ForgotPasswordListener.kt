package com.coycama.comerciales.ui.forgotPassword

interface ForgotPasswordListener {
    fun onStarted()
    fun onSuccess(message: String)
    fun onFailure(message: String)
}