package com.dafonseca.testlepaya.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.dafonseca.testlepaya.R
import com.dafonseca.testlepaya.models.Trainer


class TrainerAdapter(private val data: List<Trainer>, val context: Context) :
    RecyclerView.Adapter<TrainerAdapter.ViewHolder?>() {

    private var clickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recyclerview_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context)
            .asBitmap()
            .load(data[position].picture)
            .fitCenter()
            .override(500, 500)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(holder.picture)
        holder.name.text = data[position].name
        holder.email.text = data[position].email
    }

    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }

    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    fun getItem(position: Int): Trainer {
        return data[position]
    }

    fun getAllItems() : List<Trainer>{
        return data
    }

    fun updateItem(trainer: Trainer) {
        data[trainer.index].name = trainer.name
        data[trainer.index].email = trainer.email
        data[trainer.index].isAvailable = trainer.isAvailable
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var picture: ImageView
        var name: TextView
        var email: TextView

        init {
            picture = itemView.findViewById(R.id.picture)
            name = itemView.findViewById(R.id.name)
            email = itemView.findViewById(R.id.email)
            itemView.setOnClickListener { itemView ->
                if (clickListener != null) clickListener!!.onItemClick(
                    itemView,
                    adapterPosition
                )
            }
        }
    }
}