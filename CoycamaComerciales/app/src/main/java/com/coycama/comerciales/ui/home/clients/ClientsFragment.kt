package com.coycama.comerciales.ui.home.clients

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.coycama.comerciales.utils.PermissionsUtility
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_clients.*
import kotlinx.android.synthetic.main.fragment_clients.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions


class ClientsFragment : Fragment(), ClientItemListener, KodeinAware,
    EasyPermissions.PermissionCallbacks {

    override val kodein by kodein()

    private val factory: ClientsViewModelFactory by instance()
    private lateinit var viewModel: ClientsViewModel

    private lateinit var clientItemListener: ClientItemListener

    private var hasPhonePermissions = false
    val REQUEST_CODE_CALL_PHONE_PERMISSION = 56

    private lateinit var clientAdapter: GroupieAdapter
    private lateinit var clients: List<Client>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_clients, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(ClientsViewModel::class.java)
        viewModel.getLoggedInUser().observe(viewLifecycleOwner, {
            viewModel.activeUser = it
            bindUI()
        })

        clientItemListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_search_icon?.visibility = View.VISIBLE
        activity?.home_toolbar_title?.text = "Clientes"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        setClickListeners(view)
    }

    private fun setClickListeners(view: View){
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }
        activity?.home_toolbar_search_icon?.setOnClickListener {
            if (view.clients_list_search_et.visibility == View.GONE){
                view.clients_list_search_et.visibility = View.VISIBLE
                view.clients_list_search_btn.visibility = View.VISIBLE
            }else {
                view.clients_list_search_et.visibility = View.GONE
                view.clients_list_search_btn.visibility = View.GONE
            }
        }
        view.clients_list_search_btn.setOnClickListener {
            val searchString = view.clients_list_search_et.text.toString()
            val filteredList = clients.filter {
                it.NomComercial.contains(searchString, true) || it.NomFiscal.contains(searchString, true)
            }.toList().toClientItem()
            clientAdapter.update(filteredList)
        }
    }

    private fun bindUI() = Coroutines.main {
        clients_list_progressbar.show()
        try {
            viewModel.clients.await().observe(viewLifecycleOwner, {
                clients = it
                initRecyclerView(it.toClientItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        clients_list_progressbar.hide()
    }

    private fun initRecyclerView(clientItems: List<ClientItem>) {

        clientAdapter = GroupieAdapter().apply {
            addAll(clientItems)
        }
        clients_list_recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = clientAdapter
        }
    }


    private fun List<Client>.toClientItem(): List<ClientItem> {
        return this.map {
            ClientItem(it, clientItemListener)
        }
    }

    override fun onClientsInfoLayoutClicked(client: Client) {
        navigateToClientDetailsPage(client)
    }

    override fun onCallClicked(client: Client) {
        requestPermissions()
        if (hasPhonePermissions) {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "1122334455"))
            startActivity(intent)
        }
    }

    override fun onMapClicked(client: Client) {
        TODO("Not yet implemented")
    }

    override fun onDetailsClicked(client: Client) {
        navigateToClientDetailsPage(client)
    }

    private fun navigateToClientDetailsPage(client: Client) {
        viewModel.setConsulta(client)
        val action = ClientsFragmentDirections.clientsListPageToClientDetailsPage(client)
        findNavController().navigate(action)
    }

    private fun requestPermissions() {
        if (PermissionsUtility.hasPhonePermissions(requireContext())) {
            hasPhonePermissions = true
            return
        }
        EasyPermissions.requestPermissions(
            this,
            "Para llamar, necesitas dar permisso para acceder al teléfono",
            REQUEST_CODE_CALL_PHONE_PERMISSION,
            Manifest.permission.CALL_PHONE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        hasPhonePermissions = true
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            requestPermissions()
        }
    }
}