package com.dafonseca.testdtt.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dafonseca.testdtt.R;
import com.dafonseca.testdtt.main.MainActivity;

public class SplashActivity extends AppCompatActivity implements SplashContract.View {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final SplashPresenter splashPresenter = new SplashPresenter(this);

        // Splash screen timer
        int SPLASH_TIME_OUT = 3000;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                splashPresenter.handleSplashTimer();
            }
        }, SPLASH_TIME_OUT);
    }

    ////        View Methods       /////
    @Override
    public void showMainScreen() {
        // Start app main activity
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);

        // close this activity
        finish();
    }
}
