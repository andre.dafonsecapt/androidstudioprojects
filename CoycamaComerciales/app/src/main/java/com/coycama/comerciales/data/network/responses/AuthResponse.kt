package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.db.entities.User

data class AuthResponse(
    val isSuccessful: Boolean?,
    val message: String?,
    val user: User?
)