package com.dafonseca.utour.retrofit;

import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.utils.ConstantsAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TourAPI {

    @GET(ConstantsAPI.URL_TOURS_USER)
    Call<List<Tour>> getMyTours(@Path("idUser") int idUser);

    @GET(ConstantsAPI.URL_TOURS)
    Call<List<Tour>> getAllTours(@Query("idUser") int idUser);

    @POST(ConstantsAPI.URL_TOURS)
    Call<String> addTour(@Body String tourJSON);

    @PUT(ConstantsAPI.URL_TOURS)
    Call<String> updateTour(@Body String tourJSON);

    @DELETE(ConstantsAPI.URL_TOURS)
    Call<String> deleteTour(@Query("idTour") int idTour);
}
