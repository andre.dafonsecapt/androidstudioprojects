package com.coycama.comerciales.data.network.requests

import com.google.gson.annotations.SerializedName

data class GetByClientRequest(
    @SerializedName("Cliente") var client: Int
)