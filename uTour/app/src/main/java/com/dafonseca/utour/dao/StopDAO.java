package com.dafonseca.utour.dao;

import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.retrofit.StopAPI;
import com.dafonseca.utour.singleton.RetrofitSingleton;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

public class StopDAO implements StopAPI {

    private final StopAPI stopAPI;
    private final Gson gson;

    public StopDAO() {
        stopAPI = RetrofitSingleton.getInstance().getRetrofit().create(StopAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<List<Stop>> getStopsByIdTour(int idTour) {
        return stopAPI.getStopsByIdTour(idTour);
    }
}
