package com.dafonseca.testdtt.map;

import android.app.Activity;

import com.dafonseca.testdtt.R;
import com.dafonseca.testdtt.services.AppServices;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapPresenter extends AppServices implements MapContract.Presenter {

    private final MapContract.View mapView;

    MapPresenter(MapContract.View mapView) {
        this.mapView = mapView;
    }

    /////       Presenter Methods       //////
    @Override
    public void handleListeners(String propertyToActivate) {
        mapView.showAlert(propertyToActivate);
    }

    @Override
    public void handleCallButton() {
        mapView.showCallIntent();
    }

    @Override
    public void handleCustomDialogCallButton() {
        mapView.showCustomDialog();
    }

    @Override
    public void setMarker(LatLng latLng, String address, float DEFAULT_ZOOM, Activity activity) {

        MarkerOptions myMarker = new MarkerOptions()
                .position(latLng)
                .title(activity.getString(R.string.uw_locatie))
                .snippet(address)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker));

        mapView.showInfoWindow(myMarker, latLng, DEFAULT_ZOOM);
    }
}
