package com.coycama.comerciales.data.models

import com.google.gson.annotations.SerializedName

data class Sale(
    @SerializedName("Año") val year: String,
    val Cliente: String,
    val Facturacion: String,
    var Mes: String,
    val SubCliente: String,
    val UnidadesVendidas: String
)