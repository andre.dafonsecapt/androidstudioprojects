package com.dafonseca.testdtt.splash;

public interface SplashContract {

    interface View {
        void showMainScreen();
    }

    interface Presenter {
        void handleSplashTimer();
    }
}
