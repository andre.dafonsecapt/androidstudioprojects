package com.dafonseca.utour.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Feedback {

    private int idFeedback;
    private String opinion;
    private int rating;
    private int fk_idTour;
}
