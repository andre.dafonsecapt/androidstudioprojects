package com.dafonseca.utour.models;

import android.graphics.Bitmap;
import android.util.LruCache;

public class BitmapCache {

    private static BitmapCache instance;
    private final LruCache<String, Bitmap> lru;

    private BitmapCache() {

        lru = new LruCache<>(1024);

    }

    public static BitmapCache getInstance() {

        if (instance == null) {

            instance = new BitmapCache();
        }

        return instance;

    }

    public LruCache<String, Bitmap> getLru() {
        return lru;
    }
}
