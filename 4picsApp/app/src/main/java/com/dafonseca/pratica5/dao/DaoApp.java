package com.dafonseca.pratica5.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Xml;

import com.dafonseca.pratica5.db.PuntuacionesSQLiteHelper;
import com.dafonseca.pratica5.models.Puntuacion;
import com.dafonseca.pratica5.models.Usuario;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoApp {


    public List<Puntuacion> getPuntuaciones(PuntuacionesSQLiteHelper usdbh) {

        SQLiteDatabase db = usdbh.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT id, nombre, dificultad, tiempo, score FROM Puntuaciones", null);
        List<Puntuacion> puntuaciones = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Puntuacion p = new Puntuacion();
                p.setId(c.getInt(c.getColumnIndex("id")));
                p.setNombre(c.getString(c.getColumnIndex("nombre")));
                p.setDificultad(c.getString(c.getColumnIndex("dificultad")));
                p.setTiempo(c.getInt(c.getColumnIndex("tiempo")));
                p.setScore(c.getInt(c.getColumnIndex("score")));
                puntuaciones.add(p);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return puntuaciones;
    }

    public boolean savePuntuacion(Puntuacion p, PuntuacionesSQLiteHelper usdbh) {
        SQLiteDatabase db = null;
        try {
            String[] args = new String[]{p.getNombre(), p.getDificultad(), String.valueOf(p.getTiempo()), String.valueOf(p.getScore())};
            db = usdbh.getWritableDatabase();
            db.execSQL("INSERT INTO Puntuaciones (nombre,dificultad,tiempo,score) VALUES (?,?,?,?)", args);
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public boolean updatePuntuacion(Puntuacion p, PuntuacionesSQLiteHelper usdbh) {
        SQLiteDatabase db = null;
        try {
            // update Puntuacion to db
            ContentValues valores = new ContentValues();
            valores.put("nombre", p.getNombre());
            valores.put("dificultad", p.getDificultad());
            valores.put("tiempo", p.getTiempo());
            valores.put("score", p.getScore());
            db = usdbh.getWritableDatabase();
            db.update("Puntuaciones", valores, "id=?", new String[]{String.valueOf(p.getId())});
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public boolean deletePuntuacion(String puntIdClicked, PuntuacionesSQLiteHelper usdbh) {
        SQLiteDatabase db = null;
        try {
            // delete Puntuacion to db
            db = usdbh.getWritableDatabase();
            db.delete("Puntuaciones", "id=?", new String[]{puntIdClicked});
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public List<Usuario> getUsuariosFromXML(Activity activity) {
        List<Usuario> usuarios = new ArrayList<>();
        try {
            // create list of users from XML using XMLPullParser
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            InputStream is = activity.openFileInput("xmlFile.xml");
            parser.setInput(is, null);

            Usuario usuarioActivo = null;
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() == XmlPullParser.START_TAG) {
                    switch (parser.getName()) {
                        case "usuario":
                            usuarioActivo = new Usuario();
                            usuarios.add(usuarioActivo);
                            break;
                        case "nombre":
                            usuarioActivo.setNombre(parser.nextText());
                            break;
                        case "visitas":
                            usuarioActivo.setVisitas(Integer.parseInt(parser.nextText()));
                            break;
                        case "tiempoUso":
                            usuarioActivo.setTiempoUso(parser.nextText());
                            break;
                    }
                }
                parser.next();
            }
            is.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return usuarios;
    }

    public void writeUsuariosToXML(List<Usuario> usuarios, Activity activity) {
        try {
            // Declarar y abrir el fichero externo
            FileOutputStream fileos = activity.openFileOutput("xmlFile.xml",
                    Context.MODE_PRIVATE);
            // Crear un objeto de la clase XmlSerializer:
            XmlSerializer xmlSerializer = Xml.newSerializer();

            // Utilizaremos un objeto StringWriter para editar el serializador:
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);

            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, "usuarios");
            for (Usuario u : usuarios) {
                xmlSerializer.startTag(null, "usuario");
                xmlSerializer.startTag(null, "nombre");
                xmlSerializer.text(u.getNombre());
                xmlSerializer.endTag(null, "nombre");
                xmlSerializer.startTag(null, "visitas");
                xmlSerializer.text(String.valueOf(u.getVisitas()));
                xmlSerializer.endTag(null, "visitas");
                xmlSerializer.startTag(null, "tiempoUso");
                xmlSerializer.text(u.getTiempoUso());
                xmlSerializer.endTag(null, "tiempoUso");
                xmlSerializer.endTag(null, "usuario");
            }
            xmlSerializer.endTag(null, "usuarios");
            xmlSerializer.endDocument();

            // Escribir resultado en el fichero y cerrar stream:
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }
}
