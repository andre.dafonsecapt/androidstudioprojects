package com.dafonseca.testlepayajava.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.testlepayajava.R;
import com.dafonseca.testlepayajava.models.Trainer;
import com.squareup.picasso.Picasso;

public class TrainerDetailsActivity extends AppCompatActivity {

    private Trainer trainer;
    private ImageView detailsPicture;
    private EditText etName;
    private EditText etEmail;
    private CheckBox checkBoxAvailability;
    private EditText etFruit;
    private EditText etSkills;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer_details);

        detailsPicture = findViewById(R.id.detailsPicture);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        checkBoxAvailability = findViewById(R.id.checkBoxAvailability);
        etFruit = findViewById(R.id.etFruit);
        etSkills = findViewById(R.id.etSkills);
        trainer = getIntent().getParcelableExtra("trainer");
        setTrainerDetails();
    }

    private void setTrainerDetails(){
        Picasso.get().load(trainer.getPicture()).into(detailsPicture);
        etName.setText(trainer.getName());
        etEmail.setText(trainer.getEmail());
        if (trainer.isAvailable()){
            checkBoxAvailability.setChecked(true);
        } else {
            checkBoxAvailability.setChecked(false);
        }
        etFruit.setText(trainer.getFavoriteFruit());
        etSkills.setText(trainer.getSkills().toString());

    }

    public void backAction(View view){
        trainer.setName(etName.getText().toString());
        trainer.setEmail(etEmail.getText().toString());
        trainer.setAvailable(checkBoxAvailability.isChecked());
        Intent data = new Intent();
        data.putExtra("trainer", trainer);
        setResult(RESULT_OK, data);
        finish();
    }
}
