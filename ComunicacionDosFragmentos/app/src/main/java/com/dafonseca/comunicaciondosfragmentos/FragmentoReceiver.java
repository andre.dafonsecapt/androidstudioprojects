package com.dafonseca.comunicaciondosfragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class FragmentoReceiver extends Fragment {

    View rootView;
    TextView tv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fragmentoreceiver, container,false);
        tv = rootView.findViewById(R.id.textView);

        return rootView;
    }

    public void showMessage(String message){
        tv.setText(message);
    }
}
