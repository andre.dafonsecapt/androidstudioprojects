package com.dafonseca.commsdosfragmentosimagens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

public class FragmentReceiver extends Fragment {

    private View rootView;
    private ImageView image;

    public static FragmentReceiver newInstance() {
        return new FragmentReceiver();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_fragmentreceiver, container, false);
        image = rootView.findViewById(R.id.imageView);


        return rootView;

    }

    public void showImage(String image) {
        switch (image){
            case "imagen1":
                this.image.setImageResource(R.drawable.imagen1);
                break;
            case "imagen2":
                this.image.setImageResource(R.drawable.imagen2);
                break;
            case "imagen3":
                this.image.setImageResource(R.drawable.imagen3);
                break;
            case "imagen4":
                this.image.setImageResource(R.drawable.imagen4);
                break;
            case "imagen5":
                this.image.setImageResource(R.drawable.imagen5);
                break;
        }
    }


}
