package com.dafonseca.utour.fragments;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dafonseca.utour.R;
import com.dafonseca.utour.adapters.FeedbackAdapter;
import com.dafonseca.utour.alertDialogs.CustomFeedbackEditAlertDialog;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Feedback;
import com.dafonseca.utour.services.FeedbackServices;
import com.dafonseca.utour.viewmodels.MainViewModel;

import java.util.ArrayList;
import java.util.List;

import fj.data.Either;
import lombok.SneakyThrows;

public class FeedbackSearchSplashFragment extends Fragment implements FeedbackAdapter.ItemClickListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private FeedbackAdapter feedbackAdapter;
    private RecyclerView recyclerView;
    private FeedbackServices feedbackServices;
    private List<Feedback> feedbackList;
    private CustomFeedbackEditAlertDialog feedbackEditAlertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_feedbacks_search_splash, container, false);

        setViews(rootView);
        setTitle();
        initObjects();
        setRecyclerAdapter(rootView);
        setListeners();
        launchGetToursTask();

        return rootView;
    }

    private void setViews(View rootView) {
        swipeRefreshLayout = rootView.findViewById(R.id.feedbacks_splash_swipe_refresh);
        recyclerView = rootView.findViewById(R.id.feedbacks_splash_recycler);
    }

    private void setTitle() {
        MainViewModel viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        viewModel.getAppTitle().setValue(getString(R.string.title_feedbacks));
    }

    private void initObjects() {
        feedbackList = new ArrayList<>();
        feedbackServices = new FeedbackServices();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
    }

    private void setRecyclerAdapter(View rootView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        feedbackAdapter = new FeedbackAdapter();
        feedbackAdapter.setClickListener(this);
        recyclerView.setAdapter(feedbackAdapter);
    }

    private void setListeners() {
        swipeRefreshLayout.setOnRefreshListener(this::launchGetToursTask);
    }

    public void launchGetToursTask() {
        GetFeedbacksTask task = new GetFeedbacksTask();
        task.execute();
    }

    @Override
    public void onItemClick(View view, int position) {
        feedbackEditAlertDialog = new CustomFeedbackEditAlertDialog(this, feedbackAdapter.getItem(position));
        feedbackEditAlertDialog.show();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (feedbackEditAlertDialog != null) {
            feedbackEditAlertDialog.dismiss();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class GetFeedbacksTask extends AsyncTask<Void, Void, Either<ApiError, List<Feedback>>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Feedback>> doInBackground(Void... voids) {
            return feedbackServices.getFeedbacks();
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Feedback>> result) {
            swipeRefreshLayout.setRefreshing(false);
            if (result.isRight()) {
                feedbackList = result.right().value();
                feedbackAdapter.setItems(feedbackList);
            } else {
                Toast.makeText(getContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
