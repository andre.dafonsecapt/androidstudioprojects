package com.dafonseca.utour.retrofit;

import com.dafonseca.utour.models.Profile;
import com.dafonseca.utour.utils.ConstantsAPI;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProfileAPI {

    @POST(ConstantsAPI.URL_STATS)
    Call<Void> incrementToursTaken(@Query("idUser") int idUser);

    @PUT(ConstantsAPI.URL_STATS)
    Call<Void> incrementToursRated(@Query("idUser") int idUser);

    @GET(ConstantsAPI.URL_PROFILE_USER)
    Call<Profile> getProfile(@Path("idUser") int idUser);


    @POST(ConstantsAPI.URL_PROFILE)
    Call<String> updateProfile(@Body Profile profile);

    @PUT(ConstantsAPI.URL_PROFILE)
    Call<String> adminUpdateProfile(@Body Profile profile);
}
