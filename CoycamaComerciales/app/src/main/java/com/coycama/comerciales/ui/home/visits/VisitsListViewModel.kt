package com.coycama.comerciales.ui.home.visits

import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import com.coycama.comerciales.data.repositories.ComercialRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.utils.lazyDeferred

class VisitsListViewModel(
    private val comercialRepository: ComercialRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    var activeUser: User? = null

    fun getLoggedInUser() = userRepository.getUser()

    val visits by lazyDeferred {
        val request = GetByComercialRequest(0)
        if (activeUser != null) {
            request.comercial = activeUser!!.id!!
        }
        comercialRepository.getVisitsByComercial(request)
    }
}