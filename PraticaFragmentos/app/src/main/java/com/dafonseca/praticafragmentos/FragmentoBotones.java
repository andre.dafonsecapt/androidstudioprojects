package com.dafonseca.praticafragmentos;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class FragmentoBotones extends Fragment {

    private View rootView;
    private Button salvarButton;
    private Button salirButton;
    private FloatingActionButton floatingButton;

    public static FragmentoBotones newInstance() {
        return new FragmentoBotones();
    }
    /*La interfaz del fragmento*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fragmentobotones, container,false);
        salvarButton = rootView.findViewById(R.id.salvarButton);
        salirButton = rootView.findViewById(R.id.salirButton);
        floatingButton = rootView.findViewById(R.id.floatingActionButton);

        salvarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getContext();
                Toast toast = Toast.makeText(context,R.string.salvarMessage, Toast.LENGTH_LONG);
                toast.show();
            }
        });

        salirButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setMessage(R.string.closeApp);
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finishAffinity();
                    }
                });
                alert.setNegativeButton("NO", null);
                alert.show();
            }
        });

        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getContext();
                Toast toast = Toast.makeText(context,R.string.floatingButtonMessage, Toast.LENGTH_LONG);
                toast.show();
            }
        });
        return rootView;
    }

}
