package com.coycama.comerciales.data.models

import com.google.gson.annotations.SerializedName

data class Good(
    @SerializedName("Año") val year: String,
    val Cliente: String,
    val DescSubfamilia: String,
    var Facturacion: String,
    val Mes: String,
    val SubCliente: String,
    val Subfamilia: String,
    val UnidadesVendidas: String
)