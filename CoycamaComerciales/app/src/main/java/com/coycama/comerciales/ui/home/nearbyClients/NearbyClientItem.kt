package com.coycama.comerciales.ui.home.nearbyClients

import android.annotation.SuppressLint
import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.databinding.ItemClientBinding
import com.coycama.comerciales.databinding.ItemNearbyClientBinding
import com.coycama.comerciales.ui.home.clients.ClientItemListener
import com.coycama.comerciales.utils.Coroutines
import com.xwray.groupie.viewbinding.BindableItem

class NearbyClientItem(
    private val client: Client,
    private val clientItemListener: NearbyClientItemListener
) : BindableItem<ItemNearbyClientBinding>() {

    @SuppressLint("ClickableViewAccessibility")
    override fun bind(viewBinding: ItemNearbyClientBinding, position: Int) {
        viewBinding.client = client

        viewBinding.itemNearbyClientMapImageview.setOnClickListener {
            clientItemListener.onInitRouteClicked(client)
        }

        viewBinding.itemNearbyClientComercialNameTv.setOnClickListener {
            clientItemListener.onClientNameClicked(client)
        }
    }

    override fun getLayout() = R.layout.item_nearby_client

    override fun initializeViewBinding(view: View): ItemNearbyClientBinding {
        return ItemNearbyClientBinding.bind(view)
    }
}