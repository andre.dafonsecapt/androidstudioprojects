package com.coycama.comerciales.ui.home.clientDetails.goods

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Good
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_client_goods.*
import kotlinx.android.synthetic.main.fragment_client_goods.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ClientGoodsFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val factory: ClientGoodsViewModelFactory by instance()
    private lateinit var viewModel: ClientGoodsViewModel

    private val args by navArgs<ClientGoodsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_client_goods, container, false)
        setListeners(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(ClientGoodsViewModel::class.java)
        viewModel.clientId = args.client.Cliente.toInt()
        bindUI()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Ficha Cliente"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }
    }

    private fun setListeners(view: View) {
        view.client_goods_details_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientGoodsFragmentDirections.clientGoodsPageToClientDetailsPage(
                    args.client
                )
            )
        }
        view.client_goods_sales_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientGoodsFragmentDirections.clientGoodsPageToClientSalesPage(
                    args.client
                )
            )
        }

        view.client_goods_surveys_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientGoodsFragmentDirections.clientGoodsPageToClientSurveyListPage(
                    args.client
                )
            )
        }
    }

    private fun bindUI() = Coroutines.main {
        client_goods_progressbar.show()
        try {
            viewModel.goods.await().observe(viewLifecycleOwner, {
                initRecyclerView(it.toGoodItem())
            })
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
        client_goods_progressbar.hide()
    }

    private fun initRecyclerView(goodItems: List<GoodItem>) {
        val goodsAdapter = GroupieAdapter().apply {
            addAll(goodItems)
        }
        client_goods_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = goodsAdapter
        }
    }


    private fun List<Good>.toGoodItem(): List<GoodItem> {
        return this.map {
            GoodItem(it)
        }
    }

}