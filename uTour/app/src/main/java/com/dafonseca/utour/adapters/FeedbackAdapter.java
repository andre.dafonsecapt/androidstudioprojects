package com.dafonseca.utour.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.models.Feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {

    private List<Feedback> data;
    private ItemClickListener clickListener;
    private final Random random;

    public FeedbackAdapter() {
        this.data = new ArrayList<>();
        this.random = new Random();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_feedback, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.username.setText(generateRandomUsername());
        holder.opinion.setText(data.get(position).getOpinion());
        holder.ratingBar.setRating((float) data.get(position).getRating());
    }

    private String generateRandomUsername() {
        char upperCaseChar = (char) (random.nextInt(26) + 'A');
        char lowerCaseChar = (char) (random.nextInt(26) + 'a');
        StringBuilder nameFiller = new StringBuilder();
        for (int i = 0; i < (random.nextInt(7) + 1); i++) {
            nameFiller.append("*");
        }
        return upperCaseChar + nameFiller.toString() + lowerCaseChar;
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Feedback getItem(int position) {
        return data.get(position);
    }

    public void setItems(List<Feedback> items) {
        this.data = items;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView username;
        final TextView opinion;
        private final RatingBar ratingBar;

        ViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.row_feedback_username);
            ratingBar = itemView.findViewById(R.id.row_feedback_ratingBar);
            opinion = itemView.findViewById(R.id.row_feedback_opinion);
            itemView.setOnClickListener(itemView1 -> {
                if (clickListener != null) {
                    clickListener.onItemClick(itemView1, getAdapterPosition());
                }
            });
        }
    }
}
