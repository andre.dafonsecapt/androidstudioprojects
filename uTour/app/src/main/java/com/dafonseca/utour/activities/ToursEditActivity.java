package com.dafonseca.utour.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dafonseca.utour.R;
import com.dafonseca.utour.adapters.StopAdapter;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.BitmapCache;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.StopServices;
import com.dafonseca.utour.services.TourServices;
import com.dafonseca.utour.utils.ImageTools;
import com.dafonseca.utour.viewmodels.TourViewModel;
import com.google.android.material.textfield.TextInputEditText;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import lombok.SneakyThrows;

import static com.dafonseca.utour.utils.ConstantsClient.CAMERA_PERMISSION_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.CREATE_STOP_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_CAPTURE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_GALLERY;
import static com.dafonseca.utour.utils.ConstantsClient.UPDATE_STOP_CODE;

public class ToursEditActivity extends AppCompatActivity implements StopAdapter.ItemClickListener {

    private ScrollView scrollView;
    private ImageView imageView;
    private TextInputEditText etName;
    private TextInputEditText etDescription;
    private TextInputEditText etCity;
    private NumberPicker npHours;
    private NumberPicker npMinutes;
    private Button editTourBtn;
    private StopAdapter adapterStops;
    private CardView cardViewStops;
    private ProgressBar progressBarStops;
    private ProgressBar progressBar;
    private TourServices tourServices;
    private StopServices stopServices;
    private Tour tourToUpdate;
    private String tourJSON;
    private String oldStopPositionStr;
    private String imgListSizeStr;
    private ImageTools imageTools;
    private TourViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tours_edit);
        setViews();
        initObjects();
        setListeners();
        initRecyclerView();
        setFields();
        setViewModel();
        executeGetStopsTask();
    }

    private void setViews() {
        scrollView = findViewById(R.id.tours_edit_scrollview);
        imageView = findViewById(R.id.tours_edit_img);
        etName = findViewById(R.id.tours_edit_name);
        etDescription = findViewById(R.id.tours_edit_description);
        etCity = findViewById(R.id.tours_edit_city);
        npHours = findViewById(R.id.tours_edit_hours);
        npHours.setMaxValue(999);
        npHours.setMinValue(0);
        npMinutes = findViewById(R.id.tours_edit_minutes);
        npMinutes.setMaxValue(60);
        npMinutes.setMinValue(0);
        cardViewStops = findViewById(R.id.tours_edit_stops_cardview);
        progressBarStops = findViewById(R.id.tours_edit_stops_progbar);
        editTourBtn = findViewById(R.id.tours_edit_tour_btn);
        progressBar = findViewById(R.id.tours_edit_progbar);
    }

    private void initObjects() {
        tourServices = new TourServices();
        stopServices = new StopServices();
        tourToUpdate = new Tour();
        imageTools = new ImageTools();
        adapterStops = new StopAdapter(new ArrayList<>(), this);
        oldStopPositionStr = "oldStopPosition";
        imgListSizeStr = "imgListSize";
    }

    private void setListeners() {
        etName.addTextChangedListener(new MyTextWatcher());
        etCity.addTextChangedListener(new MyTextWatcher());
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = findViewById(R.id.tours_edit_stops_recyclerview);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterStops);
        adapterStops.setClickListener(this);
    }

    private void setFields() {
        Tour tourSelected = getIntent().getParcelableExtra("tourSelected");
        if (tourSelected != null) {
            this.tourToUpdate = tourSelected;
            imageView.setImageBitmap(BitmapCache.getInstance().getLru().get("tourImage"));
            etName.setText(tourSelected.getTourname());
            etDescription.setText(tourSelected.getDescription());
            etCity.setText(tourSelected.getCity());
            String durationString;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                Date dt = sdf.parse(String.valueOf(tourSelected.getDuration()));
                sdf = new SimpleDateFormat("HH:mm");
                durationString = sdf.format(dt);
                String[] durationArray = durationString.split(":");
                npHours.setValue(Integer.parseInt(durationArray[0]));
                npMinutes.setValue(Integer.parseInt(durationArray[1]));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    private void setViewModel() {
        viewModel = new ViewModelProvider(this).get(TourViewModel.class);
        viewModel.getStopsLive().observe(this, stops -> adapterStops.setItems(stops));
        viewModel.getHourLive().observe(this, hour -> npHours.setValue(hour));
        viewModel.getMinuteLive().observe(this, minutes -> npMinutes.setValue(minutes));
    }

    private void executeGetStopsTask() {
        if (Objects.requireNonNull(viewModel.getStopsLive().getValue()).isEmpty()) {
            GetStopsTask getStopsTask = new GetStopsTask();
            getStopsTask.execute();
        }
    }

    public void onUploadPhotoBtnClick(View view) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else if (options[item].equals("Choose from Gallery")) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_IMAGE_CAPTURE || requestCode == REQUEST_IMAGE_GALLERY) {
                    Bitmap imageBitmap;
                    if (requestCode == REQUEST_IMAGE_CAPTURE) {
                        Bundle extras = data.getExtras();
                        imageBitmap = (Bitmap) Objects.requireNonNull(extras).get("data");
                        imageView.setImageBitmap(imageBitmap);
                    } else {
                        Uri imageUri = data.getData();
                        imageBitmap = imageTools.getCorrectlyOrientedImage(this, imageUri, 800);
                        Glide.with(this).load(imageUri).into(imageView);
                    }
                    imageBitmap = imageTools.getResizedBitmap(Objects.requireNonNull(imageBitmap), 1000);
                    tourToUpdate.setImage(imageTools.convertBitmapToEncodedString(imageBitmap));
                } else if (requestCode == CREATE_STOP_CODE) {
                    Stop newStop = data.getParcelableExtra("stop");
                    if (newStop != null) {
                        int imgListSize = data.getIntExtra(imgListSizeStr, 0);
                        newStop.setImages(getCachedImages(imgListSize));
                        adapterStops.addItem(newStop);
                    }
                } else if (requestCode == UPDATE_STOP_CODE) {
                    Stop updatedStop = data.getParcelableExtra("stop");
                    if (updatedStop != null) {
                        int imgListSize = data.getIntExtra(imgListSizeStr, 0);
                        updatedStop.setImages(getCachedImages(imgListSize));
                        int oldStopPosition = data.getIntExtra(oldStopPositionStr, 0);
                        adapterStops.updateItem(updatedStop, oldStopPosition);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            Toast.makeText(this, "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    private List<String> getCachedImages(int imgListSize) {
        List<String> encodedImgList = new ArrayList<>();
        for (int i = 0; i < imgListSize; i++) {
            Bitmap cachedImg = BitmapCache.getInstance().getLru().get("stopCachedImg" + i);
            encodedImgList.add(imageTools.convertBitmapToEncodedString(cachedImg));
        }
        return encodedImgList;
    }

    /////    Click Events Methods      //////

    @Override
    public void onItemClick(View view, int position) {
        Stop stopSelected = adapterStops.getItem(position);
        for (int i = 0; i < stopSelected.getImages().size(); i++) {
            BitmapCache.getInstance().getLru().put("stopCachedImg" + i, imageTools.convertEncodedStringToBitmap(stopSelected.getImages().get(i)));
        }
        Intent i = new Intent(this, StopsNewActivity.class);
        i.putExtra("stopSelected", stopSelected);
        i.putExtra(oldStopPositionStr, position);
        i.putExtra(imgListSizeStr, stopSelected.getImages().size());
        stopSelected.setImages(new ArrayList<>());
        startActivityForResult(i, UPDATE_STOP_CODE);
    }


    public void onAddStopBtnClick(View view) {
        Intent i = new Intent(this, StopsNewActivity.class);
        startActivityForResult(i, CREATE_STOP_CODE);
    }

    public void onEditTourBtnClick(View view) {
        tourToUpdate.setTourname(Objects.requireNonNull(etName.getText()).toString());
        tourToUpdate.setDescription(Objects.requireNonNull(etDescription.getText()).toString());
        tourToUpdate.setCity(Objects.requireNonNull(etCity.getText()).toString());
        int hours = npHours.getValue();
        int minutes = npMinutes.getValue();
        minutes = minutes + (hours * 60);
        tourToUpdate.setDuration(minutes);
        tourToUpdate.setFk_idUser(DataHolder.getInstance().getIdUser());
        tourToUpdate.setStops(adapterStops.getData());
        tourJSON = tourServices.getGson().toJson(tourToUpdate);
        EditTourTask editTourTask = new EditTourTask();
        editTourTask.execute();
    }

    /////    Lifecycle Methods      //////

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.getHourLive().setValue(npHours.getValue());
        viewModel.getMinuteLive().setValue(npMinutes.getValue());
    }

    @Override
    protected void onResume() {
        super.onResume();
        editTourBtn.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty()
                && !Objects.requireNonNull(etCity.getText()).toString().isEmpty()
                && Objects.requireNonNull(etName.getText()).length() <= 30
                && Objects.requireNonNull(etCity.getText()).length() <= 30
                && Objects.requireNonNull(etDescription.getText()).length() <= 300);
    }

    // custom class that implements TextWatcher to check if the form is valid everytime the text changes
    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            editTourBtn.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty()
                    && !Objects.requireNonNull(etCity.getText()).toString().isEmpty()
                    && Objects.requireNonNull(etName.getText()).length() <= 30
                    && Objects.requireNonNull(etCity.getText()).length() <= 30
                    && Objects.requireNonNull(etDescription.getText()).length() <= 300);
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }
    }


    /////    AsyncTasks Methods      //////

    @SuppressLint("StaticFieldLeak")
    class GetStopsTask extends AsyncTask<Void, Void, Either<ApiError, List<Stop>>> {

        @Override
        protected void onPreExecute() {
            progressBarStops.setVisibility(View.VISIBLE);
            cardViewStops.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Stop>> doInBackground(Void... voids) {
            return stopServices.getStopsByIdTour(tourToUpdate.getIdTour());
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Stop>> result) {
            progressBarStops.setVisibility(View.INVISIBLE);
            cardViewStops.setAlpha(1f);
            if (result.isRight()) {
                adapterStops.setItems(result.right().value());
                viewModel.getStopsLive().setValue(result.right().value());
                adapterStops.setDeleteVisible();
            } else {
                Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class EditTourTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.5f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return tourServices.updateTour(tourJSON);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
                Intent data = new Intent();
                setResult(RESULT_OK, data);
                finish();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
        }
    }
}
