package com.dafonseca.utour.dao;

import com.dafonseca.utour.models.Feedback;
import com.dafonseca.utour.retrofit.FeedbackAPI;
import com.dafonseca.utour.singleton.RetrofitSingleton;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

public class FeedbackDAO implements FeedbackAPI {

    private final FeedbackAPI feedbackAPI;
    private final Gson gson;

    public FeedbackDAO() {
        feedbackAPI = RetrofitSingleton.getInstance().getRetrofit().create(FeedbackAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<List<Feedback>> getFeedbacks() {
        return feedbackAPI.getFeedbacks();
    }

    @Override
    public Call<List<Feedback>> getFeedbackByIdTour(int idTour) {
        return feedbackAPI.getFeedbackByIdTour(idTour);
    }

    @Override
    public Call<String> addFeedback(Feedback feedback) {
        return feedbackAPI.addFeedback(feedback);
    }

    @Override
    public Call<String> updateFeedback(Feedback feedback) {
        return feedbackAPI.updateFeedback(feedback);
    }

    @Override
    public Call<String> deleteFeedback(int idFeedback) {
        return feedbackAPI.deleteFeedback(idFeedback);
    }


}
