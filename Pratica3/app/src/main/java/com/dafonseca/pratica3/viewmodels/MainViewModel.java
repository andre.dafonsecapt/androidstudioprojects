package com.dafonseca.pratica3.viewmodels;

import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {

    private boolean appStart = false;

    public boolean isAppStart() {
        return appStart;
    }

    public void setAppStart(boolean appStart) {
        this.appStart = appStart;
    }
}
