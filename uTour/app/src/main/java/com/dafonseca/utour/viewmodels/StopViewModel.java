package com.dafonseca.utour.viewmodels;

import android.graphics.Bitmap;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import lombok.Getter;

@Getter
public class StopViewModel extends ViewModel {

    private MutableLiveData<List<Bitmap>> imagesLive;
    private MutableLiveData<Boolean> audioExistsLive;

    public StopViewModel() {
        imagesLive = new MutableLiveData<>();
        audioExistsLive = new MutableLiveData<>();
    }
}
