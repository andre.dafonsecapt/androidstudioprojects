package com.dafonseca.pratica5.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Dificultad implements Parcelable {

    private String dificultad;
    private int image;

    public Dificultad(String dificultad, int image) {
        this.dificultad = dificultad;
        this.image = image;
    }

    private Dificultad(Parcel in){
        dificultad = in.readString();
        image = in.readInt();
    }

    public String getDificultad() {
        return dificultad;
    }

    public int getImage() {
        return image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dificultad);
        dest.writeInt(image);
    }

    public static final Parcelable.Creator<Dificultad> CREATOR
            = new Parcelable.Creator<Dificultad>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public Dificultad createFromParcel(Parcel in) {
            return new Dificultad(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public Dificultad[] newArray(int size) {
            return new Dificultad[size];
        }
    };
}
