package com.coycama.comerciales.ui.register

import android.view.View
import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.myFramework.ktExtensions.md5
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException

class RegisterViewModel(
    private val userRepository: UserRepository
) : ViewModel() {

    var name: String? = null
    var email: String? = null
    var password: String? = null
    var confirmPassword: String? = null

    var registerListener: RegisterListener? = null

    fun onRegisterBtnClick(view: View) {
        registerListener?.onStarted()
        if (name.isNullOrEmpty() || password.isNullOrEmpty() || confirmPassword.isNullOrEmpty() || email.isNullOrEmpty()) {
            registerListener?.onFailure("Error - Rellena todos los campos")
            return
        }

        if (!password.equals(confirmPassword)) {
            registerListener?.onFailure("Error - Contraseñas no coinciden")
            return
        }

        Coroutines.main {
            try {
                val loginResponse = userRepository.userRegister(name!!, email!!, password!!.md5())
                loginResponse.user?.let {
                    registerListener?.onSuccess(it)
                    userRepository.saveUser(it)
                    return@main
                }
                registerListener?.onFailure(loginResponse.message!!)
            } catch (e: ApiException) {
                registerListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                registerListener?.onFailure(e.message!!)
            }
        }

    }
}