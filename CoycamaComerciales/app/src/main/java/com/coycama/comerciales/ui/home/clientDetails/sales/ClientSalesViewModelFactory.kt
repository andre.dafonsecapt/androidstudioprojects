package com.coycama.comerciales.ui.home.clientDetails.sales

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.SalesRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.home.clients.ClientsViewModel
import com.coycama.comerciales.ui.home.profile.ProfileViewModel
import com.coycama.comerciales.ui.login.LoginViewModel

@Suppress("UNCHECKED_CAST")
class ClientSalesViewModelFactory (
    private val salesRepository: SalesRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClientSalesViewModel(salesRepository) as T
    }
}