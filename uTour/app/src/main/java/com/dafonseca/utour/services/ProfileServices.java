package com.dafonseca.utour.services;

import com.dafonseca.utour.dao.ProfileDAO;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Profile;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileServices {

    private final ProfileDAO dao;

    public ProfileServices() {
        this.dao = new ProfileDAO();
    }

    public Void incrementToursTaken(int idUser) throws IOException {
        Call<Void> call = dao.incrementToursTaken(idUser);
        Response<Void> resp = call.execute();
        Void voidObj = null;
        if (resp.isSuccessful()) {
            voidObj = resp.body();
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, resp.errorBody());
        }
        return voidObj;
    }

    public Void incrementToursRated(int idUser) throws IOException {
        Call<Void> call = dao.incrementToursRated(idUser);
        Response<Void> resp = call.execute();
        Void voidObj = null;
        if (resp.isSuccessful()) {
            voidObj = resp.body();
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, resp.errorBody());
        }
        return voidObj;
    }

    public Either<ApiError, Profile> getProfile(int idUser) throws IOException {
        Call<Profile> call = dao.getProfile(idUser);
        Response<Profile> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateProfile(Profile profile) throws IOException {
        Call<String> call = dao.updateProfile(profile);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, resp.errorBody());

            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> adminUpdateProfile(Profile profile) throws IOException {
        Call<String> call = dao.adminUpdateProfile(profile);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, resp.errorBody());

            }
            return Either.left(error);
        }
    }

}
