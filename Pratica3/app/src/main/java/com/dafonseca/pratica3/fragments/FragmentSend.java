package com.dafonseca.pratica3.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.dafonseca.pratica3.R;
import com.dafonseca.pratica3.viewmodels.SendViewModel;

public class FragmentSend extends Fragment {

    private EditText etDestino;
    private EditText etAsunto;
    private EditText etMensaje;
    private SendViewModel sendViewModel;

    public FragmentSend() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_send, container, false);

        sendViewModel = ViewModelProviders.of(this).get(SendViewModel.class);

        Button btnEnviar = rootView.findViewById(R.id.btnEnviar);
        etDestino = rootView.findViewById(R.id.etDestino);
        etAsunto = rootView.findViewById(R.id.etAsunto);
        etMensaje = rootView.findViewById(R.id.etMensaje);

        sendViewModel.getDestinoText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                etDestino.setText(s);
            }
        });

        sendViewModel.getAsuntoText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                etAsunto.setText(s);
            }
        });

        sendViewModel.getMensajeText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                etMensaje.setText(s);
            }
        });

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {etDestino.getText().toString()});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, etAsunto.getText().toString());
                emailIntent.putExtra(Intent.EXTRA_TEXT, etMensaje.getText().toString());
                startActivity(emailIntent);
            }
        });

        return rootView;
    }
}
