package com.dafonseca.recyclerviewwithcardview;

public class Album {

    private int imagen;
    private String cancion;
    private String album;

    public Album(int imagen, String cancion, String album) {
        this.imagen = imagen;
        this.cancion = cancion;
        this.album = album;
    }

    public int getImagen() {
        return imagen;
    }

    public String getCancion() {
        return cancion;
    }

    public String getAlbum() {
        return album;
    }
}
