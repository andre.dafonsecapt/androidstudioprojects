package com.dafonseca.utour.retrofit;

import com.dafonseca.utour.models.Feedback;
import com.dafonseca.utour.utils.ConstantsAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FeedbackAPI {

    @GET(ConstantsAPI.URL_FEEDBACK)
    Call<List<Feedback>> getFeedbacks();

    @GET(ConstantsAPI.URL_FEEDBACK_TOUR)
    Call<List<Feedback>> getFeedbackByIdTour(@Path("idTour") int idTour);

    @POST(ConstantsAPI.URL_FEEDBACK)
    Call<String> addFeedback(@Body Feedback feedback);

    @PUT(ConstantsAPI.URL_FEEDBACK)
    Call<String> updateFeedback(@Body Feedback feedback);

    @DELETE(ConstantsAPI.URL_FEEDBACK)
    Call<String> deleteFeedback(@Query("idFeedback") int idFeedback);
}
