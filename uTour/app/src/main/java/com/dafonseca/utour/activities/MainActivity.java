package com.dafonseca.utour.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.dafonseca.utour.R;
import com.dafonseca.utour.fragments.FeedbackSearchSplashFragment;
import com.dafonseca.utour.fragments.ProfileSplashFragment;
import com.dafonseca.utour.fragments.ToursSearchSplashFragment;
import com.dafonseca.utour.fragments.ToursUserSplashFragment;
import com.dafonseca.utour.fragments.UsersSearchSplashFragment;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.viewmodels.MainViewModel;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;

public class MainActivity extends AppCompatActivity {

    private MeowBottomNavigation bottomNavigation;
    private TextView tvAppTitle;
    private ImageView contentFrameImage;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setViews();
        addNavItems();
        setListeners();
        setViewModel();
    }

    private void setViews() {
        bottomNavigation = findViewById(R.id.bottomNav);
        tvAppTitle = findViewById(R.id.app_title);
        contentFrameImage = findViewById(R.id.content_frame_image);
    }

    private void addNavItems() {
        if (DataHolder.getInstance().isAdmin()) {
            bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_people));
            bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_map));
            bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_star));
        } else {
            bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_map));
            bottomNavigation.add(new MeowBottomNavigation.Model(5, R.drawable.ic_edit_location));
            bottomNavigation.add(new MeowBottomNavigation.Model(6, R.drawable.ic_profile));
        }
    }

    private void setListeners() {
        bottomNavigation.setOnClickMenuListener(menuItem -> {

            contentFrameImage.setVisibility(View.GONE);

            boolean fragmentTransaction = false;
            Fragment fragment = null;

            // switch fragment in main content frame
            switch (menuItem.getId()) {
                case 1:
                    fragment = new UsersSearchSplashFragment();
                    fragmentTransaction = true;
                    break;
                case 2:
                    fragment = new ToursSearchSplashFragment();
                    fragmentTransaction = true;
                    break;
                case 3:
                    fragment = new FeedbackSearchSplashFragment();
                    fragmentTransaction = true;
                    break;
                case 4:
                    fragment = new ToursSearchSplashFragment();
                    fragmentTransaction = true;
                    break;
                case 5:
                    fragment = new ToursUserSplashFragment();
                    fragmentTransaction = true;
                    break;
                case 6:
                    fragment = new ProfileSplashFragment();
                    fragmentTransaction = true;
                    break;
                default:
                    break;
            }
            if (fragmentTransaction) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .commit();
            }
        });

        bottomNavigation.setOnShowListener(item -> {
            // empty method
        });

        bottomNavigation.setOnReselectListener(item -> {
            // empty method
        });
    }

    private void setViewModel() {
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.getAppTitle().observe(this, s -> tvAppTitle.setText(s));
        viewModel.getMenuImageVisibility().observe(this, visibility -> contentFrameImage.setVisibility(visibility));
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.getMenuImageVisibility().setValue(contentFrameImage.getVisibility());
    }
}