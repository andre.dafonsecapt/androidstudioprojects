package com.dafonseca.utour.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.utils.AudioTools;
import com.dafonseca.utour.utils.ImageTools;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StopAdapter extends RecyclerView.Adapter<StopAdapter.ViewHolder> {

    private List<Stop> data;
    private ItemClickListener clickListener;
    private final ImageTools imageTools;
    private final AudioTools audioTools;
    private final Activity activity;
    private boolean drawablesEnd;

    public StopAdapter(List<Stop> data, Activity activity) {
        this.data = data;
        this.activity = activity;
        imageTools = new ImageTools();
        audioTools = new AudioTools();
        drawablesEnd = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_stops, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (!data.get(position).getImages().isEmpty()) {
            holder.image.setImageBitmap(imageTools.convertEncodedStringToBitmap(data.get(position).getImages().get(0)));
        } else {
            holder.image.setImageDrawable(activity.getDrawable(R.drawable.ic_image_placeholder));
        }
        holder.name.setText(data.get(position).getStopname());
        if (data.get(position).isDeleteVisible()) {
            holder.btnDelete.setVisibility(View.VISIBLE);
        } else {
            holder.btnDelete.setVisibility(View.GONE);
        }
        boolean isExpanded = data.get(position).isExpanded();
        if (isExpanded) {
            holder.expandableLayout.setVisibility(View.VISIBLE);
        } else {
            holder.expandableLayout.setVisibility(View.GONE);
        }
        if (drawablesEnd) {
            if (isExpanded) {
                holder.name.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up, 0);
            } else {
                holder.name.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down, 0);
            }
        }
        if (data.get(position).getText() != null && !data.get(position).getText().isEmpty()) {
            holder.textLayout.setVisibility(View.VISIBLE);
            holder.text.setText(data.get(position).getText());
        } else {
            holder.textLayout.setVisibility(View.GONE);
        }
        if (data.get(position).getAudio() != null && !data.get(position).getAudio().isEmpty()) {
            holder.audioLayout.setVisibility(View.VISIBLE);
            audioTools.writeAudioStringToFile(data.get(position).getAudio(), Objects.requireNonNull(activity.getExternalCacheDir()).getAbsolutePath() + "/stopAudio" + position + ".3gp");
        } else {
            holder.audioLayout.setVisibility(View.GONE);
        }
        List<Bitmap> stopImgList = new ArrayList<>();
        if (!data.get(position).getImages().isEmpty()) {
            for (String imgString : data.get(position).getImages()) {
                stopImgList.add(imageTools.convertEncodedStringToBitmap(imgString));
            }
            holder.galleryAdapter = new GalleryAdapter(stopImgList);
            holder.recyclerView.setAdapter(holder.galleryAdapter);
        } else {
            holder.galleryAdapter = null;
            holder.recyclerView.setAdapter(null);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public Stop getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Stop> getData() {
        return data;
    }

    public void setItems(List<Stop> items) {
        this.data = items;
        notifyDataSetChanged();
    }

    public void addItem(Stop stop) {
        data.add(stop);
        notifyDataSetChanged();
    }

    public void updateItem(Stop updateStop, int oldStopPosition) {
        data.remove(oldStopPosition);
        data.add(updateStop);
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    public void setDeleteVisible() {
        for (Stop s : data) {
            s.setDeleteVisible(true);
        }
        notifyDataSetChanged();
    }

    public void expandView(int position) {
        Stop stop = data.get(position);
        stop.setExpanded(!stop.isExpanded());
        stop.setEndDrawable(!stop.isEndDrawable());
        notifyItemChanged(position);
    }

    public void setDrawablesEnd(boolean value) {
        this.drawablesEnd = value;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final CardView cardView;
        final LinearLayout expandableLayout;
        final LinearLayout audioLayout;
        final ImageView image;
        final TextView name;
        final ImageButton btnDelete;
        final ProgressBar progressBar;
        final TextInputLayout textLayout;
        final TextInputEditText text;
        final RecyclerView recyclerView;
        private final long timeSwapBuff;
        private long startTime;
        private final TextView tvRecordTimer;
        private Timer timer;
        private final ImageButton audioPlayBtn;
        GalleryAdapter galleryAdapter;
        private boolean audioPlayStop;
        private MediaPlayer player;

        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.row_stops_cardview);
            expandableLayout = itemView.findViewById(R.id.row_stops_expandableLayout);
            audioLayout = itemView.findViewById(R.id.row_stops_audio_layout);
            image = itemView.findViewById(R.id.row_stops_image);
            name = itemView.findViewById(R.id.row_stops_name);
            btnDelete = itemView.findViewById(R.id.row_stops_btn_delete);
            progressBar = itemView.findViewById(R.id.row_stops_progbar);
            textLayout = itemView.findViewById(R.id.row_stops_text_layout);
            text = itemView.findViewById(R.id.row_stops_text);
            tvRecordTimer = itemView.findViewById(R.id.row_stops_recording_time_text);
            timeSwapBuff = 0L;
            audioPlayBtn = itemView.findViewById(R.id.row_stops_play_button);
            audioPlayStop = false;
            recyclerView = itemView.findViewById(R.id.row_stops_recyclerview);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            itemView.setOnClickListener(itemView1 -> {
                if (clickListener != null) {
                    clickListener.onItemClick(itemView1, getAdapterPosition());
                }
            });

            audioPlayBtn.setOnClickListener(v -> {
                if (audioPlayStop) {
                    stopPlaying();
                } else {
                    startPlaying();
                }
            });
            btnDelete.setOnClickListener(v -> new AlertDialog.Builder(v.getContext())
                    .setMessage(v.getContext().getString(R.string.row_stops_delete_stop_msg))
                    .setPositiveButton(v.getContext().getString(R.string.btn_yes),
                            (arg0, arg1) -> removeItem(getAdapterPosition()))
                    .setNegativeButton(v.getContext().getString(R.string.btn_cancel),
                            (arg0, arg1) -> arg0.cancel())
                    .show());
        }

        private void startPlaying() {
            player = new MediaPlayer();
            try {
                audioPlayBtn.setImageDrawable(activity.getDrawable(R.drawable.ic_stop));
                audioPlayStop = true;
                player.setDataSource(Objects.requireNonNull(activity.getExternalCacheDir()).getAbsolutePath() + "/stopAudio" + getAdapterPosition() + ".3gp");
                player.prepare();
                player.start();
                player.setOnCompletionListener(mediaPlayer -> stopPlaying());
                startTime = SystemClock.uptimeMillis();
                timer = new Timer();
                MyTimerTask myTimerTask = new MyTimerTask();
                timer.schedule(myTimerTask, 1000, 1000);
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
        }

        private void stopPlaying() {
            audioPlayStop = false;
            audioPlayBtn.setImageDrawable(activity.getDrawable(R.drawable.ic_play_arrow));
            player.release();
            player = null;
            if (timer != null) {
                timer.cancel();
            }
            if (tvRecordTimer.getText().toString().equals(activity.getString(R.string.stops_new_audio_timer))) {
                return;
            }
            tvRecordTimer.setText(activity.getString(R.string.stops_new_audio_timer));
        }

        // starts a timer to show the time elapsed when recording or playing
        class MyTimerTask extends TimerTask {

            @Override
            public void run() {
                long timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                long updatedTime = timeSwapBuff + timeInMilliseconds;
                final String hms = String.format(activity.getResources().getConfiguration().locale,
                        "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(updatedTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(updatedTime)),
                        TimeUnit.MILLISECONDS.toSeconds(updatedTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(updatedTime)));
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (tvRecordTimer != null) {
                                tvRecordTimer.setText(hms);
                            }
                        } catch (Exception e) {
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        }
                    }
                });
            }
        }
    }
}
