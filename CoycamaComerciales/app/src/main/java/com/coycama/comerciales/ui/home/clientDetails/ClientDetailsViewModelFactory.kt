package com.coycama.comerciales.ui.home.clientDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.LastDataRepository
import com.coycama.comerciales.data.repositories.SalesRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.home.clientDetails.sales.ClientSalesViewModel
import com.coycama.comerciales.ui.home.clients.ClientsViewModel
import com.coycama.comerciales.ui.home.profile.ProfileViewModel
import com.coycama.comerciales.ui.login.LoginViewModel

// serves to inject the viewModels with the repositories
@Suppress("UNCHECKED_CAST")
class ClientDetailsViewModelFactory (
    private val lastDataRepository: LastDataRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClientDetailsViewModel(lastDataRepository) as T
    }
}