package com.dafonseca.scrollalertsapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    CheckBox cb1;
    CheckBox cb2;
    CheckBox alertBox;
    CheckBox cb4;
    CheckBox cb5;
    CheckBox toastBox;
    CheckBox sundayBox;
    TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cb1 = findViewById(R.id.checkBox);
        cb2 = findViewById(R.id.checkBox2);
        alertBox = findViewById(R.id.checkBox3);
        cb4 = findViewById(R.id.checkBox4);
        cb5 = findViewById(R.id.checkBox5);
        toastBox = findViewById(R.id.checkBox6);
        sundayBox = findViewById(R.id.checkBox7);
        tvMessage = findViewById(R.id.textView2);

    }

    public void onSundayCheckBoxClicked(View view) {

        if (sundayBox.isChecked()) {
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.INVISIBLE);
        }
    }

    public void onToastCheckBoxClicked(View view) {
        if (toastBox.isChecked()) {
            Context context = getApplicationContext();
            Toast toast = Toast.makeText(context, "Here is a Toast to You!", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void onAlertCheckBoxclicked(View view) {
        final CheckBox cb = findViewById(view.getId());
        if (cb.isChecked()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage("Keep this box checked??");
            alert.setPositiveButton("YES", null);
            alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cb.setChecked(false);
                }
            });
            alert.show();
        }
    }
}
