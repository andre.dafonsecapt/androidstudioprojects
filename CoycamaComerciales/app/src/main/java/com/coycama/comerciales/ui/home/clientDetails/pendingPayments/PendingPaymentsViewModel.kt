package com.coycama.comerciales.ui.home.clientDetails.pendingPayments

import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import com.coycama.comerciales.data.repositories.PaymentsRepository
import com.coycama.comerciales.utils.lazyDeferred

class PendingPaymentsViewModel(
    private val paymentsRepository: PaymentsRepository
) : ViewModel() {

    var clientId = 0

    val payments by lazyDeferred {
        paymentsRepository.getPendingPaymentsByClient(GetByClientRequest(clientId))
    }
}