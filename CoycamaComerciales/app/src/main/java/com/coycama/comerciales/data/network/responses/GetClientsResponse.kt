package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.Client

data class GetClientsResponse(
    val code: Int,
    val records: List<Client>
)