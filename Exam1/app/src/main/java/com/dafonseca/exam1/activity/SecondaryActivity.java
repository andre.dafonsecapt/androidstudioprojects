package com.dafonseca.exam1.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.exam1.R;
import com.dafonseca.exam1.models.Pedido;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SecondaryActivity extends AppCompatActivity {

    private EditText etMesa;
    private CheckBox terraza;
    private RadioGroup radioGroup;
    private Spinner spinner;
    private EditText etComidas;
    private List<String> comidasList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        Toast.makeText(getApplicationContext(), "Fecha: " + getIntent().getStringExtra("fecha"), Toast.LENGTH_LONG).show();

        etMesa = findViewById(R.id.etMesa);
        terraza = findViewById(R.id.checkBox);
        etComidas = findViewById(R.id.etComidas);
        radioGroup = findViewById(R.id.radiogroup);
//        ((RadioButton)radioGroup.getChildAt(0)).setChecked(true);
        spinner = findViewById(R.id.spinner);
        List<String> spinnerArray = new ArrayList<String>();
        InputStream is;
        is = getResources().openRawResource(R.raw.comidas);
        Scanner sc = new Scanner(is);
        while (sc.hasNextLine()) {
            String comida = sc.nextLine();
            spinnerArray.add(comida);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        comidasList = new ArrayList<>();
    }

    public void agregarComida(View view) {
        etComidas.setText(etComidas.getText().toString() + "- " + spinner.getSelectedItem() + "\r\n");
        comidasList.add(spinner.getSelectedItem()+"");

    }

    public void altaPedido(View view) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Confirma el Pedido.");
        alert.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    int mesa = Integer.parseInt(etMesa.getText().toString());
                    String terraza_comedor;
                    if (terraza.isChecked()) {
                        terraza_comedor = "Terraza";
                    } else {
                        terraza_comedor = "Comedor";
                    }
                    RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
                    String camarero;
                    if (radioButton != null) {
                        camarero = radioButton.getText().toString();
                    } else {
                        camarero = "";
                    }
                    Pedido pedido = new Pedido(mesa, terraza_comedor, camarero);
                    pedido.addComidas(comidasList);
                    Intent datos = new Intent();
                    datos.putExtra("pedido", pedido);
                    setResult(RESULT_OK, datos);
                    finish();
                } catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Introduz un numero válido", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton("Cancelar", null);
        alert.show();
    }
}
