package com.dafonseca.utour.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserPostDTO {

    private String username;
    private String email;
    private String password;

}
