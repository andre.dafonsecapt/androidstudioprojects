package com.coycama.comerciales.ui.forgotPassword

import android.view.View
import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.myFramework.ktExtensions.md5
import com.coycama.comerciales.ui.register.RegisterListener
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException

class ForgotPasswordViewModel(
    private val userRepository: UserRepository
) : ViewModel() {

    var email: String? = null

    var registerListener: ForgotPasswordListener? = null

    fun onForgotPasswordBtnClick(view: View) {
        registerListener?.onStarted()

        if (email.isNullOrEmpty()) {
            registerListener?.onFailure("Error - Rellena el campo de email")
            return
        }

        registerListener?.onSuccess("Email enviado.")
//
//        Coroutines.main {
//            try {
//                val loginResponse = userRepository.userForgotPassword(email!!)
//                registerListener?.onSuccess(loginResponse.message!!)
//            } catch (e: ApiException) {
//                registerListener?.onFailure(e.message!!)
//            } catch (e: NoInternetException) {
//                registerListener?.onFailure(e.message!!)
//            }
//        }

    }
}