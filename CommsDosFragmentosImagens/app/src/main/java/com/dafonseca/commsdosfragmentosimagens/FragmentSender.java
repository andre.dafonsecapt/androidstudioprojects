package com.dafonseca.commsdosfragmentosimagens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class FragmentSender extends Fragment {

    View rootView;
    SendMessageListener sendMessageListener;


    public static FragmentSender newInstance() {
        return new FragmentSender();
    }

    /*La interfaz del fragmento*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fragmentsender, container, false);
        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("Select...");
        spinnerArray.add("imagen1");
        spinnerArray.add("imagen2");
        spinnerArray.add("imagen3");
        spinnerArray.add("imagen4");
        spinnerArray.add("imagen5");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner = rootView.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View
                    view, int position, long id) {
                Object item = adapterView.getItemAtPosition(position);
                if (item != null) {
                    if (sendMessageListener != null){
                        if (!item.toString().equals("Select...")) {
                            sendMessageListener.onSendImageString(item.toString());
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // empty method
            }
        });


        return rootView;
    }

    public void setClickListener(SendMessageListener sendMessageListener) {
        this.sendMessageListener = sendMessageListener;
    }

    public interface SendMessageListener {
        void onSendImageString(String image);
    }
}
