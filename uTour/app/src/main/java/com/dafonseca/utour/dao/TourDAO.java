package com.dafonseca.utour.dao;

import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.retrofit.TourAPI;
import com.dafonseca.utour.singleton.RetrofitSingleton;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

public class TourDAO implements TourAPI {

    private final TourAPI tourAPI;
    private final Gson gson;

    public TourDAO() {
        tourAPI = RetrofitSingleton.getInstance().getRetrofit().create(TourAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }


    @Override
    public Call<List<Tour>> getMyTours(int idUser) {
        return tourAPI.getMyTours(idUser);
    }

    @Override
    public Call<List<Tour>> getAllTours(int idUser) {
        return tourAPI.getAllTours(idUser);
    }

    @Override
    public Call<String> addTour(String tourJSON) {
        return tourAPI.addTour(tourJSON);
    }

    @Override
    public Call<String> updateTour(String tourJSON) {
        return tourAPI.updateTour(tourJSON);
    }

    @Override
    public Call<String> deleteTour(int idTour) {
        return tourAPI.deleteTour(idTour);
    }
}
