package com.coycama.comerciales.utils

object Constants {

    const val ACTION_START_OR_RESUME_SERVICE = "ACTION_START_OR_RESUME_SERVICE"
    const val ACTION_PAUSE_SERVICE = "ACTION_PAUSE_SERVICE"
    const val ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE"

    const val NOTIFICATION_CHANNEL_ID = "location_channel"
    const val NOTIFICATION_CHANNEL_NAME = "Locating"
    const val NOTIFICATION_ID = 1

    const val LOCATION_UPDATE_INTERVAL = 2000L
    const val FASTEST_LOCATION_INTERVAL = 1000L

    const val MAP_ZOOM = 15f

    const val LAST_ORDERS_TABLE_NAME = "lastOrders"
    const val LAST_SHIPMENTS_TABLE_NAME = "lastShipments"
    const val LAST_INCIDENTS_TABLE_NAME = "lastIncidents"
}