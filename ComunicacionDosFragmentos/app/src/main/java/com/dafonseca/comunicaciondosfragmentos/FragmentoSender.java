package com.dafonseca.comunicaciondosfragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

public class FragmentoSender extends Fragment {

    View rootView;
    SendMessageListener mSendMessageListener;
    Button send;
    EditText et;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fragmentosender, container,false);
        send = (Button) rootView.findViewById(R.id.send);
        et = rootView.findViewById(R.id.editText);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSendMessageListener != null){
                    mSendMessageListener.onSendMessage(et.getText().toString());
                }
            }
        });

        return rootView;
    }

    public interface SendMessageListener {
        void onSendMessage(String message);
    }

    public void setClickListener(SendMessageListener sendMessageListener) {
        this.mSendMessageListener = sendMessageListener;
    }



}
