package com.dafonseca.pratica3.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.pratica3.R;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List<Button> data;
    private ItemClickListener mClickListener;
    private Integer clickedPosition;

    public RecyclerAdapter(List<Button> data) {
        this.data = data;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private Button mapBtn;

        ViewHolder(View itemView) {
            super(itemView);
            mapBtn = itemView.findViewById(R.id.mapBtn);
            mapBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View itemView) {
                    if (mClickListener != null)
                        mClickListener.onItemClick(itemView, getAdapterPosition());
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.recyclerview_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mapBtn.setText(data.get(position).getText());
        if (clickedPosition != null && clickedPosition >= position){
            holder.mapBtn.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public Button getItem(int position) {
        return data.get(position);
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void disableItem(int position){
        clickedPosition = position;
        notifyDataSetChanged();
    }
}
