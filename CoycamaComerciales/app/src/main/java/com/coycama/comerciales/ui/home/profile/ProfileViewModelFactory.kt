package com.coycama.comerciales.ui.home.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.login.LoginViewModel

@Suppress("UNCHECKED_CAST")
class ProfileViewModelFactory (
    private val userRepository : UserRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ProfileViewModel(userRepository) as T
    }
}