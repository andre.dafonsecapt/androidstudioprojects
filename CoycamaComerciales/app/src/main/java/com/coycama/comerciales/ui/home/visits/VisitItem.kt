package com.coycama.comerciales.ui.home.visits

import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Visit
import com.coycama.comerciales.databinding.ItemVisitBinding
import com.xwray.groupie.viewbinding.BindableItem

class VisitItem(
    private val visit: Visit
) : BindableItem<ItemVisitBinding>() {

    override fun bind(viewBinding: ItemVisitBinding, position: Int) {
        viewBinding.visit = visit
    }

    override fun getLayout() = R.layout.item_visit

    override fun initializeViewBinding(view: View): ItemVisitBinding {
        return ItemVisitBinding.bind(view)
    }
}