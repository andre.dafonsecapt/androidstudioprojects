package com.coycama.comerciales.ui.home.clientDetails

import com.coycama.comerciales.data.models.LastData

interface LastDataItemListener {

    fun onLastDataItemClicked(data: LastData)
}