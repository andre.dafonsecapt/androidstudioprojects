package com.dafonseca.dosactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        String nombre = getIntent().getStringExtra("nombreS");
        TextView etiqueta = findViewById(R.id.etiqueta);
        etiqueta.setText("Hola " + nombre);

    }

    public void onResponder (View view){
        EditText e= findViewById(R.id.editText2);
        String r= e.getText().toString();
        Intent datos = new Intent();
        datos.putExtra("respuesta", r);
        setResult(RESULT_OK, datos);
        finish();
    }
}
