package com.coycama.comerciales.ui.forgotPassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.R
import com.coycama.comerciales.databinding.ActivityForgotPasswordBinding
import com.coycama.comerciales.databinding.ActivityRegisterBinding
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.ui.login.LoginViewModel
import com.coycama.comerciales.ui.login.LoginViewModelFactory
import com.coycama.comerciales.ui.register.RegisterViewModel
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ForgotPasswordActivity : AppCompatActivity(), ForgotPasswordListener, KodeinAware {

    override val kodein by kodein()

    // lifecycle elements
    private val factory: ForgotPasswordViewModelFactory by instance()
    private lateinit var viewModel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        bindViewModel()
        transformToolbar()
    }

    private fun bindViewModel() {
        val binding: ActivityForgotPasswordBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_forgot_password
        )
        viewModel = ViewModelProvider(this, factory).get(ForgotPasswordViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.registerListener = this
    }

    fun transformToolbar(){
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        forgot_password_progressBar.show()
    }

    override fun onSuccess(message: String) {
        forgot_password_progressBar.hide()
        toast(message)
    }

    override fun onFailure(message: String) {
        forgot_password_progressBar.hide()
        toast(message)
    }
}