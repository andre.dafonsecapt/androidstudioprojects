package com.dafonseca.pratica5.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {

            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            if (noConnectivity){
                Toast.makeText(context, "Desconectado.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Conectado.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
