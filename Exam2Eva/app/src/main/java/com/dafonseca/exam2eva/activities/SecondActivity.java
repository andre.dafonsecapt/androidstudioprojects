package com.dafonseca.exam2eva.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.dafonseca.exam2eva.R;
import com.dafonseca.exam2eva.db.PuntuacionesSQLiteHelper;
import com.dafonseca.exam2eva.models.Socio;
import com.dafonseca.exam2eva.services.ServicesApp;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends Activity {

    private EditText etNombre;
    private Spinner spinner;
    private RadioGroup radioGroup;
    private RadioButton rbSi;
    private EditText etTelefono;
    private PuntuacionesSQLiteHelper usdbh;
    private ServicesApp sa = new ServicesApp();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        etNombre = findViewById(R.id.etNombre);
        radioGroup = findViewById(R.id.radioGroup);
        rbSi = findViewById(R.id.rbSi);
        rbSi.setChecked(true);
        etTelefono = findViewById(R.id.etTelefono);

        usdbh = new PuntuacionesSQLiteHelper(this, getString(R.string.dbsocios), null, 1);

        setSpinner();
    }

    private void setSpinner() {
        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add(getString(R.string.adulto));
        spinnerArray.add(getString(R.string.infantil));
        spinnerArray.add(getString(R.string.parado));
        spinnerArray.add(getString(R.string.pensionista));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
    }

    public void darAlta(View view){
        Socio s = new Socio();
        s.setNombre(etNombre.getText().toString());
        s.setTipoCuota(spinner.getSelectedItem().toString());
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = radioGroup.findViewById(radioButtonID);
        String alergias = radioButton.getText().toString();
        s.setAlergias(alergias);
        s.setTelefono(etTelefono.getText().toString());
        String message;
        if (sa.addSocio(s, usdbh)){
            message = getString(R.string.socioAltaOK);
        } else {
            message = getString(R.string.socioAltaNO);

        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void volver(View view){
        finish();
    }
}
