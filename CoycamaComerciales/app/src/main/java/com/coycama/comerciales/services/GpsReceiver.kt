package com.coycama.comerciales.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.provider.Settings
import androidx.appcompat.app.AlertDialog

// broadcast receiver checks for changes in GPS or Network connection
class GpsReceiver : BroadcastReceiver() {

    internal var isGpsEnabled: Boolean = false
    internal var isNetworkEnabled: Boolean = false

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.action?.let { act ->
            if (act.matches("android.location.PROVIDERS_CHANGED".toRegex())) {
                val locationManager =
                    context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                //isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) - no need to check for network change for now

                if (!isGpsEnabled) {
                    AlertDialog.Builder(context)
                        .setTitle("Localización no disponible")
                        .setMessage("Activa el GPS para poder disfrutar del mapa")
                        .setCancelable(false)
                        .setNeutralButton("Cancelar") { dialog, _ ->
                        }
                        .setPositiveButton("Aceptar") { dialog, _ ->
                            context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                        }
                        .show()
                }
            }
        }
    }
}