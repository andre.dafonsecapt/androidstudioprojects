package com.coycama.comerciales.data.network

import com.coycama.comerciales.data.network.requests.GetByClientRequest
import com.coycama.comerciales.data.network.requests.GetByClientSubClientRequest
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import com.coycama.comerciales.data.network.requests.SetConsultaRequest
import com.coycama.comerciales.data.network.responses.*
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

// API interface to make calls to back end
interface MyApi {

    @FormUrlEncoded
    @POST("auth/checkEmail")
    suspend fun checkEmail(
        @Field("email") email: String
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("auth/validateLogin")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") pwd: String
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("auth/register")
    suspend fun userRegister(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("password") pwd: String
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("auth/forgotPassword")
    suspend fun userForgotPassword(
        @Field("email") email: String
    ): Response<AuthResponse>

    @POST("listaClientes/get_by_comercial")
    suspend fun getClientsByComercial(
        @Body body: GetByComercialRequest
    ): Response<GetClientsResponse>

    @POST("listaConsultas/set")
    suspend fun setConsulta(
        @Body body: SetConsultaRequest
    ): Response<SetConsultaResponse>

    @POST("listaVisitas/get_by_comercial")
    suspend fun getVisitsByComercial(
        @Body body: GetByComercialRequest
    ): Response<GetVisitasResponse>

    @POST("listaUltimoPedido/get_by_cliente_subcliente")
    suspend fun getLastOrdersByClientSubClient(
        @Body body: GetByClientSubClientRequest
    ): Response<GetLastDataResponse>

    @POST("listaUltimoEnvio/get_by_cliente_subcliente")
    suspend fun getLastShipmentsByClientSubClient(
        @Body body: GetByClientSubClientRequest
    ): Response<GetLastDataResponse>

    @POST("listaUltimaIncidencia/get_by_cliente_subcliente")
    suspend fun getLastIncidentsByClientSubClient(
        @Body body: GetByClientSubClientRequest
    ): Response<GetLastDataResponse>

    @POST("listaEstadisticaFacturacion/get_by_cliente")
    suspend fun getSalesByClient(
        @Body body: GetByClientRequest
    ): Response<GetSalesResponse>

    @POST("encuestaRespuestas/get_by_cliente")
    suspend fun getSurveysByClient(
        @Body body: GetByClientRequest
    ): Response<GetSurveysResponse>

    @POST("listaEstadisticaArticulo/get_by_cliente")
    suspend fun getGoodsByClient(
        @Body body: GetByClientRequest
    ): Response<GetGoodsResponse>

    @POST("listaPagosPendientes/get_by_cliente")
    suspend fun getPendingPaymentsByClient(
        @Body body: GetByClientRequest
    ): Response<GetPendingPaymentsResponse>

    // abstract method to build retrofit to be used throughout the app
    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): MyApi {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()
            return Retrofit.Builder()
                .baseUrl("http://comerciales.coycama.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
                .create(MyApi::class.java)
        }
    }
}