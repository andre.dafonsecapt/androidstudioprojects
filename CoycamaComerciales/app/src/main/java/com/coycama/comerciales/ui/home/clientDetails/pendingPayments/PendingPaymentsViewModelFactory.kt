package com.coycama.comerciales.ui.home.clientDetails.pendingPayments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.GoodsRepository
import com.coycama.comerciales.data.repositories.PaymentsRepository
import com.coycama.comerciales.data.repositories.SalesRepository

@Suppress("UNCHECKED_CAST")
class PendingPaymentsViewModelFactory (
    private val paymentsRepository: PaymentsRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PendingPaymentsViewModel(paymentsRepository) as T
    }
}