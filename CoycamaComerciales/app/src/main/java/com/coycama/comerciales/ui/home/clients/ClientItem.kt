package com.coycama.comerciales.ui.home.clients

import android.annotation.SuppressLint
import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.databinding.ItemClientBinding
import com.xwray.groupie.viewbinding.BindableItem

class ClientItem(
    private val client: Client,
    private val clientItemListener: ClientItemListener
) : BindableItem<ItemClientBinding>() {

    private var optionsVisible: Boolean = false

    @SuppressLint("ClickableViewAccessibility")
    override fun bind(viewBinding: ItemClientBinding, position: Int) {
        viewBinding.client = client

        viewBinding.itemClientInfoLayout.setOnClickListener {
            clientItemListener.onClientsInfoLayoutClicked(client)
        }

        viewBinding.itemClientMoreImageview.setOnClickListener {
            changeOptionsViewVisibility(viewBinding)
        }

        viewBinding.windowOptionsCallIcon.setOnClickListener {
            clientItemListener.onCallClicked(client)
        }

        viewBinding.windowOptionsDetailsIcon.setOnClickListener {
            clientItemListener.onDetailsClicked(client)
        }
        
        viewBinding.windowOptionsClearIcon.setOnClickListener {
            changeOptionsViewVisibility(viewBinding)
        }
    }

    override fun getLayout() = R.layout.item_client

    override fun initializeViewBinding(view: View): ItemClientBinding {
        return ItemClientBinding.bind(view)
    }

    private fun changeOptionsViewVisibility(viewBinding: ItemClientBinding){
        optionsVisible = !optionsVisible
        if (optionsVisible) {
            viewBinding.itemClientOptionsLayout2.visibility = View.VISIBLE
        } else {
            viewBinding.itemClientOptionsLayout2.visibility = View.GONE
        }
    }
}