package com.coycama.comerciales.data.models

data class Survey(
    val Fecha: String,
    val IdPregunta: String,
    val Respuesta: String,
    val Texto: String,
    val Visita: String
)