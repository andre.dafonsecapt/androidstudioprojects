package com.dafonseca.pratica3.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.dafonseca.pratica3.R;
import com.dafonseca.pratica3.fragments.FragmentHome;
import com.dafonseca.pratica3.fragments.FragmentSend;
import com.dafonseca.pratica3.fragments.FragmentShare;
import com.dafonseca.pratica3.viewmodels.MainViewModel;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainViewModel mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        if (!mainViewModel.isAppStart()) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new FragmentHome())
                    .commit();
            mainViewModel.setAppStart(true);
        }

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        boolean fragmentTransaction = false;
                        Fragment fragment = null;

                        switch (menuItem.getItemId()) {
                            case R.id.nav_home:
                                fragment = new FragmentHome();
                                fragmentTransaction = true;
                                break;
                            case R.id.nav_exit:
                                finish();
                                break;
                            case R.id.nav_share:
                                fragment = new FragmentShare();
                                fragmentTransaction = true;
                                break;
                            case R.id.nav_send:
                                fragment = new FragmentSend();
                                fragmentTransaction = true;
                                break;

                        }

                        if (fragmentTransaction) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, fragment)
                                    .commit();

                            menuItem.setChecked(true);
                            getSupportActionBar().setTitle(menuItem.getTitle());
                        }

                        drawer.closeDrawers();

                        return true;
                    }
                });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_webpage:

                Uri webpage = Uri.parse("https://4fotos-1palabra.com/");
                Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(webIntent);
                return true;
            case R.id.action_buscar:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = getLayoutInflater();
                final View inflator = inflater.inflate(R.layout.dialog_search, null);
                builder.setView(inflator)
                        .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                EditText et = inflator.findViewById(R.id.editText);
                                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                                intent.putExtra(SearchManager.QUERY, et.getText().toString());
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                }
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                Dialog dialogAlert = builder.create();
                dialogAlert.show();
                return true;
            case R.id.action_settings:
                Intent intentSettings = new Intent(Settings.ACTION_SETTINGS);
                if (intentSettings.resolveActivity(getPackageManager()) != null) {
                    startActivity(intentSettings);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
