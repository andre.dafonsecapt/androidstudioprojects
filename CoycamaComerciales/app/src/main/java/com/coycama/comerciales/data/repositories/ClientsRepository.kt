package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import com.coycama.comerciales.data.network.requests.SetConsultaRequest
import com.coycama.comerciales.data.network.responses.SetConsultaResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ClientsRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val clients = MutableLiveData<List<Client>>()

    // call an API request using kotlin's corountines to make the call asynchronous
    suspend fun getClientsByComercial(comercialId: GetByComercialRequest) : LiveData<List<Client>> {
        return withContext(Dispatchers.IO){
            fetchClientsByComercial(comercialId)
            clients
        }
    }

    private suspend fun fetchClientsByComercial(comercialId: GetByComercialRequest){
        val response = apiRequest { api.getClientsByComercial(comercialId) }
        clients.postValue(response.records)
    }

    // suspended tag in function is need as this is a asynchronous function
    suspend fun setConsulta(consulta: SetConsultaRequest): SetConsultaResponse{
        return apiRequest { api.setConsulta(consulta) }
    }
}