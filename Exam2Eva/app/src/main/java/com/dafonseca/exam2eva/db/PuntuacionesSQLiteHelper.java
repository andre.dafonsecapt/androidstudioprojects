package com.dafonseca.exam2eva.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PuntuacionesSQLiteHelper extends SQLiteOpenHelper {

    //Sentencia SQL para crear la tabla de Socios
    String sqlCreate = "CREATE TABLE Socios (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, tipocuota TEXT, alergias TEXT, telefono TEXT)";

    public PuntuacionesSQLiteHelper(Context contexto, String nombre,
                                    SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL(sqlCreate);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
//Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS Socios");
//Se crea la nueva versión de la tabla
        db.execSQL(sqlCreate);
    }

}
