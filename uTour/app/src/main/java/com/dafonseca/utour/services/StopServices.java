package com.dafonseca.utour.services;

import com.dafonseca.utour.dao.StopDAO;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Stop;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import retrofit2.Call;
import retrofit2.Response;

public class StopServices {

    private final StopDAO dao;

    public StopServices() {
        this.dao = new StopDAO();
    }

    public Gson getGson() {
        return dao.getGson();
    }

    public Either<ApiError, List<Stop>> getStopsByIdTour(int idTour) throws IOException {
        Call<List<Stop>> call = dao.getStopsByIdTour(idTour);
        Response<List<Stop>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }
}
