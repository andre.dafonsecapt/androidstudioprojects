package com.coycama.comerciales.data.network.responses

data class SetConsultaResponse(

    val code: Int,
    val msg: String
)
