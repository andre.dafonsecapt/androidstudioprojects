package com.coycama.comerciales.ui.home.clientDetails.surveys.list

import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Survey
import com.coycama.comerciales.data.models.Visit
import com.coycama.comerciales.databinding.ItemSurveyBinding
import com.coycama.comerciales.databinding.ItemVisitBinding
import com.xwray.groupie.viewbinding.BindableItem

class SurveyItem(
    private val survey: Survey
) : BindableItem<ItemSurveyBinding>() {

    override fun bind(viewBinding: ItemSurveyBinding, position: Int) {
        viewBinding.survey = survey
    }

    override fun getLayout() = R.layout.item_survey

    override fun initializeViewBinding(view: View): ItemSurveyBinding {
        return ItemSurveyBinding.bind(view)
    }
}