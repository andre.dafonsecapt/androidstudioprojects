package com.dafonseca.pratica3.activities;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.ViewModelProviders;

import com.dafonseca.pratica3.R;
import com.dafonseca.pratica3.fragments.FragmentHome;
import com.dafonseca.pratica3.fragments.FragmentImagen;
import com.dafonseca.pratica3.fragments.FragmentRecycler;
import com.dafonseca.pratica3.models.Dificultad;
import com.dafonseca.pratica3.viewmodels.GameViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameActivity extends AppCompatActivity {

    private static final int GAMESECONDARY_ACTIVITY_TAG = 1;
    Dificultad dificultadObject;
    private FragmentRecycler fragmentRecycler;
    private List<List<String>> respuestas;
    private NotificationManagerCompat notificationManager;
    private GameViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        fragmentRecycler = (FragmentRecycler) getSupportFragmentManager().findFragmentById(R.id.fragmentRecycler);

        viewModel = ViewModelProviders.of(this).get(GameViewModel.class);
        ImageView imageDificultad = findViewById(R.id.imageDificultad);
        respuestas = new ArrayList<>();
        respuestas.add(Arrays.asList("raiz", "alas", "cola"));
        respuestas.add(Arrays.asList("inglés", "pintar", "gestos"));
        respuestas.add(Arrays.asList("circuito", "gardenia", "comparar"));

        notificationManager = NotificationManagerCompat.from(this);

        dificultadObject = getIntent().getParcelableExtra("dificultadObject");
        imageDificultad.setImageResource(dificultadObject.getImage());
        viewModel.setDificultad(dificultadObject.getDificultad());

        for (int i = 0; i < viewModel.getNivel(); i++) {
            fragmentRecycler.getAdapter().disableItem(i);
        }

        FragmentImagen fragmentImagen = (FragmentImagen) getSupportFragmentManager().findFragmentById(R.id.fragmentImagen);
        if (fragmentImagen != null) {
            fragmentImagen.showImage("nivel1", viewModel.getDificultad());
        }
    }

    public int getNivel() {
        return viewModel.getNivel();
    }

    public int getSecondaryActivityTag() {
        return GAMESECONDARY_ACTIVITY_TAG;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String respuesta;
        String dificultad = viewModel.getDificultad();
        int dificultadInt = -1;
        switch (dificultad) {
            case "facil":
                dificultadInt = 0;
                break;
            case "medio":
                dificultadInt = 1;
                break;
            case "dificil":
                dificultadInt = 2;
                break;
        }
        if ((resultCode == RESULT_CANCELED)) {
            respuesta = "canceled";
        } else {
            respuesta = data.getStringExtra("respuesta");
            if (respuesta.equals(respuestas.get(dificultadInt).get(viewModel.getNivel()))) {
                Toast.makeText(getApplicationContext(), "Has acertado!!", Toast.LENGTH_LONG).show();
                viewModel.setNivel(viewModel.getNivel() + 1);
                fragmentRecycler.getAdapter().disableItem(data.getIntExtra("position", -1));
                if (viewModel.getNivel() > 2) {
                    long[] pattern = {0, 100, 200, 100, 200, 100};
                    Notification noti = new NotificationCompat.Builder(GameActivity.this, FragmentHome.CHANNEL_1_ID)
                            .setSmallIcon(R.drawable.ic_smiley)
                            .setContentTitle("Enhorabuenas!!")
                            .setContentText("Has completado todos los niveles en esta dificultad!")
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setVibrate(pattern)
                            .build();

                    notificationManager.notify(1, noti);
                }
            } else {
                Toast.makeText(getApplicationContext(), "Respuesta errada intenta otra vez!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onResponder(View view){
        EditText e = findViewById(R.id.etRespuesta);
        String respuesta = e.getText().toString();

        String dificultad = viewModel.getDificultad();
        int dificultadInt = -1;
        switch (dificultad) {
            case "facil":
                dificultadInt = 0;
                break;
            case "medio":
                dificultadInt = 1;
                break;
            case "dificil":
                dificultadInt = 2;
                break;
        }
        if (respuesta.equals(respuestas.get(dificultadInt).get(viewModel.getNivel()))) {
            Toast.makeText(getApplicationContext(), "Has acertado!!", Toast.LENGTH_LONG).show();
            viewModel.setNivel(viewModel.getNivel() + 1);
            fragmentRecycler.getAdapter().disableItem(viewModel.getNivel()-1);
            if (viewModel.getNivel() > 2) {
                long[] pattern = {0, 100, 200, 100, 200, 100};
                Notification noti = new NotificationCompat.Builder(GameActivity.this, FragmentHome.CHANNEL_1_ID)
                        .setSmallIcon(R.drawable.ic_smiley)
                        .setContentTitle("Enhorabuenas!!")
                        .setContentText("Has completado todos los niveles en esta dificultad!")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setVibrate(pattern)
                        .build();

                notificationManager.notify(1, noti);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Respuesta errada intenta otra vez!", Toast.LENGTH_LONG).show();
        }
    }
}
