package com.dafonseca.dosactividades;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int SECONDARY_ACTIVITY_TAG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPulsame(View view) {
        EditText e = findViewById(R.id.editText2);
        String s = e.getText().toString();
        if (s.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Pone algo de texto", Toast.LENGTH_LONG).show();
        } else {
            Intent i = new Intent(this, SecondaryActivity.class);
            i.putExtra("nombreS", s);
            startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String respuesta;
        if ((resultCode == RESULT_CANCELED) ||
                (data.getStringExtra("respuesta").equals(""))) {
            respuesta = "canceled";
        } else {
            respuesta = data.getStringExtra("respuesta");
        }
        Context contexto = getApplicationContext();
        Toast.makeText(contexto, respuesta, Toast.LENGTH_LONG).show();

    }
}
