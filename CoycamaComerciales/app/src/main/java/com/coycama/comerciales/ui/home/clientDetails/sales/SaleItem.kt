package com.coycama.comerciales.ui.home.clientDetails.sales

import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.Sale
import com.coycama.comerciales.databinding.ItemSaleBinding
import com.xwray.groupie.viewbinding.BindableItem

class SaleItem (
    private val sale: Sale
        ) : BindableItem<ItemSaleBinding>() {

    override fun bind(viewBinding: ItemSaleBinding, position: Int) {
        sale.Mes = when(sale.Mes){
            "1" -> "Enero"
            "2" -> "Febrero"
            "3" -> "Marzo"
            "4" -> "Abril"
            "5" -> "Mayo"
            "6" -> "Junio"
            "7" -> "Julio"
            "8" -> "Agosto"
            "9" -> "Septiembre"
            "10" -> "Octubre"
            "11" -> "Noviembre"
            "12" -> "Diciembre"
            else -> ""
        }
        viewBinding.sale = sale
    }

    override fun getLayout() = R.layout.item_sale

    override fun initializeViewBinding(view: View): ItemSaleBinding {
        return ItemSaleBinding.bind(view)
    }


}