package com.dafonseca.utour.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dafonseca.utour.R;
import com.dafonseca.utour.adapters.StopAdapter;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.BitmapCache;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.TourServices;
import com.dafonseca.utour.utils.ImageTools;
import com.dafonseca.utour.viewmodels.TourViewModel;
import com.google.android.material.textfield.TextInputEditText;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import lombok.SneakyThrows;

import static com.dafonseca.utour.utils.ConstantsClient.CAMERA_PERMISSION_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.CREATE_STOP_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_CAPTURE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_GALLERY;
import static com.dafonseca.utour.utils.ConstantsClient.UPDATE_STOP_CODE;

public class ToursNewActivity extends AppCompatActivity implements StopAdapter.ItemClickListener {

    private ScrollView scrollView;
    private ImageView imageView;
    private TextInputEditText etName;
    private TextInputEditText etDescription;
    private TextInputEditText etCity;
    private NumberPicker npHours;
    private NumberPicker npMinutes;
    private Button createTourBtn;
    private StopAdapter adapterStops;
    private ProgressBar progressBar;
    private TourServices tourServices;
    private Tour tour;
    private String tourJSON;
    private String oldStopPositionStr;
    private String imgListSizeStr;
    private ImageTools imageTools;
    private TourViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tours_new);
        setViews();
        initObjects();
        setListeners();
        initRecyclerView();
        setViewModel();
    }

    private void setViews() {
        scrollView = findViewById(R.id.tours_new_scrollview);
        imageView = findViewById(R.id.tours_new_img);
        etName = findViewById(R.id.tours_new_name);
        etDescription = findViewById(R.id.tours_new_description);
        etCity = findViewById(R.id.tours_new_city);
        npHours = findViewById(R.id.tours_new_hours);
        npHours.setMaxValue(999);
        npHours.setMinValue(0);
        npMinutes = findViewById(R.id.tours_new_minutes);
        npMinutes.setMaxValue(60);
        npMinutes.setMinValue(0);
        createTourBtn = findViewById(R.id.tours_new_create_tour_btn);
        progressBar = findViewById(R.id.tours_new_progbar);
    }

    private void initObjects() {
        tourServices = new TourServices();
        tour = new Tour();
        imageTools = new ImageTools();
        adapterStops = new StopAdapter(new ArrayList<>(), this);
    }

    private void setListeners() {
        etName.addTextChangedListener(new MyTextWatcher());
        etCity.addTextChangedListener(new MyTextWatcher());
        adapterStops.setClickListener(this);
        oldStopPositionStr = "oldStopPosition";
        imgListSizeStr = "imgListSize";
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = findViewById(R.id.tours_new_recyclerview_stops);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterStops);
    }

    private void setViewModel() {
        viewModel = new ViewModelProvider(this).get(TourViewModel.class);
        viewModel.getStopsLive().observe(this, stops -> adapterStops.setItems(stops));
        viewModel.getHourLive().observe(this, hour -> npHours.setValue(hour));
        viewModel.getMinuteLive().observe(this, minutes -> npMinutes.setValue(minutes));
    }

    public void onUploadPhotoBtnClick(View view) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else if (options[item].equals("Choose from Gallery")) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_IMAGE_CAPTURE || requestCode == REQUEST_IMAGE_GALLERY) {
                    Bitmap imageBitmap;
                    if (requestCode == REQUEST_IMAGE_CAPTURE) {
                        Bundle extras = data.getExtras();
                        imageBitmap = (Bitmap) Objects.requireNonNull(extras).get("data");
                        imageView.setImageBitmap(imageBitmap);
                    } else {
                        Uri imageUri = data.getData();
                        imageBitmap = imageTools.getCorrectlyOrientedImage(this, imageUri, 800);
                        Glide.with(this).load(imageUri).into(imageView);
                    }
                    imageBitmap = imageTools.getResizedBitmap(Objects.requireNonNull(imageBitmap), 1000);
                    tour.setImage(imageTools.convertBitmapToEncodedString(imageBitmap));
                } else if (requestCode == CREATE_STOP_CODE) {
                    Stop newStop = data.getParcelableExtra("stop");
                    if (newStop != null) {
                        int imgListSize = data.getIntExtra(imgListSizeStr, 0);
                        newStop.setImages(getCachedImages(imgListSize));
                        adapterStops.addItem(newStop);
                    }
                } else if (requestCode == UPDATE_STOP_CODE) {
                    Stop updatedStop = data.getParcelableExtra("stop");
                    if (updatedStop != null) {
                        int imgListSize = data.getIntExtra(imgListSizeStr, 0);
                        updatedStop.setImages(getCachedImages(imgListSize));
                        int oldStopPosition = data.getIntExtra(oldStopPositionStr, 0);
                        adapterStops.updateItem(updatedStop, oldStopPosition);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            Toast.makeText(this, "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    private List<String> getCachedImages(int imgListSize) {
        List<String> encodedImgList = new ArrayList<>();
        for (int i = 0; i < imgListSize; i++) {
            Bitmap cachedImg = BitmapCache.getInstance().getLru().get("stopCachedImg" + i);
            encodedImgList.add(imageTools.convertBitmapToEncodedString(cachedImg));
        }
        return encodedImgList;
    }

    /////    Click Events Methods      //////

    public void onAddStopBtnClick(View view) {
        Intent i = new Intent(this, StopsNewActivity.class);
        startActivityForResult(i, CREATE_STOP_CODE);
    }

    public void onCreateTourBtnClick(View view) {
        tour.setTourname(Objects.requireNonNull(etName.getText()).toString());
        tour.setDescription(Objects.requireNonNull(etDescription.getText()).toString());
        tour.setCity(Objects.requireNonNull(etCity.getText()).toString());
        int hours = npHours.getValue();
        int minutes = npMinutes.getValue();
        minutes = minutes + (hours * 60);
        tour.setDuration(minutes);
        tour.setFk_idUser(DataHolder.getInstance().getIdUser());
        tour.setStops(adapterStops.getData());
        tourJSON = tourServices.getGson().toJson(tour);
        CreateTourTask createTourTask = new CreateTourTask();
        createTourTask.execute();
    }

    @Override
    public void onItemClick(View view, int position) {
        Stop stopSelected = adapterStops.getItem(position);
        for (String encodedImg : stopSelected.getImages()) {
            BitmapCache.getInstance().getLru().put("stopCachedImg", imageTools.convertEncodedStringToBitmap(encodedImg));
        }
        Intent i = new Intent(this, StopsNewActivity.class);
        i.putExtra("stopSelected", stopSelected);
        i.putExtra(oldStopPositionStr, position);
        i.putExtra(imgListSizeStr, stopSelected.getImages().size());
        stopSelected.setImages(new ArrayList<>());
        startActivityForResult(i, UPDATE_STOP_CODE);
    }

    /////    Lifecycle Methods      //////

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.getHourLive().setValue(npHours.getValue());
        viewModel.getMinuteLive().setValue(npMinutes.getValue());
    }

    @Override
    protected void onResume() {
        super.onResume();
        createTourBtn.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty()
                && !Objects.requireNonNull(etCity.getText()).toString().isEmpty()
                && Objects.requireNonNull(etName.getText()).length() <= 30
                && Objects.requireNonNull(etCity.getText()).length() <= 30
                && Objects.requireNonNull(etDescription.getText()).length() <= 300);
    }

    /////    AsyncTasks Methods      //////

    @SuppressLint("StaticFieldLeak")
    class CreateTourTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.5f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return tourServices.addTour(tourJSON);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
                Intent data = new Intent();
                setResult(RESULT_OK, data);
                finish();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();

        }
    }

    // custom class that implements TextWatcher to check if form is valid everytime the text changes
    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            createTourBtn.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty()
                    && !Objects.requireNonNull(etCity.getText()).toString().isEmpty()
                    && Objects.requireNonNull(etName.getText()).length() <= 30
                    && Objects.requireNonNull(etCity.getText()).length() <= 30
                    && Objects.requireNonNull(etDescription.getText()).length() <= 300);
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }
    }
}
