package com.dafonseca.testdtt.map;

import android.app.Activity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public interface MapContract {

    interface View {
        void showAlert(String propertyToActivate);

        void showCustomDialog();

        void showCallIntent();

        void showInfoWindow(MarkerOptions myMarker, LatLng latLng, float DEFAULT_ZOOM);

    }

    interface Presenter {
        void handleListeners(String propertyToActivate);

        void handleCallButton();

        void handleCustomDialogCallButton();

        void setMarker(LatLng latLng, String address, float DEFAULT_ZOOM, Activity activity);

    }
}
