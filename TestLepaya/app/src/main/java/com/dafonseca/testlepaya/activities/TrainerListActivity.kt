package com.dafonseca.testlepaya.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dafonseca.testlepaya.R
import com.dafonseca.testlepaya.adapters.TrainerAdapter
import com.dafonseca.testlepaya.models.Trainer
import com.dafonseca.testlepaya.services.AppServices
import com.google.gson.Gson
import java.io.File

class TrainerListActivity : AppCompatActivity(), TrainerAdapter.ItemClickListener {

    private lateinit var appServices: AppServices
    private lateinit var adapter: TrainerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_trainer_list)
        appServices = AppServices()
        setRecyclerAdapter()
    }

    private fun setRecyclerAdapter() {
        val recyclerView = findViewById<RecyclerView>(R.id.rvTrainers)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = TrainerAdapter(dataSet(), this)
        adapter.setClickListener(this)
        recyclerView.adapter = adapter
    }

    private fun dataSet(): List<Trainer> {
        // check if there is a local JSON file and load trainers from correspondent File
        val file = File(filesDir,"trainersLocal.json")
        if (file.exists()){
            return appServices.getTrainersFromLocalJSON(applicationContext)
        } else {
            return appServices.getTrainersFromJSON(applicationContext)
        }
    }

    override fun onItemClick(view: View?, position: Int) {
        val trainer: Trainer = adapter.getItem(position)
        val bundle = Bundle()
        bundle.putParcelable("trainer", trainer)
        val i = Intent(this, TrainerDetailsActivity::class.java)
        i.putExtra("myBundle", bundle)
        startActivityForResult(i, TRAINER_DETAILS_ACTIVITY_TAG)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val trainer: Trainer = data!!.getParcelableExtra("trainer") as Trainer
            adapter.updateItem(trainer)
        }
    }

    companion object {
        private const val TRAINER_DETAILS_ACTIVITY_TAG = 1
    }

    override fun onStop() {
        // save trainers to Json File if the app is finishing
        if (isFinishing) {
            val trainers = adapter.getAllItems()
            val gson = Gson()
            val jsonString: String = gson.toJson(trainers)
            appServices.writeToLocalFile(this, jsonString)
        }
        super.onStop()
    }
}
