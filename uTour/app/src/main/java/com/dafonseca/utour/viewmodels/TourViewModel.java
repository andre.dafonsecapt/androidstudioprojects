package com.dafonseca.utour.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dafonseca.utour.models.Stop;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class TourViewModel extends ViewModel {

    private MutableLiveData<List<Stop>> stopsLive;
    private MutableLiveData<Integer> hourLive;
    private MutableLiveData<Integer> minuteLive;

    public TourViewModel() {
        stopsLive = new MutableLiveData<>();
        stopsLive.setValue(new ArrayList<>());
        hourLive = new MutableLiveData<>();
        minuteLive = new MutableLiveData<>();
    }
}
