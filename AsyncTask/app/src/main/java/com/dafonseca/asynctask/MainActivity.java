package com.dafonseca.asynctask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private TextView tvPulsar;
    private TextView tvPrimos;
    private Button btnPulsar;
    private Button btnCalcular;
    private int pulsos;
    private CalcularPrimos cp;
    private ProgressBar pbPrimos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvPulsar = findViewById(R.id.tvPulsar);
        tvPrimos = findViewById(R.id.tvPrimos);
        btnPulsar = findViewById(R.id.btnPulsar);
        btnCalcular = findViewById(R.id.btnCalcular);
        pbPrimos = findViewById(R.id.pbPrimos);
        pulsos = 0;
    }

    public void pulsar(View view) {
        pulsos++;
        tvPulsar.setText("Pulsados: " + pulsos);
    }

    public void calcularPrimos(View view) {
        tvPrimos.setText("Primos: ");
        if (cp == null) {
            cp = new CalcularPrimos();
            cp.execute();
        } else {
            cp.cancel(true);
        }
    }

    class CalcularPrimos extends AsyncTask<Void, Integer, Long> {
        public /*static*/ boolean esPrimo(long i) {
            if (i == 1) return false;
            if (i < 4) return true;
            if ((i % 2) == 0 || (i % 3) == 0) return false;
            if (i < 9) return true;
            long n = 5;
            while (n * n <= i && i % n != 0 && i % (n + 2) != 0)
                n += 6;
            return (n * n > i);
        }

        public /*static*/ long cuantosPrimos(long limite) {
            long result = 0;
            long lenStep = limite / 100;
            int prevProgress = -1;
            for (long i = 1; i <= limite; ++i) {
                if (esPrimo(i)) {
                    ++result;
                }
                if (isCancelled()) {
                    break;
                }
                if (prevProgress != i / lenStep) {
                    prevProgress = (int)(i / lenStep);
                    publishProgress(prevProgress);
                }
            }
            return result;
        }

        @Override
        protected Long doInBackground(Void... params) {
            return cuantosPrimos(10000000);
        }

        @Override
        protected void onPreExecute() {
            pbPrimos.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            pbPrimos.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Long resultado) {
            terminar(resultado.toString());
        }

        @Override
        protected void onCancelled(Long resultadoParcial) {
            terminar("¡Cancelado! Llevaba " + resultadoParcial +" primos!");
        }


        protected void terminar(String s) {
            tvPrimos.setText("Primos: "+s);
            pbPrimos.setVisibility(View.INVISIBLE);
            cp = null;
        }

    } // CalcularPrimos asíncrona
}
