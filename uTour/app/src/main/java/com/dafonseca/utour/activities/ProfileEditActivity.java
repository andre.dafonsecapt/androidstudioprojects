package com.dafonseca.utour.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.dafonseca.utour.R;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.BitmapCache;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Profile;
import com.dafonseca.utour.models.UserPostDTO;
import com.dafonseca.utour.services.ProfileServices;
import com.dafonseca.utour.services.UserServices;
import com.dafonseca.utour.utils.ImageTools;
import com.google.android.material.textfield.TextInputEditText;
import com.hbb20.CountryCodePicker;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.hdodenhof.circleimageview.CircleImageView;
import fj.data.Either;
import lombok.SneakyThrows;

import static com.dafonseca.utour.utils.ConstantsClient.CAMERA_PERMISSION_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_CAPTURE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_GALLERY;

public class ProfileEditActivity extends AppCompatActivity {

    private ScrollView scrollView;
    private CircleImageView profileImg;
    private TextInputEditText etName;
    private TextInputEditText etEmail;
    private TextInputEditText etNewPwd;
    private TextInputEditText etNewPwdConfirm;
    private TextView tvWrongPwd;
    private CountryCodePicker ccpNationality;
    private CountryCodePicker ccpPhoneCode;
    private RadioGroup radioGroup;
    private RadioButton rbMale;
    private RadioButton rbFemale;
    private RadioButton rbOther;
    private TextInputEditText etPhone;
    private Button btnPwd;
    private Button btnSave;
    private ProgressBar progressBar;
    private ProfileServices profileServices;
    private UserServices userServices;
    private Profile profileToUpdate;
    private String gender;
    private ImageTools imageTools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        setViews();
        initObjects();
        setInitFields();
        setListeners();
    }

    private void setViews() {
        scrollView = findViewById(R.id.profile_edit_scrollview);
        profileImg = findViewById(R.id.profile_edit_img);
        etName = findViewById(R.id.profile_edit_name);
        etEmail = findViewById(R.id.profile_edit_email);
        etNewPwd = findViewById(R.id.profile_edit_newPwd);
        etNewPwdConfirm = findViewById(R.id.profile_edit_newPwdConfirm);
        tvWrongPwd = findViewById(R.id.profile_edit_wrong_pwd);
        tvWrongPwd.setVisibility(View.GONE);
        ccpNationality = findViewById(R.id.profile_edit_nationality);
        ccpPhoneCode = findViewById(R.id.profile_edit_phone_code);
        radioGroup = findViewById(R.id.profile_edit_radio_group);
        rbMale = findViewById(R.id.profile_edit_radio_male);
        rbFemale = findViewById(R.id.profile_edit_radio_female);
        rbOther = findViewById(R.id.profile_edit_radio_other);
        etPhone = findViewById(R.id.profile_edit_phone);
        btnPwd = findViewById(R.id.profile_edit_btn_pwd);
        btnSave = findViewById(R.id.profile_edit_btn_save);
        progressBar = findViewById(R.id.profile_edit_progbar);
        ccpPhoneCode.registerCarrierNumberEditText(etPhone);
    }

    private void initObjects() {
        userServices = new UserServices();
        profileServices = new ProfileServices();
        profileToUpdate = new Profile();
        imageTools = new ImageTools();
    }

    private void setInitFields() {
        Profile initProfile = getIntent().getParcelableExtra("profile");
        if (initProfile != null) {
            //set profile picture
            if (BitmapCache.getInstance().getLru().get("profileImg") != null) {
                Bitmap profileBitmap = BitmapCache.getInstance().getLru().get("profileImg");
                profileImg.setImageBitmap(profileBitmap);
                profileToUpdate.setPicture(imageTools.convertBitmapToEncodedString(profileBitmap));
            }
            // set name
            etName.setText(initProfile.getName());
            // set nationality
            if (initProfile.getNationality() != null && !initProfile.getNationality().isEmpty()) {
                ccpNationality.setCountryForNameCode(initProfile.getNationality());
            } else {
                ccpNationality.setCountryForNameCode(Locale.getDefault().getCountry());
            }
            // set gender
            if (initProfile.getGender() != null && !initProfile.getGender().isEmpty()) {
                switch (initProfile.getGender()) {
                    case "Male":
                        rbMale.setChecked(true);
                        break;
                    case "Female":
                        rbFemale.setChecked(true);
                        break;
                    case "Other":
                        rbOther.setChecked(true);
                        break;
                    default:
                        break;
                }
            }
            // set phone number
            if (initProfile.getPhone() != null && !initProfile.getPhone().isEmpty()) {
                ccpPhoneCode.setFullNumber(initProfile.getPhone());
            }
        }
        // set email
        etEmail.setText(DataHolder.getInstance().getEmail());
    }

    private void setListeners() {
        etName.addTextChangedListener(new FormTextWatcher());
        etPhone.addTextChangedListener(new FormTextWatcher());
        etNewPwd.addTextChangedListener(new PasswordTextWatcher());
        etNewPwdConfirm.addTextChangedListener(new PasswordTextWatcher());
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton selectedRadioButton = findViewById(checkedId);
            gender = selectedRadioButton.getText().toString();
            btnSave.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty() && Objects.requireNonNull(etName.getText()).length() <= 15);
        });
    }

    public void onNewImageBtnClick(View view) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else if (options[item].equals("Choose from Gallery")) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Bitmap imageBitmap = null;
                if (requestCode == REQUEST_IMAGE_CAPTURE) {
                    Bundle extras = data.getExtras();
                    imageBitmap = (Bitmap) Objects.requireNonNull(extras).get("data");
                    profileImg.setImageBitmap(imageBitmap);
                } else if (requestCode == REQUEST_IMAGE_GALLERY) {
                    Uri imageUri = data.getData();
                    imageBitmap = imageTools.getCorrectlyOrientedImage(this, imageUri, 800);
                    Glide.with(this).load(imageUri)
                            .fitCenter()
                            .override(500, 500)
                            .into(profileImg);
                }
                imageBitmap = imageTools.getResizedBitmap(Objects.requireNonNull(imageBitmap), 800);
                profileToUpdate.setPicture(imageTools.convertBitmapToEncodedString(imageBitmap));
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            Toast.makeText(this, "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    public void onChangePwdBtnClick(View view) {
        ChangePasswordTask task = new ChangePasswordTask();
        task.execute();
    }

    public void onSaveProfileBtnClick(View view) {
        // set attributes to profile
        profileToUpdate.setName(Objects.requireNonNull(etName.getText()).toString());
        profileToUpdate.setNationality(ccpNationality.getSelectedCountryNameCode());
        profileToUpdate.setGender(gender);
        profileToUpdate.setPhone(ccpPhoneCode.getFullNumberWithPlus());
        profileToUpdate.setFk_idUser(DataHolder.getInstance().getIdUser());
        // launch task
        SaveProfileTask task = new SaveProfileTask();
        task.execute();
    }

    @Override
    protected void onResume() {
        btnSave.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty() && Objects.requireNonNull(etName.getText()).length() <= 15);
        super.onResume();
    }

    @SuppressLint("StaticFieldLeak")
    class ChangePasswordTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.5f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            UserPostDTO userToPost = new UserPostDTO();
            userToPost.setEmail(DataHolder.getInstance().getEmail());
            userToPost.setPassword(Objects.requireNonNull(etNewPwd.getText()).toString());
            return userServices.changePwd(userToPost);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class SaveProfileTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.5f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return profileServices.updateProfile(profileToUpdate);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
        }
    }

    // custom class that implements TextWatcher to check if password form is valid everytime the text changes
    class PasswordTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!Objects.requireNonNull(etNewPwd.getText()).toString().isEmpty() && !Objects.requireNonNull(etNewPwdConfirm.getText()).toString().isEmpty()) {
                if (Objects.requireNonNull(etNewPwd.getText()).length() >= 6 && Objects.requireNonNull(etNewPwdConfirm.getText()).length() >= 6 && etNewPwd.getText().toString().equals(etNewPwdConfirm.getText().toString())) {
                    tvWrongPwd.setVisibility(View.GONE);
                    btnPwd.setEnabled(true);
                } else {
                    tvWrongPwd.setVisibility(View.VISIBLE);
                    btnPwd.setEnabled(false);
                }
            } else {
                btnPwd.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }
    }

    // custom class that implements TextWatcher to check if form is valid everytime the text changes
    class FormTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            btnSave.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty() && Objects.requireNonNull(etName.getText()).length() <= 15);
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }
    }
}
