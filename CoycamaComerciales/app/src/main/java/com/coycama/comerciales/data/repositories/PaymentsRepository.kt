package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.PendingPayment
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PaymentsRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val payments = MutableLiveData<List<PendingPayment>>()

    suspend fun getPendingPaymentsByClient(clientId: GetByClientRequest) : LiveData<List<PendingPayment>> {
        return withContext(Dispatchers.IO){
            fetchPendingPaymentsByClient(clientId)
            payments
        }
    }

    private suspend fun fetchPendingPaymentsByClient(clientId: GetByClientRequest){
        val response = apiRequest { api.getPendingPaymentsByClient(clientId) }
        payments.postValue(response.records)
    }
}