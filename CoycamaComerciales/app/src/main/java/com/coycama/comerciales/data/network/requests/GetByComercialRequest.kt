package com.coycama.comerciales.data.network.requests

import com.google.gson.annotations.SerializedName

data class GetByComercialRequest(
    @SerializedName("Comercial") var comercial: Int
)