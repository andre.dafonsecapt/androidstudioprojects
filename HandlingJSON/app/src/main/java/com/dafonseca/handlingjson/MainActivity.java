package com.dafonseca.handlingjson;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.icu.text.SymbolTable;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            // write to JSON
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObj1 = new JSONObject();
            jsonObj1.put("firstName", "Pepe");
            jsonObj1.put("lastName", "Sanchez");
            JSONObject jsonObj2 = new JSONObject();
            jsonObj2.put("firstName", "Pepita");
            jsonObj2.put("lastName", "Suarez");
            jsonArray.put(jsonObj1);
            jsonArray.put(jsonObj2);

            JSONObject jsonObjRes = new JSONObject();
            jsonObjRes.put("employees", jsonArray);

            FileOutputStream fos;
            fos = openFileOutput("employees.json", Context.MODE_PRIVATE);

            OutputStreamWriter out;
            out = new OutputStreamWriter(fos);

            out.write(jsonObjRes.toString());
            out.close();

            // read from JSON

            StringBuilder strBuild = new StringBuilder();
            String line;
            FileInputStream fis;
            fis = openFileInput("employees.json");

            Scanner scanner = new Scanner(fis);
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                strBuild.append(line + "\n");
            }
            scanner.close();

            String jsonStr = strBuild.toString();

            JSONObject jsonObj = new JSONObject(jsonStr);

            JSONArray jsonArrayGen = jsonObj.getJSONArray("employees");

            List<Employee> listEmp = new ArrayList<>();

            for (int i = 0; i < jsonArrayGen.length(); i++) {
                JSONObject c = jsonArrayGen.getJSONObject(i);
                listEmp.add(new Employee(c.getString("firstName"), c.getString("lastName")));
            }

            TextView tvText = findViewById(R.id.tvText);
            tvText.setText(listEmp.toString());
        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
