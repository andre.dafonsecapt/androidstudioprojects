package com.dafonseca.testlepayajava.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.List;

public class Trainer implements Parcelable {

    private int index;
    private String guid;
    private String name;
    private String picture;
    private String email;
    private boolean isAvailable;
    private String favoriteFruit;
    private List<String> skills;

    public Trainer() {
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private Trainer(Parcel in) {
        index = in.readInt();
        guid = in.readString();
        name = in.readString();
        picture = in.readString();
        email = in.readString();
        isAvailable = in.readBoolean();
        favoriteFruit = in.readString();
        skills = in.createStringArrayList();

    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getFavoriteFruit() {
        return favoriteFruit;
    }

    public void setFavoriteFruit(String favoriteFruit) {
        this.favoriteFruit = favoriteFruit;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeString(guid);
        dest.writeString(name);
        dest.writeString(picture);
        dest.writeString(email);
        dest.writeBoolean(isAvailable);
        dest.writeString(favoriteFruit);
        dest.writeStringList(skills);
    }

    public static final Parcelable.Creator<Trainer> CREATOR
            = new Parcelable.Creator<Trainer>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public Trainer createFromParcel(Parcel in) {
            return new Trainer(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public Trainer[] newArray(int size) {
            return new Trainer[size];
        }
    };
}
