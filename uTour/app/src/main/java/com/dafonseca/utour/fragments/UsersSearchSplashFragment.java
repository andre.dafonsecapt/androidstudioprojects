package com.dafonseca.utour.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dafonseca.utour.R;
import com.dafonseca.utour.activities.UserEditActivity;
import com.dafonseca.utour.adapters.UserAdapter;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.UserGetDTO;
import com.dafonseca.utour.services.UserServices;
import com.dafonseca.utour.viewmodels.MainViewModel;

import java.util.ArrayList;
import java.util.List;

import fj.data.Either;
import lombok.SneakyThrows;

public class UsersSearchSplashFragment extends Fragment implements UserAdapter.ItemClickListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private UserAdapter userAdapter;
    private RecyclerView recyclerView;
    private UserServices userServices;
    private List<UserGetDTO> users;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_users_search_splash, container, false);

        setViews(rootView);
        setTitle();
        initObjects();
        setRecyclerAdapter(rootView);
        setListeners();
        launchGetToursTask();

        return rootView;
    }

    private void setViews(View rootView) {
        swipeRefreshLayout = rootView.findViewById(R.id.users_splash_swipe_refresh);
        recyclerView = rootView.findViewById(R.id.users_splash_recycler);
    }

    private void setTitle() {
        MainViewModel viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        viewModel.getAppTitle().setValue(getString(R.string.title_users));
    }

    private void initObjects() {
        users = new ArrayList<>();
        userServices = new UserServices();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
    }

    private void setRecyclerAdapter(View rootView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        userAdapter = new UserAdapter(getContext());
        userAdapter.setClickListener(this);
        recyclerView.setAdapter(userAdapter);
    }

    private void setListeners() {
        swipeRefreshLayout.setOnRefreshListener(this::launchGetToursTask);
    }

    private void launchGetToursTask() {
        GetUsersTask task = new GetUsersTask();
        task.execute();
    }

    @Override
    public void onItemClick(View view, int position) {
        UserGetDTO userSelected = userAdapter.getItem(position);
        Intent i = new Intent(getContext(), UserEditActivity.class);
        i.putExtra("userSelected", userSelected);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
        launchGetToursTask();
    }

    @SuppressLint("StaticFieldLeak")
    class GetUsersTask extends AsyncTask<Void, Void, Either<ApiError, List<UserGetDTO>>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<UserGetDTO>> doInBackground(Void... voids) {
            return userServices.getUsers();
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<UserGetDTO>> result) {
            swipeRefreshLayout.setRefreshing(false);
            if (result.isRight()) {
                users = result.right().value();
                userAdapter.setItems(users);
            } else {
                Toast.makeText(getContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
