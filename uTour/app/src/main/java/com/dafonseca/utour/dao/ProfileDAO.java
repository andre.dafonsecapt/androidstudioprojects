package com.dafonseca.utour.dao;

import com.dafonseca.utour.models.Profile;
import com.dafonseca.utour.retrofit.ProfileAPI;
import com.dafonseca.utour.singleton.RetrofitSingleton;
import com.google.gson.Gson;

import retrofit2.Call;

public class ProfileDAO implements ProfileAPI {

    private final ProfileAPI profileAPI;
    private final Gson gson;

    public ProfileDAO() {
        profileAPI = RetrofitSingleton.getInstance().getRetrofit().create(ProfileAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<Void> incrementToursTaken(int idUser) {
        return profileAPI.incrementToursTaken(idUser);
    }

    @Override
    public Call<Void> incrementToursRated(int idUser) {
        return profileAPI.incrementToursRated(idUser);
    }

    @Override
    public Call<Profile> getProfile(int idUser) {
        return profileAPI.getProfile(idUser);
    }

    @Override
    public Call<String> updateProfile(Profile profile) {
        return profileAPI.updateProfile(profile);
    }

    @Override
    public Call<String> adminUpdateProfile(Profile profile) {
        return profileAPI.adminUpdateProfile(profile);
    }

}
