package com.dafonseca.utour.models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Profile implements Parcelable {

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
    private int idProfile;
    private String picture;
    private String name;
    private String nationality;
    private String phone;
    private String gender;
    private double rating;
    private int fk_idUser;
    private int myTours;
    private int toursTaken;
    private int toursRated;

    public Profile() {
        picture = "";
    }

    private Profile(Parcel in) {
        idProfile = in.readInt();
        picture = in.readString();
        name = in.readString();
        nationality = in.readString();
        phone = in.readString();
        gender = in.readString();
        fk_idUser = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idProfile);
        dest.writeString(picture);
        dest.writeString(name);
        dest.writeString(nationality);
        dest.writeString(phone);
        dest.writeString(gender);
        dest.writeInt(fk_idUser);
    }
}
