package com.dafonseca.exam1.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.dafonseca.exam1.R;
import com.dafonseca.exam1.models.Pedido;

public class FragmentPedido extends Fragment {

    View rootView;

    EditText textArea;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frament_pedido, container, false);

        textArea = rootView.findViewById(R.id.textArea);

        return rootView;
    }

    public void showPedido(String pedido){
        textArea.setText(pedido);
    }

}

