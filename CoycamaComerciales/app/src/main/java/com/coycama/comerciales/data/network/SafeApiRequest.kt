package com.coycama.comerciales.data.network

import com.coycama.comerciales.utils.ApiException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

// wrapper class for easy handling of api errors
abstract class SafeApiRequest {

    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): T {
        val response = call.invoke()
        if (response.isSuccessful) {
            return response.body()!!
        } else {
            val error = response.errorBody()?.string()
            val message = StringBuilder()
            error?.let{
                try {
                    message.append(JSONObject(error).getString("message"))
                } catch (e: JSONException) {
                    message.append(response.errorBody()?.string())
                }
                message.append("\n")
            }
            message.append("Error Code: ${response.code()}")

            throw ApiException(message.toString())
        }
    }
}