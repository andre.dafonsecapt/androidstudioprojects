package com.coycama.comerciales.ui.login

import com.coycama.comerciales.data.db.entities.User

interface LoginListener {
    fun onStarted()
    fun onSuccess(user: User)
    fun onFailure(message: String)
    fun onRegisterClicked()
    fun onForgotPasswordClicked()
}