package com.dafonseca.testlepayajava.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.testlepayajava.R;
import com.dafonseca.testlepayajava.models.Trainer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TrainerAdapter extends RecyclerView.Adapter<TrainerAdapter.ViewHolder> {

    private List<Trainer> data;
    private ItemClickListener clickListener;

    public TrainerAdapter(List<Trainer> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.recyclerview_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get().load(data.get(position).getPicture()).into(holder.picture);
        holder.name.setText(data.get(position).getName());
        holder.email.setText(data.get(position).getEmail());
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public Trainer getItem(int position){
        return data.get(position);
    }

    public void updateItem(Trainer trainer){

        data.get(trainer.getIndex()).setName(trainer.getName());
        data.get(trainer.getIndex()).setEmail(trainer.getEmail());
        data.get(trainer.getIndex()).setAvailable(trainer.isAvailable());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView picture;
        public TextView name;
        public TextView email;

        ViewHolder(View itemView) {
            super(itemView);
            picture = itemView.findViewById(R.id.picture);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email);
            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View itemView) {
                    if (clickListener != null)
                        clickListener.onItemClick(itemView,
                                getAdapterPosition());
                }
            });
        }
    }
}
