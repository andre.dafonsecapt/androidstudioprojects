package com.dafonseca.testlepayajava.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dafonseca.testlepayajava.R;
import com.dafonseca.testlepayajava.adapters.TrainerAdapter;
import com.dafonseca.testlepayajava.models.Trainer;
import com.dafonseca.testlepayajava.services.AppServices;

import java.util.List;

public class TrainerListActivity extends AppCompatActivity implements TrainerAdapter.ItemClickListener {

    private AppServices appServices;
    private TrainerAdapter adapter;
    private static final int TRAINER_DETAILS_ACTIVITY_TAG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer_list);
        appServices = new AppServices();
        setRecyclerAdapter();


    }

    private void setRecyclerAdapter() {
        RecyclerView recyclerView = findViewById(R.id.rvTrainers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TrainerAdapter(dataSet());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    private List<Trainer> dataSet(){
        return appServices.getTrainersFromJSON(getApplicationContext());
    }

    @Override
    public void onItemClick(View view, int position) {
        Trainer trainer = adapter.getItem(position);
        Intent i = new Intent(this, TrainerDetailsActivity.class);
        i.putExtra("trainer", trainer);
        startActivityForResult(i, TRAINER_DETAILS_ACTIVITY_TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Trainer trainer = data.getParcelableExtra("trainer");
            adapter.updateItem(trainer);
        }
    }
}
