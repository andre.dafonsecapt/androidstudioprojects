package com.dafonseca.pratica5.dao;

import com.dafonseca.pratica5.models.Puntuacion;
import com.dafonseca.pratica5.retrofit.AppAPI;
import com.dafonseca.pratica5.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DaoServidor implements AppAPI {

    private Retrofit retrofit;
    private Gson gson;
    private AppAPI appAPI;

    public DaoServidor() {
        gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create()) // For parsing JSONP or String
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        appAPI = retrofit.create(AppAPI.class);
    }

    @Override
    public Call<List<Puntuacion>> recuperarPuntuaciones(){
        return appAPI.recuperarPuntuaciones();
    }

    @Override
    public Call<String> guardarPuntuaciones(List<Puntuacion> puntuaciones){
        return appAPI.guardarPuntuaciones(puntuaciones);
    }
}
