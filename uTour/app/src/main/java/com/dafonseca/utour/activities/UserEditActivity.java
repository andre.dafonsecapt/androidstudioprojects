package com.dafonseca.utour.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.dafonseca.utour.R;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Profile;
import com.dafonseca.utour.models.UserGetDTO;
import com.dafonseca.utour.services.ProfileServices;
import com.dafonseca.utour.services.UserServices;
import com.dafonseca.utour.utils.ImageTools;
import com.dafonseca.utour.viewmodels.UserViewModel;
import com.google.android.material.textfield.TextInputEditText;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.hdodenhof.circleimageview.CircleImageView;
import fj.data.Either;
import lombok.SneakyThrows;

import static com.dafonseca.utour.utils.ConstantsClient.CAMERA_PERMISSION_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_CAPTURE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_GALLERY;

public class UserEditActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private CircleImageView profileImg;
    private TextInputEditText etName;
    private TextInputEditText etEmail;
    private CheckBox cbAdmin;
    private CheckBox cbActivated;
    private Button btnSave;
    private ProfileServices profileServices;
    private UserServices userServices;
    private ImageTools imageTools;
    private Profile profileToUpdate;
    private UserGetDTO userToUpdate;
    private UserViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        setViews();
        initObjects();
        setInitFields();
        setListeners();
        executeGetProfileTask();
    }

    private void setViews() {
        swipeRefreshLayout = findViewById(R.id.user_edit_swiperefreshlayout);
        profileImg = findViewById(R.id.user_edit_img);
        etName = findViewById(R.id.user_edit_name);
        etEmail = findViewById(R.id.user_edit_email);
        cbAdmin = findViewById(R.id.user_edit_checkBox_admin);
        cbActivated = findViewById(R.id.user_edit_checkBox_activated);
        btnSave = findViewById(R.id.user_edit_save_btn);
    }

    private void initObjects() {
        profileServices = new ProfileServices();
        userServices = new UserServices();
        imageTools = new ImageTools();
        profileToUpdate = new Profile();
        userToUpdate = new UserGetDTO();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));

    }

    private void setInitFields() {
        UserGetDTO user = getIntent().getParcelableExtra("userSelected");
        if (user != null) {
            etEmail.setText(user.getEmail());
            if (user.getIsAdmin() == 1) {
                cbAdmin.setChecked(true);
            }
            if (user.getIsActivated() == 1) {
                cbActivated.setChecked(true);
            }
            userToUpdate.setIdUser(user.getIdUser());
        }
    }

    private void setListeners() {
        etName.addTextChangedListener(new MyTextWatcher());
        etEmail.addTextChangedListener(new MyTextWatcher());
    }

    private void executeGetProfileTask() {
        GetProfileTask task = new GetProfileTask();
        task.execute();
    }

    //////      Click Events Methods        ///////

    public void onUpdateUserBtnClick(View view) {
        profileToUpdate.setName(Objects.requireNonNull(etName.getText()).toString());
        UpdateProfileTask task = new UpdateProfileTask();
        task.execute();
    }

    public void onDeleteUserBtnClick(View view) {
        DeleteUserTask task = new DeleteUserTask();
        task.execute();
    }

    public void onNewImageBtnClick(View view) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else if (options[item].equals("Choose from Gallery")) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Bitmap imageBitmap = null;
                if (requestCode == REQUEST_IMAGE_CAPTURE) {
                    Bundle extras = data.getExtras();
                    imageBitmap = (Bitmap) Objects.requireNonNull(extras).get("data");
                    profileImg.setImageBitmap(imageBitmap);
                } else if (requestCode == REQUEST_IMAGE_GALLERY) {
                    Uri imageUri = data.getData();
                    imageBitmap = imageTools.getCorrectlyOrientedImage(this, imageUri, 800);
                    Glide.with(this).load(imageUri).into(profileImg);
                }
                imageBitmap = imageTools.getResizedBitmap(Objects.requireNonNull(imageBitmap), 1000);
                profileToUpdate.setPicture(imageTools.convertBitmapToEncodedString(imageBitmap));
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            Toast.makeText(this, "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    //////      Lifecycle Methods        ///////

    @Override
    protected void onResume() {
        btnSave.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty() && !Objects.requireNonNull(etEmail.getText()).toString().isEmpty());
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.getUsernameLive().setValue(Objects.requireNonNull(etName.getText()).toString());
        viewModel.getEmailLive().setValue(Objects.requireNonNull(etEmail.getText()).toString());
    }

    private void setViewModel() {
        if (viewModel != null) {
            etName.setText(viewModel.getUsernameLive().getValue());
            etEmail.setText(viewModel.getEmailLive().getValue());
        }
        viewModel = new ViewModelProvider(this).get(UserViewModel.class);
        viewModel.getUsernameLive().observe(this, username -> etName.setText(username));
        viewModel.getEmailLive().observe(this, email -> etEmail.setText(email));
    }

    //////      AsyncTasks Methods        ///////

    @SuppressLint("StaticFieldLeak")
    class GetProfileTask extends AsyncTask<Void, Void, Either<ApiError, Profile>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, Profile> doInBackground(Void... voids) {
            return profileServices.getProfile(DataHolder.getInstance().getIdUser());
        }

        @Override
        protected void onPostExecute(Either<ApiError, Profile> result) {
            swipeRefreshLayout.setRefreshing(false);
            if (result.isRight()) {
                Profile profile = result.right().value();
                etName.setText(profile.getName());
                if (profile.getPicture() != null && !profile.getPicture().isEmpty()) {
                    profileImg.setImageBitmap(imageTools.convertEncodedStringToBitmap(profile.getPicture()));
                }
                setViewModel();
            } else {
                Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class UpdateProfileTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return profileServices.adminUpdateProfile(profileToUpdate);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            String resultString;
            if (result.isRight()) {
                userToUpdate.setEmail(Objects.requireNonNull(etEmail.getText()).toString());
                if (cbAdmin.isChecked()) {
                    userToUpdate.setIsAdmin(1);
                } else {
                    userToUpdate.setIsAdmin(0);
                }
                if (cbActivated.isChecked()) {
                    userToUpdate.setIsActivated(1);
                } else {
                    userToUpdate.setIsActivated(0);
                }
                UpdateUserTask task = new UpdateUserTask();
                task.execute();
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class UpdateUserTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return userServices.updateUser(userToUpdate);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            swipeRefreshLayout.setRefreshing(false);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DeleteUserTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return userServices.deleteUser(userToUpdate.getIdUser());
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            swipeRefreshLayout.setRefreshing(false);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    // custom class that implements TextWatcher to check if form is valid everytime the text changes
    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            btnSave.setEnabled(!Objects.requireNonNull(etName.getText()).toString().isEmpty() && !Objects.requireNonNull(etEmail.getText()).toString().isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }
    }
}
