package com.dafonseca.commsdosfragmentosimagens;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements FragmentSender.SendMessageListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentSender fragmentSender = (FragmentSender) getSupportFragmentManager().findFragmentById(R.id.fragmentSender);
        fragmentSender.setClickListener(this);
    }

    @Override
    public void onSendImageString(String imageName) {

        FragmentReceiver fragmentReceiver = (FragmentReceiver) getSupportFragmentManager().findFragmentById(R.id.fragmentReceiver);

        if (fragmentReceiver != null) {

            fragmentReceiver.showImage(imageName);
        } else {
            Intent i = new Intent(this, SecondaryActivity.class);
            i.putExtra("imageName", imageName);
            startActivity(i);
        }

    }
}
