package com.dafonseca.mycalculadora;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txNumber1 = findViewById(R.id.txNumber1);
                EditText txNumber2 = findViewById(R.id.txNumber2);
                TextView resultTextView =  findViewById(R.id.textViewResult);

                int num1 = Integer.parseInt(txNumber1.getText().toString());
                int num2 = Integer.parseInt(txNumber2.getText().toString());
                int result = num1 +num2;
                resultTextView.setText(result+"");
            }
        });
    }
}
