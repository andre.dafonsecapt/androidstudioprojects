package com.dafonseca.utour.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private List<Bitmap> data;
    private ItemClickListener clickListener;
    private boolean showDeleteBtn;

    public GalleryAdapter(List<Bitmap> data) {
        this.data = data;
        this.showDeleteBtn = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.image.setImageBitmap(data.get(position));
        if (showDeleteBtn) {
            holder.deleteBtn.setVisibility(View.VISIBLE);
        } else {
            holder.deleteBtn.setVisibility(View.GONE);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public Bitmap getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Bitmap> getData() {
        return data;
    }

    public void setItems(List<Bitmap> images) {
        data = images;
        notifyDataSetChanged();
    }

    public void addItem(Bitmap image) {
        data.add(image);
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    public void setShowDeleteBtn(boolean showDeleteBtn) {
        this.showDeleteBtn = showDeleteBtn;
    }

    interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView image;
        final ImageButton deleteBtn;

        ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.item_gallery_img);
            deleteBtn = itemView.findViewById(R.id.item_gallery_btn_delete);
            itemView.setOnClickListener(itemView1 -> {
                if (clickListener != null) {
                    clickListener.onItemClick(itemView1,
                            getAdapterPosition());
                }
            });
            deleteBtn.setOnClickListener(itemView12 -> removeItem(getAdapterPosition()));
        }
    }
}
