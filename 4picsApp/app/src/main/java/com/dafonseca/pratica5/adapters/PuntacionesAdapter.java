package com.dafonseca.pratica5.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.pratica5.R;
import com.dafonseca.pratica5.models.Puntuacion;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PuntacionesAdapter extends RecyclerView.Adapter<PuntacionesAdapter.ViewHolder> {

    private List<Puntuacion> data;
    private ItemClickListener mClickListener;

    public PuntacionesAdapter(List<Puntuacion>data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.puntacionesrecycler_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String nombre = data.get(position).getNombre();
        holder.tvRvNombre.setText(nombre);
        String dificultad = data.get(position).getDificultad();
        holder.tvRvDificultad.setText(dificultad);
        int tiempo = data.get(position).getTiempo();
        holder.tvRvTiempo.setText(String.valueOf(tiempo));
        int score = data.get(position).getScore();
        holder.tvRvScore.setText(String.valueOf(score));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvRvNombre;
        TextView tvRvDificultad;
        TextView tvRvTiempo;
        TextView tvRvScore;
        ViewHolder(View itemView) {
            super(itemView);
            tvRvNombre = itemView.findViewById(R.id.tvRvNombre);
            tvRvDificultad = itemView.findViewById(R.id.tvRvDificultad);
            tvRvTiempo = itemView.findViewById(R.id.tvRvTiempo);
            tvRvScore = itemView.findViewById(R.id.tvRvScore);
            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View itemView) {
                    if (mClickListener != null)
                        mClickListener.onItemClick(itemView,
                                getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public Puntuacion getItem(int id) {
        return data.get(id);
    }

    public List<Puntuacion> getAllItems(){
        return data;
    }

    public void addItem(Puntuacion item){
        data.add(item);
        sortDataByScore();
        notifyDataSetChanged();
    }

    public void updateItem(Puntuacion item){

        for (Puntuacion p : data){
            if (p.getId() == item.getId()){
                p.setNombre(item.getNombre());
                p.setDificultad(item.getDificultad());
                p.setTiempo(item.getTiempo());
                p.setScore(item.getScore());
            }
        }
        sortDataByScore();
        notifyDataSetChanged();
    }

    public void deleteItem(int id){

        int index = 0;
        int indexToRemove = 0;
        for (Puntuacion p : data){
            if (p.getId() == id){
                indexToRemove = index;
            }
            index++;
        }
        data.remove(indexToRemove);
        notifyItemRemoved(indexToRemove);
    }

    public void deleteAll(){
        for (int i = 0 ; i < data.size() ; i++){
            data.remove(i);
            notifyItemRemoved(i);
        }
    }

    private void sortDataByScore() {
        Collections.sort(data, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return Integer.compare(((Puntuacion)o2).getScore(), ((Puntuacion)o1).getScore());
            }
        });
    }
}
