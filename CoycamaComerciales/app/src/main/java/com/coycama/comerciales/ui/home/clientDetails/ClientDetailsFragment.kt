package com.coycama.comerciales.ui.home.clientDetails

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.LastData
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Constants.LAST_INCIDENTS_TABLE_NAME
import com.coycama.comerciales.utils.Constants.LAST_ORDERS_TABLE_NAME
import com.coycama.comerciales.utils.Constants.LAST_SHIPMENTS_TABLE_NAME
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.xwray.groupie.GroupieAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_client_details.*
import kotlinx.android.synthetic.main.fragment_client_details.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class ClientDetailsFragment : Fragment(), KodeinAware, LastDataItemListener {

    // needed variable for KodeIn dependency injection
    override val kodein by kodein()

    // factory is used to inject repositories in viewmodel
    private val factory: ClientDetailsViewModelFactory by instance()
    private lateinit var viewModel: ClientDetailsViewModel

    // interface for callback functions
    private lateinit var lastDataListener: LastDataItemListener

    // navigation arguments that are passed between fragments (declared in nav_graph)
    private val args by navArgs<ClientDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_client_details, container, false)
        view.client_details_name_tv.text = args.client.NomComercial
        view.client_details_pending_pay_tv.text = args.client.PagoPendiente
        view.client_details_last_shipment_tv.text =
            "${args.client.FechaUltimoEnvio} \n  ${args.client.UltimoEnvio}"
        view.client_details_last_order_tv.text =
            args.client.FechaUltimoPedido + "\n" + args.client.UltimoPedido
        view.client_details_last_incident_tv.text =
            args.client.FechaUltimaIncidencia + "\n" + args.client.UltimaIncidencia
        view.client_details_last_visit_tv.text = args.client.ultimaVisita?.Fecha

        setListeners(view)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(ClientDetailsViewModel::class.java)
        viewModel.clientId = args.client.Cliente.toInt()
        viewModel.subClientId = args.client.SubCliente.toInt()
        bindUI(LAST_SHIPMENTS_TABLE_NAME)
        bindUI(LAST_ORDERS_TABLE_NAME)
        bindUI(LAST_INCIDENTS_TABLE_NAME)
        lastDataListener = this
    }

    // serves to transform the app toolbar throughout the different fragments
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.home_logo_imageview?.visibility = View.INVISIBLE
        activity?.home_toolbar_person_icon?.visibility = View.GONE
        activity?.home_toolbar_search_icon?.visibility = View.GONE
        activity?.home_toolbar_title?.text = "Ficha Clientes"
        activity?.home_toolbar?.setNavigationIcon(R.drawable.ic_arrow_back)
        activity?.home_toolbar?.setNavigationOnClickListener {
            activity?.home_toolbar?.navigationIcon = null
            activity?.onBackPressed()
        }

    }

    // listens to clicks in UI elements to navigate to other fragment
    private fun setListeners(view: View) {
        view.client_details_sales_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientDetailsFragmentDirections.clientDetailsPageToClientSalesPage(
                    args.client
                )
            )
        }

        view.client_details_surveys_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientDetailsFragmentDirections.clientDetailsPageToClientSurveyListPage(
                    args.client
                )
            )
        }

        view.client_details_goods_btn_tv.setOnClickListener {
            findNavController().navigate(
                ClientDetailsFragmentDirections.clientDetailsPageToClientGoodsPage(
                    args.client
                )
            )
        }

        view.client_details_pending_pay_tv.setOnClickListener {
            findNavController().navigate(
                ClientDetailsFragmentDirections.clientDetailsPageToPendingPaymentsPage(
                    args.client
                )
            )
        }
        view.client_details_pending_pay_label.setOnClickListener {
            findNavController().navigate(
                ClientDetailsFragmentDirections.clientDetailsPageToPendingPaymentsPage(
                    args.client
                )
            )
        }
    }

    // bind recyclerview with data using the Groupie library
    private fun bindUI(lastDataSet: String) = Coroutines.main {
        when (lastDataSet) {
            LAST_SHIPMENTS_TABLE_NAME -> {
                client_details_shipments_progressBar.show()
                observeLastData(lastDataSet)
                client_details_shipments_progressBar.hide()
            }
            LAST_ORDERS_TABLE_NAME -> {
                client_details_orders_progressBar.show()
                observeLastData(lastDataSet)
                client_details_orders_progressBar.hide()
            }
            LAST_INCIDENTS_TABLE_NAME -> {
                client_details_incidents_progressBar.show()
                observeLastData(lastDataSet)
                client_details_incidents_progressBar.hide()
            }
        }
    }

    private suspend fun observeLastData(lastDataSet: String) {
        try {
            when (lastDataSet) {
                LAST_SHIPMENTS_TABLE_NAME -> {
                    viewModel.lastShipments.await().observe(viewLifecycleOwner, {
                        initRecyclerView(it.toLastDataItem(), LAST_SHIPMENTS_TABLE_NAME)
                    })
                }
                LAST_ORDERS_TABLE_NAME -> {
                    viewModel.lastOrders.await().observe(viewLifecycleOwner, {
                        initRecyclerView(it.toLastDataItem(), LAST_ORDERS_TABLE_NAME)

                    })
                }
                LAST_INCIDENTS_TABLE_NAME -> {
                    viewModel.lastIncidents.await().observe(viewLifecycleOwner, {
                        initRecyclerView(it.toLastDataItem(), LAST_INCIDENTS_TABLE_NAME)
                    })
                }
            }
        } catch (e: ApiException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        } catch (e: NoInternetException) {
            requireContext().toast("API ERROR: " + e.message!!)
            Log.e("API ERROR", e.message)
        }
    }


    // setup the recycler views and add items to the list
    private fun initRecyclerView(lastDataItems: List<LastDataItem>, lastDataSet: String) {
        val lastDataAdapter = GroupieAdapter().apply {
            addAll(lastDataItems)
        }
        when (lastDataSet) {
            LAST_SHIPMENTS_TABLE_NAME -> {
                client_details_last_shipments_recycler_view.apply {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    adapter = lastDataAdapter
                }
            }
            LAST_ORDERS_TABLE_NAME -> {
                client_details_last_orders_recycler_view.apply {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    adapter = lastDataAdapter
                }
            }
            LAST_INCIDENTS_TABLE_NAME -> {
                client_details_last_incidents_recycler_view.apply {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    adapter = lastDataAdapter
                }
            }
        }
    }


    // transforms data model into an data model item so that it can be used to bind data to recycler view list item
    private fun List<LastData>.toLastDataItem(): List<LastDataItem> {
        return this.map {
            LastDataItem(it, lastDataListener)
        }
    }

    override fun onLastDataItemClicked(data: LastData) {
        TODO("Not yet implemented")
    }


}