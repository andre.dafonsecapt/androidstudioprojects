package com.dafonseca.utour.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tour implements Parcelable {

    public static final Creator<Tour> CREATOR = new Creator<Tour>() {
        @Override
        public Tour createFromParcel(Parcel in) {
            return new Tour(in);
        }

        @Override
        public Tour[] newArray(int size) {
            return new Tour[size];
        }
    };
    private int idTour;
    private String image;
    private String tourname;
    private String description;
    private String city;
    private int duration;
    private float rating;
    private int fk_idUser;
    private List<Stop> stops;
    private boolean expanded;
    private boolean isLongClickable;

    public Tour() {
        stops = new ArrayList<>();
        isLongClickable = false;
    }


    protected Tour(Parcel in) {
        idTour = in.readInt();
        tourname = in.readString();
        description = in.readString();
        city = in.readString();
        duration = in.readInt();
        rating = in.readFloat();
        fk_idUser = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idTour);
        dest.writeString(tourname);
        dest.writeString(description);
        dest.writeString(city);
        dest.writeInt(duration);
        dest.writeFloat(rating);
        dest.writeInt(fk_idUser);
    }
}
