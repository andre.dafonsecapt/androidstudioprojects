package com.dafonseca.radiobuttonsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvMessage;
    RadioGroup radioGroup;
    RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvMessage = findViewById(R.id.tvMessage);
        radioGroup = findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group,
                                                 int checkedId) {
                        radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
                        tvMessage.setText("You have selected "+radioButton.getText().toString());
                    }
                }
        );

    }

    public void selectCountry(View view){
        radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
        tvMessage.setText("You have selected "+radioButton.getText().toString());
    }
}
