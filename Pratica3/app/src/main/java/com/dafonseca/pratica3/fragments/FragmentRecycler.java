package com.dafonseca.pratica3.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.pratica3.activities.GameActivity;
import com.dafonseca.pratica3.activities.GameSecondaryActivity;
import com.dafonseca.pratica3.R;
import com.dafonseca.pratica3.adapters.RecyclerAdapter;
import com.dafonseca.pratica3.viewmodels.GameViewModel;

import java.util.ArrayList;
import java.util.List;

public class FragmentRecycler extends Fragment implements RecyclerAdapter.ItemClickListener {

    private RecyclerAdapter adapter;
    private GameViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_recyclerview, container, false);

        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RecyclerAdapter(dataSet());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        viewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);
        return rootView;
    }


    private List<Button> dataSet() {
        List<Button> dataSet = new ArrayList<>();
        Button b1 = new Button(getContext());
        b1.setText("Nivel 1");
        Button b2 = new Button(getContext());
        b2.setText("Nivel 2");
        Button b3 = new Button(getContext());
        b3.setText("Nivel 3");
        dataSet.add(b1);
        dataSet.add(b2);
        dataSet.add(b3);
        return dataSet;
    }

    @Override
    public void onItemClick(View view, int position) {

        FragmentImagen fragmentImagen = (FragmentImagen) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragmentImagen);

        if (getActivity().getClass().equals(GameActivity.class) && position == ((GameActivity) getActivity()).getNivel()){
            if (fragmentImagen != null) {
                fragmentImagen.showImage(adapter.getItem(position).getText().toString().replace(" ", "").toLowerCase(), viewModel.getDificultad());
            } else {
                Intent i = new Intent(getContext(), GameSecondaryActivity.class);
                i.putExtra("dificultad", viewModel.getDificultad());
                i.putExtra("nivel", adapter.getItem(position).getText().toString().replace(" ", "").toLowerCase());
                i.putExtra("position", position);
                startActivityForResult(i, ((GameActivity) getActivity()).getSecondaryActivityTag());
            }
        } else {
            Toast.makeText(getContext(), "Completa los niveles anteriores primero.", Toast.LENGTH_LONG).show();
        }
    }

    public RecyclerAdapter getAdapter() {
        return adapter;
    }
}
