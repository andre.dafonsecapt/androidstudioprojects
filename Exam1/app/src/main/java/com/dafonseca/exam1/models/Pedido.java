package com.dafonseca.exam1.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Pedido implements Parcelable {

    private int nMesa;
    private String terraza_comedor;
    private String camarero;
    private List<String> comidasList;

    public Pedido(int nMesa, String terraza_comedor, String camarero) {
        this.nMesa = nMesa;
        this.terraza_comedor = terraza_comedor;
        this.camarero = camarero;
        comidasList = new ArrayList<>();
    }

    private Pedido(Parcel in){
        nMesa = in.readInt();
        terraza_comedor = in.readString();
        camarero = in.readString();
        comidasList = in.createStringArrayList();
    }

    public void addComidas(List<String> comidas){
        comidasList.addAll(comidas);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nMesa);
        dest.writeString(terraza_comedor);
        dest.writeString(camarero);
        dest.writeStringList(comidasList);
    }

    public static final Parcelable.Creator<Pedido> CREATOR
            = new Parcelable.Creator<Pedido>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public Pedido createFromParcel(Parcel in) {
            return new Pedido(in);
        }

        @Override
        public Pedido[] newArray(int size) {
            return new Pedido[size];
        }
    };

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Numero de Mesa: ").append(nMesa).append('\n');
        sb.append("Ubicación: ").append(terraza_comedor).append('\n');
        sb.append("Camarero: ").append(camarero).append('\n');
        sb.append("Comidas:\n").append(comidasList);
        return sb.toString();
    }
}
