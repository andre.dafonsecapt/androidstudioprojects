package com.coycama.comerciales.ui.login

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.myFramework.ktExtensions.md5
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException

class LoginViewModel(
    private val userRepository: UserRepository
) : ViewModel() {

    var email: String? = null
    var password: String? = null

    var loginListener: LoginListener? = null

    fun getLoggedInUser() = userRepository.getUser()

    fun onLoginBtnClick(view: View) {
        loginListener?.onStarted()
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            loginListener?.onFailure("Email o contraseña inválidos")
            return
        }

        Coroutines.main {
            try {
                val loginResponse = userRepository.userLogin(email!!, password!!.md5())
                loginResponse.user?.let {
                    loginListener?.onSuccess(it)
                    userRepository.saveUser(it)
                    return@main
                }
                loginListener?.onFailure(loginResponse.message!!)
            } catch (e: ApiException) {
                loginListener?.onFailure(e.message!!)
            } catch(e: NoInternetException){
                loginListener?.onFailure(e.message!!)
            }
        }

    }

    fun onRegisterClicked(view: View){
        loginListener?.onRegisterClicked()
    }

    fun onForgotPasswordClicked(view: View){
        loginListener?.onForgotPasswordClicked()
    }
}