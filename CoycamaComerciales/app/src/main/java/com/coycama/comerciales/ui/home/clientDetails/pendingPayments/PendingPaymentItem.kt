package com.coycama.comerciales.ui.home.clientDetails.pendingPayments

import android.annotation.SuppressLint
import android.os.Build
import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.PendingPayment
import com.coycama.comerciales.databinding.ItemPendingPaymentBinding
import com.xwray.groupie.viewbinding.BindableItem
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PendingPaymentItem(
    private val payment: PendingPayment
) : BindableItem<ItemPendingPaymentBinding>() {

    @SuppressLint("SimpleDateFormat")
    override fun bind(viewBinding: ItemPendingPaymentBinding, position: Int) {
            val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
            payment.FechaFac = LocalDate.parse(payment.FechaFac, formatter).toString()
            payment.FechaVto = LocalDate.parse(payment.FechaVto, formatter).toString()
        viewBinding.payment = payment
    }

    override fun getLayout() = R.layout.item_pending_payment

    override fun initializeViewBinding(view: View): ItemPendingPaymentBinding {
        return ItemPendingPaymentBinding.bind(view)
    }


}