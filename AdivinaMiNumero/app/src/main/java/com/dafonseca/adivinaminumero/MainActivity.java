package com.dafonseca.adivinaminumero;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText numAdivinado;
    private Button resetButton;
    private TextView tvTopText;
    private TextView tvIntentos;
    private Random dado;
    private int numCorrecto;
    private int intentos;

    private final static String NUM_CORRECTO = "numCorrecto";
    private final static String INTENTOS = "intentos";
    private final static String TV_TOPMENSAJE = "tvTopText";
    private final static String TV_INTENTOS = "tvIntentos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numAdivinado = findViewById(R.id.etNumIntro);
        resetButton = findViewById(R.id.resetButton);
        tvTopText = findViewById(R.id.tvTopText);
        tvIntentos = findViewById(R.id.tvIntentos);
        dado = new Random(System.currentTimeMillis());
        intentos = 0;
        numCorrecto = dado.nextInt(100) + 1;

        if (savedInstanceState != null){
            intentos  = savedInstanceState.getInt(INTENTOS);
            numCorrecto = savedInstanceState.getInt(NUM_CORRECTO);
            tvIntentos.setText(savedInstanceState.getString(TV_INTENTOS));
            tvIntentos.setVisibility(View.VISIBLE);
            tvTopText.setText(savedInstanceState.getString(TV_TOPMENSAJE));
        }

        numAdivinado.setOnKeyListener(new android.view.View.OnKeyListener() {
            public boolean onKey(View v,
                                 int keyCode,
                                 android.view.KeyEvent event) {
                // Han pulsado (o soltado) una tecla...
                if ((event.getAction() ==
                        android.view.KeyEvent.ACTION_DOWN) &&
                        (keyCode ==
                                android.view.KeyEvent.KEYCODE_ENTER)) {
                    // Ha sido una pulsación de "intro"
                    // PENDIENTE! HACER ALGO!!
                    int numAdivinadoINT = Integer.parseInt(numAdivinado.getText().toString());
                    if (numAdivinadoINT > numCorrecto) {
                        String format = getResources().getString(R.string.menorText);
                        String cadFinal = String.format(format, numAdivinadoINT);
                        tvTopText.setText(cadFinal);
                    } else if (numAdivinadoINT < numCorrecto) {
                        String format = getResources().getString(R.string.mayorText);
                        String cadFinal = String.format(format, numAdivinadoINT);
                        tvTopText.setText(cadFinal);
                    } else {
                        tvTopText.setText(R.string.acertado);
                        numAdivinado.setVisibility(View.INVISIBLE);
                        TextView intentaloTexto = findViewById(R.id.textView);
                        intentaloTexto.setVisibility(View.INVISIBLE);
                        resetButton.setVisibility(View.VISIBLE);
                    }
                    tvIntentos.setText(setTvIntentos());
                    return true;
                } else
                    return false;
            } // onKey
        });

    }

    private String setTvIntentos(){
        intentos++;
        String format = getResources().getQuantityString(
                R.plurals.intentos,
                intentos,
                intentos);
        String cadFinal = String.format(format, intentos);
        if (tvIntentos.getVisibility() == View.INVISIBLE) {
            tvIntentos.setVisibility(View.VISIBLE);
        }
        return cadFinal;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NUM_CORRECTO, numCorrecto);
        outState.putInt(INTENTOS, intentos);
        outState.putString(TV_TOPMENSAJE, tvTopText.getText().toString());
        outState.putString(TV_INTENTOS, tvIntentos.getText().toString());


    }

    public void resetButton(View view) {
        numCorrecto = dado.nextInt(100) + 1;
        tvTopText.setText(R.string.topText);
        numAdivinado.setVisibility(View.VISIBLE);
        TextView intentaloTexto = findViewById(R.id.textView);
        intentaloTexto.setVisibility(View.VISIBLE);
        resetButton.setVisibility(View.INVISIBLE);
        tvIntentos.setVisibility(View.INVISIBLE);
        intentos = 0;
    }


}
