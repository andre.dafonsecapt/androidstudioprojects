package com.dafonseca.pratica5.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.pratica5.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

public class IntroActivity extends AppCompatActivity {

    TextView tvNombre;
    TextView tvAutor;
    TextView tvFecha;
    TextView tvLocalizacion;
    TextView tvDescripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        tvNombre = findViewById(R.id.tvNombre);
        tvAutor = findViewById(R.id.tvAutor);
        tvFecha = findViewById(R.id.tvFecha);
        tvLocalizacion = findViewById(R.id.tvLocalizacion);
        tvDescripcion = findViewById(R.id.tvDescripcion);


        String jsonStr = readJSON();

        writeTextView(jsonStr);
    }


    private String readJSON() {
        StringBuilder strBuild = new StringBuilder();
        String line;
        InputStream is = getResources().openRawResource(R.raw.nfo);
        Scanner scanner = new Scanner(is);
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            strBuild.append(line + "\n");
        }
        scanner.close();
        return strBuild.toString();
    }

    private void writeTextView(String jsonStr) {

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            tvNombre.setText(tvNombre.getText()+jsonObj.getString("nombre"));
            tvAutor.setText(tvAutor.getText()+jsonObj.getString("autor"));
            tvFecha.setText(tvFecha.getText()+jsonObj.getString("fecha"));
            tvLocalizacion.setText(tvLocalizacion.getText()+jsonObj.getString("localizacion"));
            tvDescripcion.setText(tvDescripcion.getText()+jsonObj.getString("descripcion"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void startApp(View view){
        finish();
    }
}
