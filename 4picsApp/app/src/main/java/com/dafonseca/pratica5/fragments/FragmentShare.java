package com.dafonseca.pratica5.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.dafonseca.pratica5.R;

public class FragmentShare extends Fragment {

    public FragmentShare() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_share, container, false);

        return rootView;
    }
}
