package com.coycama.comerciales.ui.home.clientDetails.goods

import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import com.coycama.comerciales.data.repositories.GoodsRepository
import com.coycama.comerciales.utils.lazyDeferred

class ClientGoodsViewModel(
    private val goodsRepository: GoodsRepository
) : ViewModel() {

    var clientId = 0

    val goods by lazyDeferred {
        goodsRepository.getGoodsByClient(GetByClientRequest(clientId))
    }
}