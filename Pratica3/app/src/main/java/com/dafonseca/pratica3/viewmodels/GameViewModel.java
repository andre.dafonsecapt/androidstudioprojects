package com.dafonseca.pratica3.viewmodels;

import androidx.lifecycle.ViewModel;

public class GameViewModel extends ViewModel {

    private int nivel = 0;
    private String dificultad;



    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }
}
