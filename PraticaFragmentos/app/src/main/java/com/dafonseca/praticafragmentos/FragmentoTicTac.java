package com.dafonseca.praticafragmentos;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.logging.Logger;

public class FragmentoTicTac extends Fragment {

    private View rootView;
    private Button[][] buttons = new Button[3][3];
    private Button buttonReset;

    private boolean player1Turn;

    private int player1Points;
    private int player2Points;

    private TextView textViewPlayer1;
    private TextView textViewPlayer2;
    private TextView textViewResultado;

    public static FragmentoTicTac newInstance() {
        return new FragmentoTicTac();
    }

    /*La interfaz del fragmento*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {

        player1Turn = true;
        player1Points = 0;
        player2Points = 0;

        rootView = inflater.inflate(R.layout.fragment_fragmentotictac, container, false);

        textViewPlayer1 = rootView.findViewById(R.id.tv_player1);
        textViewPlayer2 = rootView.findViewById(R.id.tv_player2);
        textViewResultado = rootView.findViewById(R.id.tvResultado);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String buttonID = "button_grid" + i + j;
                int resourcesID = getResources().getIdentifier(buttonID, "id", getActivity().getPackageName());
                buttons[i][j] = rootView.findViewById(resourcesID);
                buttons[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (((Button) v).getText().toString().equals("")) {
                            if (player1Turn) {
                                ((Button) v).setText("X");
                            } else {
                                ((Button) v).setText("O");
                            }
                            if (checkForWin()) {
                                if (player1Turn) {
                                    player1Wins();
                                } else {
                                    player2Wins();
                                }
                            } else {
                                player1Turn = !player1Turn;
                            }
                        }

                    }
                });
            }
        }


        buttonReset = rootView.findViewById(R.id.buttonReset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        buttons[i][j].setText("");
                        buttons[i][j].setEnabled(true);
                    }
                }
                textViewResultado.setText("");
            }
        });


        return rootView;
    }

    private boolean checkForWin() {
        String[][] field = new String[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }

        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }

        if (field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) {
            return true;
        }
        return false;
    }

    private void player1Wins() {
        player1Points++;
        textViewPlayer1.setText(getResources().getString(R.string.player1) + " " + player1Points);
        textViewResultado.setText(R.string.player1wins);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setEnabled(false);
            }
        }
    }

    private void player2Wins() {
        player2Points++;
        textViewPlayer2.setText(getResources().getString(R.string.player2) + " " + player2Points);
        textViewResultado.setText(R.string.player2wins);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setEnabled(false);
            }
        }
    }
}
