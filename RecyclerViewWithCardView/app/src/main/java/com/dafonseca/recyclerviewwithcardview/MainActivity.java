package com.dafonseca.recyclerviewwithcardview;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MusicAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.rvMusicCards);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        adapter = new MusicAdapter(dataSet());
        recyclerView.setAdapter(adapter);
    }

    private List<Album> dataSet(){

        List items = new ArrayList();

        items.add(new Album(R.drawable.album1, "Angel Beats", "Come to Me"));
        items.add(new Album(R.drawable.album2, "Death Note", "By The By"));
        items.add(new Album(R.drawable.album3, "Fate Stay Night", "All Night"));
        items.add(new Album(R.drawable.album4, "Welcome to the NHK", "Creeping Death"));
        items.add(new Album(R.drawable.album5, "Suzumiya Haruhi", "Ride the Lighnting"));
        items.add(new Album(R.drawable.album6, "Suzumiya Haruhi", "Azul"));

        return items;

    }
}
