package com.dafonseca.xml_pullparser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvText = findViewById(R.id.tvText);
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

            XmlPullParser parser = factory.newPullParser();

            InputStream is = getAssets().open("xmlFile.xml");

            parser.setInput(is, null);

            StringBuffer s = new StringBuffer(" ");
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() == XmlPullParser.START_TAG &&
                        parser.getName().equals("user_name")) {
                    parser.next();
                    s.append(parser.getText());
                    break;
                }
                parser.next();
            }
            tvText.setText(s.toString());
            is.close();
        } catch (Exception e){
            System.out.println(e);
        }


    }
}
