package com.dafonseca.xml_dom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvEmployee = findViewById(R.id.tvEmployee);

        List<Employee> listEmp = new ArrayList<>();
//Get the DOM Builder Factory
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
//Get the DOM Builder
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
//Load and Parse the XML document
//document contains the complete XML as a Tree.
        Document document = null;
        try {
            FileInputStream fis;
            fis = openFileInput("employee.xml");
            document = builder.parse(fis);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
// Shows the number of employees in the document.
        int nrEmployees= document.getElementsByTagName("employee").getLength();
        System.out.println("number of employees: " + nrEmployees);
        for (int i=0; i<nrEmployees; i++) {
            Employee emp = new Employee();

            emp.setFirstName(document.getElementsByTagName("firstName").item(i).getFirstChild().getNodeValue());

            emp.setLastName(document.getElementsByTagName("lastName").item(i).getFirstChild().getNodeValue());
            listEmp.add(emp);
        }

        tvEmployee.setText(listEmp.toString());
    }
}
