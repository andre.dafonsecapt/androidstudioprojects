package com.dafonseca.utour.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dafonseca.utour.R;
import com.dafonseca.utour.activities.ToursNewActivity;
import com.dafonseca.utour.adapters.TourAdapter;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.TourServices;
import com.dafonseca.utour.viewmodels.MainViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import fj.data.Either;
import lombok.SneakyThrows;

import static android.app.Activity.RESULT_OK;
import static com.dafonseca.utour.utils.ConstantsClient.CREATE_TOUR_CODE;

public class ToursUserSplashFragment extends Fragment {

    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView noToursImg;
    private TextView tvNoTours;
    private TourAdapter tourAdapter;
    private TourServices tourServices;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingBtn;
    private List<Tour> tours;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tours_user_splash, container, false);

        setViews(rootView);
        setTitle();
        initObjects();
        setRecyclerAdapter(rootView);
        setListeners();
        launchGetToursTask();

        return rootView;
    }

    private void setViews(View rootView) {
        swipeRefreshLayout = rootView.findViewById(R.id.tours_user_splash_swipe_refresh);
        noToursImg = rootView.findViewById(R.id.tours_user_splash_notours_img);
        tvNoTours = rootView.findViewById(R.id.tours_user_splash_notours_tv);
        recyclerView = rootView.findViewById(R.id.tours_user_splash_recycler);
        floatingBtn = rootView.findViewById(R.id.tours_user_splash_floatingBtn);
    }

    private void setTitle() {
        MainViewModel viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        viewModel.getAppTitle().setValue(getString(R.string.title_tours_user));
    }

    private void initObjects() {
        tourServices = new TourServices();
        tours = new ArrayList<>();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
    }

    private void setRecyclerAdapter(View rootView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        tourAdapter = new TourAdapter(tours, getContext());
        tourAdapter.initTourServices();
        recyclerView.setAdapter(tourAdapter);
    }

    private void setListeners() {
        floatingBtn.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), ToursNewActivity.class);
            startActivityForResult(i, CREATE_TOUR_CODE);
        });
        swipeRefreshLayout.setOnRefreshListener(this::launchGetToursTask);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CREATE_TOUR_CODE && data != null) {
            launchGetToursTask();
        }
    }

    private void launchGetToursTask() {
        GetMyToursTask task = new GetMyToursTask();
        task.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        launchGetToursTask();
    }

    @SuppressLint("StaticFieldLeak")
    class GetMyToursTask extends AsyncTask<Void, Void, Either<ApiError, List<Tour>>> {

        @Override
        protected void onPreExecute() {
            swipeRefreshLayout.setRefreshing(true);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Tour>> doInBackground(Void... voids) {
            return tourServices.getMyTours(DataHolder.getInstance().getIdUser());
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Tour>> result) {
            swipeRefreshLayout.setRefreshing(false);
            if (result.isRight()) {
                tours = result.right().value();
                if (tours.isEmpty()) {
                    noToursImg.setVisibility(View.VISIBLE);
                    tvNoTours.setVisibility(View.VISIBLE);
                } else {
                    noToursImg.setVisibility(View.INVISIBLE);
                    tvNoTours.setVisibility(View.INVISIBLE);
                    tourAdapter.addItems(tours);
                    tourAdapter.setLongClickable();
                }
            } else {
                noToursImg.setVisibility(View.VISIBLE);
                tvNoTours.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
