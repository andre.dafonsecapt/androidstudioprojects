package com.dafonseca.utour.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.adapters.CustomInfoWindowAdapter;
import com.dafonseca.utour.adapters.GalleryAdapter;
import com.dafonseca.utour.models.BitmapCache;
import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.utils.AudioTools;
import com.dafonseca.utour.utils.ImageTools;
import com.dafonseca.utour.viewmodels.StopViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textfield.TextInputEditText;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.dafonseca.utour.utils.ConstantsClient.AUDIO_RECORDING_PATH;
import static com.dafonseca.utour.utils.ConstantsClient.CAMERA_PERMISSION_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.DEFAULT_ZOOM;
import static com.dafonseca.utour.utils.ConstantsClient.LOCATION_PERMISSION_REQUEST_CODE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_CAPTURE;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_IMAGE_GALLERY;
import static com.dafonseca.utour.utils.ConstantsClient.REQUEST_RECORD_AUDIO_PERMISSION;

public class StopsNewActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ScrollView scrollView;
    private FrameLayout transparentMapFrame;
    private EditText etLatitude;
    private EditText etLongitude;
    private TextInputEditText etStopName;
    private TextInputEditText etStopText;
    private MediaRecorder recorder;
    private MediaPlayer player;
    private TextView tvRecordTimer;
    private TextView tvSlider;
    private Button createUpdateStopBtn;
    private ImageButton audioRecordBtn;
    private ImageButton audioPlayBtn;
    private View recordPanelView;
    private View slideTextView;
    private float distCanMove;
    private long startTime;
    private long timeInMilliseconds;
    private long timeSwapBuff;
    private long updatedTime;
    private Timer timer;
    private Stop stop;
    private Stop stopToUpdate;
    private GalleryAdapter adapterGallery;
    private List<Bitmap> imageDataSet;
    private ImageTools imageTools;
    private AudioTools audioTools;
    private GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationClient;
    private boolean locationPermissionGranted;
    private boolean audioRecordPermission;
    private Boolean audioPlayStop;
    private boolean recordingCancelled;
    private Marker marker;
    private Activity activity;
    private StopViewModel viewModel;

    private static int dp(float value) {
        return (int) Math.ceil(1 * value);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stops_new);
        setViews();
        initObjects();
        setVariables();
        setListeners();
        initRecyclerView();
        checkLocationPermissions();
        checkAudioRecordPermissions();
        setInitFields();
        setViewModel();
    }

    private void initObjects() {
        stop = new Stop();
        imageTools = new ImageTools();
        audioTools = new AudioTools();
        imageDataSet = new ArrayList<>();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        activity = this;
    }

    private void setVariables() {
        distCanMove = dp(80);
        startTime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedTime = 0L;
    }

    private void setViews() {
        scrollView = findViewById(R.id.stops_new_scrollview);
        transparentMapFrame = findViewById(R.id.stops_new_map_frame);
        etLatitude = findViewById(R.id.stops_new_latitude);
        etLongitude = findViewById(R.id.stops_new_longitude);
        etStopName = findViewById(R.id.stops_new_name);
        etStopText = findViewById(R.id.stops_new_text);
        recordPanelView = findViewById(R.id.stops_new_record_frame);
        tvRecordTimer = findViewById(R.id.stops_new_recording_time_text);
        slideTextView = findViewById(R.id.stops_new_audio_slide_layout);
        audioRecordBtn = findViewById(R.id.stops_new_record_button);
        audioPlayBtn = findViewById(R.id.stops_new_play_button);
        tvSlider = findViewById(R.id.stops_new_slide_textview);
        createUpdateStopBtn = findViewById(R.id.stops_new_btn);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.stops_new_recyclerview);
        recyclerView.setLayoutManager(layoutManager);
        adapterGallery = new GalleryAdapter(imageDataSet);
        adapterGallery.setShowDeleteBtn(true);
        recyclerView.setAdapter(adapterGallery);
    }

    private void setListeners() {
        etStopName.addTextChangedListener(new MyTextWatcher());
        etStopText.addTextChangedListener(new MyTextWatcher());
        transparentMapFrame.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ScrollView to intercept touch events.
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    // Disable touch on transparent view
                    return false;

                case MotionEvent.ACTION_UP:
                    // Allow ScrollView to intercept touch events.
                    scrollView.requestDisallowInterceptTouchEvent(false);
                    return true;

                case MotionEvent.ACTION_MOVE:
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    return false;
                default:
                    return true;
            }
        });
        audioRecordBtn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                recordingCancelled = false;
                if (audioRecordPermission) {
                    audioPlayBtn.setVisibility(View.INVISIBLE);
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) slideTextView.getLayoutParams();
                    params.leftMargin = dp(30);
                    slideTextView.setLayoutParams(params);
                    startRecording();
                    audioRecordBtn.getParent()
                            .requestDisallowInterceptTouchEvent(true);
                    recordPanelView.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.audio_permission_msg, Toast.LENGTH_SHORT).show();
                }
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (audioRecordPermission) {
                    if (!recordingCancelled) {
                        stopRecording();
                        Toast.makeText(getApplicationContext(), R.string.audio_recording_finish, Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                if (audioRecordPermission) {
                    float x = motionEvent.getX();
                    if (x < -distCanMove) {
                        stopRecording();
                        cancelRecording();
                    }
                }
            }
            view.onTouchEvent(motionEvent);
            return true;
        });
        audioPlayBtn.setOnClickListener(v -> {
            if (audioPlayStop != null) {
                if (audioPlayStop) {
                    stopPlaying();
                } else {
                    startPlaying();
                }
            }
        });
    }

    private void checkLocationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
            initializeMap();
        } else {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private void checkAudioRecordPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            audioRecordPermission = true;
        } else {
            String[] permissions = {Manifest.permission.RECORD_AUDIO};
            ActivityCompat.requestPermissions(activity, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
        }
    }

    private void setInitFields() {
        stopToUpdate = getIntent().getParcelableExtra("stopSelected");
        // check if fields need to be set
        if (stopToUpdate != null) {
            etStopName.setText(stopToUpdate.getStopname());
            etStopText.setText(stopToUpdate.getText());
            // set audio if exists
            if (stopToUpdate.getAudio() != null && !stopToUpdate.getAudio().isEmpty()) {
                audioTools.writeAudioStringToFile(stopToUpdate.getAudio(), Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath() + AUDIO_RECORDING_PATH);
                audioPlayBtn.setEnabled(true);
                audioPlayBtn.setVisibility(View.VISIBLE);
                audioPlayStop = false;
            }
            // set images to gallery
            List<Bitmap> stopImages = new ArrayList<>();
            int imgListSize = getIntent().getIntExtra("imgListSize", 0);
            for (int i = 0; i < imgListSize; i++) {
                stopImages.add(BitmapCache.getInstance().getLru().get("stopCachedImg" + i));
            }
            adapterGallery.setItems(stopImages);
            this.stop = stopToUpdate;
            createUpdateStopBtn.setText(getString(R.string.stops_edit_btn_update));
        } else {
            createUpdateStopBtn.setText(getString(R.string.stops_new_create_stop_btn));
        }
    }

    private void setViewModel() {
        viewModel = new ViewModelProvider(this).get(StopViewModel.class);
        viewModel.getImagesLive().observe(this, images -> adapterGallery.setItems(images));
        viewModel.getAudioExistsLive().observe(this, audioState -> {
            if (audioState != null) {
                StopsNewActivity.this.audioPlayStop = audioState;
                audioPlayBtn.setEnabled(true);
                audioPlayBtn.setVisibility(View.VISIBLE);
                if (audioState) {
                    audioPlayBtn.setImageDrawable(getDrawable(R.drawable.ic_stop));
                } else {
                    audioPlayBtn.setImageDrawable(getDrawable(R.drawable.ic_play_arrow));
                }
            }
        });
    }

    /////       Media Recording Methods       /////

    private void startRecording() {
        // change play button
        audioPlayBtn.setEnabled(false);
        audioPlayBtn.setVisibility(View.GONE);
        tvSlider.setText(R.string.audio_recorder_msg_cancel);
        Toast.makeText(getApplicationContext(), R.string.audio_recorder_msg_recording, Toast.LENGTH_SHORT).show();
        // start timer
        startTime = SystemClock.uptimeMillis();
        timer = new Timer();
        MyTimerTask myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 1000, 1000);
        vibrate();
        // start recorder
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath() + AUDIO_RECORDING_PATH);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            recorder.prepare();
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        recorder.start();
    }

    private void stopRecording() {
        tvSlider.setText("");
        if (recorder != null) {
            recorder.stop();
            recorder.release();
            recorder = null;
        }
        if (timer != null) {
            timer.cancel();
        }
        tvRecordTimer.setText(getString(R.string.stops_new_audio_timer));
        vibrate();
        audioPlayBtn.setEnabled(true);
        audioPlayBtn.setVisibility(View.VISIBLE);
        audioPlayStop = false;
    }

    private void cancelRecording() {
        recordingCancelled = true;
        Toast.makeText(getApplicationContext(), R.string.audio_recorder_canceled_msg, Toast.LENGTH_SHORT).show();
        File audioFile = new File(Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath() + AUDIO_RECORDING_PATH);
        if (audioFile.exists()) {
            audioFile.delete();
        }
        audioPlayBtn.setEnabled(false);
        audioPlayBtn.setVisibility(View.GONE);
    }

    private void startPlaying() {
        player = new MediaPlayer();
        try {
            // change play button to stop image
            audioPlayBtn.setImageDrawable(getDrawable(R.drawable.ic_stop));
            audioPlayStop = true;
            // start player
            player.setDataSource(Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath() + AUDIO_RECORDING_PATH);
            player.prepare();
            player.start();
            player.setOnCompletionListener(mediaPlayer -> stopPlaying());
            startTime = SystemClock.uptimeMillis();
            // start timer
            timer = new Timer();
            MyTimerTask myTimerTask = new MyTimerTask();
            timer.schedule(myTimerTask, 1000, 1000);
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    private void stopPlaying() {
        audioPlayStop = false;
        audioPlayBtn.setImageDrawable(getDrawable(R.drawable.ic_play_arrow));
        player.release();
        player = null;
        if (timer != null) {
            timer.cancel();
        }
        if (tvRecordTimer.getText().toString().equals(getString(R.string.stops_new_audio_timer))) {
            return;
        }
        tvRecordTimer.setText(getString(R.string.stops_new_audio_timer));
    }

    private void vibrate() {
        try {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (v != null) {
                v.vibrate(200);
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    /////       GoogleMaps Methods       /////

    private void initializeMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.stops_new_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (stopToUpdate != null) {
            LatLng latLng = new LatLng(stopToUpdate.getLatitude(), stopToUpdate.getLongitude());
            setMarkerAndInfoWindow(latLng);
        } else {
            if (locationPermissionGranted) {
                getDeviceLocation();
            }
        }
        this.googleMap.setOnMapClickListener(this::setMarkerAndInfoWindow);
    }

    private void getDeviceLocation() {
        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            setMarkerAndInfoWindow(latLng);
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.error_location_message), Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (SecurityException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    private void setMarkerAndInfoWindow(LatLng latLng) {
        try {
            etLatitude.setText(String.valueOf(latLng.latitude));
            etLongitude.setText(String.valueOf(latLng.longitude));
            // get geolocation from latitude and longitude
            Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addressList = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
            StringBuilder addressLine = new StringBuilder();
            if (addressList.size() > 0) {
                Address address = addressList.get(0);
                // build address string
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    if (address.getAddressLine(i) != null) {
                        addressLine.append(address.getAddressLine(i)).append(", ");
                    }
                }
                addressLine.append(address.getLocality()).append(", ");
                addressLine.append(address.getPostalCode()).append(", ");
                addressLine.append(address.getAdminArea()).append(", ");
                addressLine.append(address.getSubAdminArea()).append(", ");
                addressLine.append(address.getCountryName());
            }
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(getString(R.string.stops_new_location_window_title))
                    .snippet(addressLine.toString())
                    .icon(BitmapDescriptorFactory.fromBitmap(imageTools.getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.map_marker), 200)));
            // Set Custom InfoWindow Adapter
            CustomInfoWindowAdapter customWinAdapter = new CustomInfoWindowAdapter(this);
            googleMap.setInfoWindowAdapter(customWinAdapter);
            // Add Marker to map and show window info
            if (marker != null) {
                marker.remove();
            }
            marker = googleMap.addMarker(markerOptions);
            marker.showInfoWindow();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));

        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onUploadPhotoBtnClick(View view) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            } else if (options[item].equals("Choose from Gallery")) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else {
                    Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            case LOCATION_PERMISSION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            locationPermissionGranted = false;
                            return;
                        }
                    }
                    locationPermissionGranted = true;
                    initializeMap();
                }
                break;
            case REQUEST_RECORD_AUDIO_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    audioRecordPermission = true;
                } else {
                    Toast.makeText(this, "Audio Recording Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Bitmap imageBitmap = null;
                if (requestCode == REQUEST_IMAGE_CAPTURE) {
                    Bundle extras = data.getExtras();
                    imageBitmap = (Bitmap) Objects.requireNonNull(extras).get("data");
                } else if (requestCode == REQUEST_IMAGE_GALLERY) {
                    Uri imageUri = data.getData();
                    imageBitmap = imageTools.getCorrectlyOrientedImage(this, imageUri, 800);
                }
                imageBitmap = imageTools.getResizedBitmap(Objects.requireNonNull(imageBitmap), 500);
                String imageKey = "stopCachedImg" + adapterGallery.getData().size();
                BitmapCache.getInstance().getLru().put(imageKey, imageBitmap);
                adapterGallery.addItem(imageBitmap);
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            Toast.makeText(this, "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
        }
    }

    public void onCreateStopBtnClick(View view) {
        stop = new Stop();
        // set attributes in Stop object
        stop.setLatitude(Float.parseFloat(etLatitude.getText().toString()));
        stop.setLongitude(Float.parseFloat(etLongitude.getText().toString()));
        stop.setStopname(Objects.requireNonNull(etStopName.getText()).toString());
        stop.setText(Objects.requireNonNull(etStopText.getText()).toString());
        // important to display the delete button in the stops adapter
        stop.setDeleteVisible(true);
        // check for audio and encoded to string if exists
        File audioToUpload = new File(Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath() + AUDIO_RECORDING_PATH);
        if (audioToUpload.exists()) {
            stop.setAudio(audioTools.encodeAudioFileToString(getExternalCacheDir().getAbsolutePath() + AUDIO_RECORDING_PATH));
            audioToUpload.delete();
        }
        // create intent and set extras
        Intent data = new Intent();
        // check for images and cache them
        for (int i = 0; i < adapterGallery.getData().size(); i++) {
            String imageKey = "stopCachedImg" + i;
            BitmapCache.getInstance().getLru().put(imageKey, adapterGallery.getItem(i));
        }
        data.putExtra("imgListSize", adapterGallery.getData().size());
        data.putExtra("stop", stop);
        data.putExtra("oldStopPosition", getIntent().getIntExtra("oldStopPosition", 0));
        setResult(RESULT_OK, data);
        finish();
    }

    /////       Lifecycle Methods       /////

    @Override
    public void onStop() {
        super.onStop();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }
        if (player != null) {
            player.release();
            player = null;
        }
        viewModel.getAudioExistsLive().setValue(audioPlayStop);
        viewModel.getImagesLive().setValue(adapterGallery.getData());
    }

    @Override
    protected void onResume() {
        createUpdateStopBtn.setEnabled(!Objects.requireNonNull(etStopName.getText()).toString().isEmpty()
                && Objects.requireNonNull(etStopName.getText()).length() <= 30
                && Objects.requireNonNull(etStopText.getText()).length() <= 800);
        super.onResume();
    }

    // custom class that implements TextWatcher to check if form is valid everytime the text changes
    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // empty method
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            createUpdateStopBtn.setEnabled(!Objects.requireNonNull(etLatitude.getText()).toString().isEmpty()
                    && !Objects.requireNonNull(etLongitude.getText()).toString().isEmpty()
                    && !Objects.requireNonNull(etStopName.getText()).toString().isEmpty()
                    && Objects.requireNonNull(etStopName.getText()).length() <= 30
                    && Objects.requireNonNull(etStopText.getText()).length() <= 800);
        }

        @Override
        public void afterTextChanged(Editable s) {
            // empty method
        }
    }

    // starts a timer to show the time elapsed when recording or playing
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            final String hms = String.format(getResources().getConfiguration().locale,
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(updatedTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(updatedTime)),
                    TimeUnit.MILLISECONDS.toSeconds(updatedTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(updatedTime)));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (tvRecordTimer != null) {
                            if (!hms.equals("10:00")) {
                                tvRecordTimer.setText(hms);
                            } else {
                                stopRecording();
                                Toast.makeText(getApplicationContext(), "You have reached the 10min limit.", Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception e) {
                        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                    }
                }
            });
        }
    }
}
