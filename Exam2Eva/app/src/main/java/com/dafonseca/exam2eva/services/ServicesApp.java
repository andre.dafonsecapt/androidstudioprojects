package com.dafonseca.exam2eva.services;

import android.app.Activity;

import com.dafonseca.exam2eva.dao.DaoApp;
import com.dafonseca.exam2eva.db.PuntuacionesSQLiteHelper;
import com.dafonseca.exam2eva.models.Socio;

import java.util.List;

public class ServicesApp {

    private DaoApp daoApp;

    public ServicesApp() {
        daoApp = new DaoApp();
    }

    public boolean addSocio(Socio s, PuntuacionesSQLiteHelper usdbh){
        return daoApp.addSocio(s, usdbh);
    }

    public List<Socio> getSocios(List<String> tipoCuotas, PuntuacionesSQLiteHelper usdbh){
        return daoApp.getSocios(tipoCuotas, usdbh);
    }

    public boolean writeSociosToXML(Activity activity) {
        return daoApp.writeSociosToXML(activity);
    }

    public boolean restaurarSociosFromXML(Activity activity) {
        return daoApp.restaurarSociosFromXML(activity);
    }
}
