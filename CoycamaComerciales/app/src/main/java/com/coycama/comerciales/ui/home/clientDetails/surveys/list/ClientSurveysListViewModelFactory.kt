package com.coycama.comerciales.ui.home.clientDetails.surveys.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.SalesRepository
import com.coycama.comerciales.data.repositories.SurveysRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.home.clientDetails.sales.ClientSalesViewModel
import com.coycama.comerciales.ui.home.clients.ClientsViewModel
import com.coycama.comerciales.ui.home.profile.ProfileViewModel
import com.coycama.comerciales.ui.login.LoginViewModel

@Suppress("UNCHECKED_CAST")
class ClientSurveysListViewModelFactory (
    private val surveysRepository: SurveysRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClientSurveysListViewModel(surveysRepository) as T
    }
}