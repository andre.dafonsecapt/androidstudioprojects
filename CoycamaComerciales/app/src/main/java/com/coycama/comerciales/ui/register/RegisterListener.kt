package com.coycama.comerciales.ui.register

import com.coycama.comerciales.data.db.entities.User

interface RegisterListener {
    fun onStarted()
    fun onSuccess(user: User)
    fun onFailure(message: String)
}