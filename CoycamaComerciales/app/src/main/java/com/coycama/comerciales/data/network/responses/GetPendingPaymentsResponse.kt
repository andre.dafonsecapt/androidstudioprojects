package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.PendingPayment

data class GetPendingPaymentsResponse(
    val code: Int,
    val records: List<PendingPayment>
)