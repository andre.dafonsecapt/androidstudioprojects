package com.dafonseca.pratica5.services;

import android.app.Activity;

import com.dafonseca.pratica5.dao.DaoApp;
import com.dafonseca.pratica5.db.PuntuacionesSQLiteHelper;
import com.dafonseca.pratica5.models.Puntuacion;
import com.dafonseca.pratica5.models.Usuario;

import java.util.List;

public class ServicesApp {

    private DaoApp daoApp;

    public ServicesApp() {
        daoApp = new DaoApp();
    }

    public List<Puntuacion> getPuntuaciones(PuntuacionesSQLiteHelper usdbh) {
        return daoApp.getPuntuaciones(usdbh);
    }

    public boolean savePuntuacion(Puntuacion p, PuntuacionesSQLiteHelper usdbh) {
        return daoApp.savePuntuacion(p, usdbh);
    }

    public boolean updatePuntuacion(Puntuacion p, PuntuacionesSQLiteHelper usdbh) {
        return daoApp.updatePuntuacion(p, usdbh);
    }

    public boolean deletePuntuacion(String puntIdClicked, PuntuacionesSQLiteHelper usdbh) {
        return daoApp.deletePuntuacion(puntIdClicked, usdbh);
    }

    public List<Usuario> getUsuariosFromXML(Activity activity) {
        return daoApp.getUsuariosFromXML(activity);
    }

    public void writeUsuariosToXML(List<Usuario> usuarios, Activity activity) {
        daoApp.writeUsuariosToXML(usuarios, activity);
    }
}
