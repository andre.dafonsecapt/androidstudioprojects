package com.dafonseca.writetofile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    private final static String NOMBRE_FICHERO = "visitantes.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText et = findViewById(R.id.etNombre);
        et.setOnKeyListener(new android.view.View.OnKeyListener() {
            public boolean onKey(View v, int keyCode,
                                 android.view.KeyEvent event) {
                // Han pulsado una tecla...
                if ((event.getAction() ==
                        android.view.KeyEvent.ACTION_DOWN) &&
                        (keyCode ==
                                android.view.KeyEvent.KEYCODE_ENTER)) {
                    // Ha sido "intro"
                    onNuevoNombre();
                    return true;
                }
                return false;
            }
        });
        // Actualizamos la lista de visitantes a partir
        // de los datos del fichero.
        actualizarVisitantes();
    }

    protected void onNuevoNombre() {
        EditText et;
        et =  findViewById(R.id.etNombre);
        String nuevoNombre;
        nuevoNombre = et.getText().toString();
        if (nuevoNombre.trim().equals("")) {
            // No hay nombre. Acabamos con un mensaje.
            muestraMensaje("Error");
            return;
        }
        try {
            // Abrimos el fichero "crudo" para escritura por el final.
            FileOutputStream fos;
            fos = openFileOutput(NOMBRE_FICHERO, Context.MODE_PRIVATE |
                    Context.MODE_APPEND);
            // Lo envolvemos en un objeto que nos facilita
            // la escritura de cadenas.
            OutputStreamWriter out;
            out = new OutputStreamWriter(fos);
            // Escribimos el nuevo nombre.
            out.write(nuevoNombre + "\n");
            // Cerramos el fichero.
            out.close();
            // Avisamos al usuario de que todo ha ido bien.
            muestraMensaje("Bienvenido");
            // Actualizamos la lista.
            actualizarVisitantes();
        }
        catch (Exception e) {
            // Algo fue mal...Avisamos.
            muestraMensaje("Error a escribir");
        }
        // Retiramos el teclado en pantalla que puede estar
        // abierto por la escritura en el edit.
        InputMethodManager imm;
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        // Damos de nuevo el foco al edit y lo vaciamos.
        et.requestFocus();
        et.setText("");
    } // onNuevoNombre

    protected void actualizarVisitantes() {
        // La cadena con todos los nombres concatenados la creamos
        // en un StringBuilder que es más óptimo que concatenar
        // cadenas directamente.
        StringBuilder strBuild = new StringBuilder();
        TextView tv;
        tv = findViewById(R.id.etVisitantes);
        try {
            String visitante;
            // Abrimos el fichero con los visitantes para lectura.
            FileInputStream fis;
            fis = openFileInput(NOMBRE_FICHERO);
            // Lo envolvemos en un scanner para que sea más
            // fácil leer líneas.
            Scanner scanner = new Scanner(fis);
            // Si tiene alguna línea...
            if (scanner.hasNextLine()) {
                // ... la metemos en la lista.
                visitante = scanner.nextLine();
                strBuild.append(visitante);
            }
            // Ahora, por cada línea
            while (scanner.hasNextLine()) {
                // ... metemos un salto de línea y la
                // línea nueva.
                visitante = scanner.nextLine();
                strBuild.append("\n" + visitante);
            } // while
            // Cerramos el fichero.
            scanner.close();
            // Metemos los nombres leídos en la etiqueta de
            // la ventana.
            tv.setText(strBuild.toString());
        } // try
        catch (Exception e) {
            // Algo fue mal :-(
            muestraMensaje("Error Visitantes");
        } // try-catch
    } // actualizarVisitantes

    protected void muestraMensaje(String mensaje) {
        Toast t = Toast.makeText(this, mensaje, Toast.LENGTH_LONG);
        t.show();
    } // muestraMensaje

}
