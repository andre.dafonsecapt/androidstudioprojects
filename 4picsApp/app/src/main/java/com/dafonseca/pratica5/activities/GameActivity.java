package com.dafonseca.pratica5.activities;

import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.ViewModelProviders;

import com.dafonseca.pratica5.R;
import com.dafonseca.pratica5.db.PuntuacionesSQLiteHelper;
import com.dafonseca.pratica5.fragments.FragmentHome;
import com.dafonseca.pratica5.fragments.FragmentImagen;
import com.dafonseca.pratica5.fragments.FragmentRecycler;
import com.dafonseca.pratica5.models.Dificultad;
import com.dafonseca.pratica5.models.Puntuacion;
import com.dafonseca.pratica5.services.ServicesApp;
import com.dafonseca.pratica5.viewmodels.GameViewModel;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameActivity extends AppCompatActivity {

    private static final int GAMESECONDARY_ACTIVITY_TAG = 1;
    Dificultad dificultadObject;
    private FragmentRecycler fragmentRecycler;
    private List<List<String>> respuestas;
    private NotificationManagerCompat notificationManager;
    private GameViewModel viewModel;
    private TextView tvGanarMensaje;
    private LocalTime tiempoInicial;
    private LocalTime tiempoFinal;
    private Button btnRegresar;
    private ServicesApp sa = new ServicesApp();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        fragmentRecycler = (FragmentRecycler) getSupportFragmentManager().findFragmentById(R.id.fragmentRecycler);

        tiempoInicial = LocalTime.now();

        viewModel = ViewModelProviders.of(this).get(GameViewModel.class);
        ImageView imageDificultad = findViewById(R.id.imageDificultad);

        crearRespuestas();

        notificationManager = NotificationManagerCompat.from(this);

        // recoge el objecto dificuldad - coloca la imagen correspondiente en el image view y la dificuldad en el viewmodel
        dificultadObject = getIntent().getParcelableExtra("dificultadObject");
        imageDificultad.setImageResource(dificultadObject.getImage());
        viewModel.setDificultad(dificultadObject.getDificultad());

        // desactiva butones de nivel
        for (int i = 0; i < viewModel.getNivel(); i++) {
            fragmentRecycler.getAdapter().disableItem(i);
        }

        // show image del primero nivel de la correspondiente dificuldad
        FragmentImagen fragmentImagen = (FragmentImagen) getSupportFragmentManager().findFragmentById(R.id.fragmentImagen);
        if (fragmentImagen != null) {
            fragmentImagen.showImage("nivel1", viewModel.getDificultad());
        }

        // find elements in view
        tvGanarMensaje = findViewById(R.id.tvGanarMensaje);
        btnRegresar = findViewById(R.id.btnRegresar);
    }

    private void crearRespuestas() {
        respuestas = new ArrayList<>();
        respuestas.add(Arrays.asList("raiz", "alas", "cola"));
        respuestas.add(Arrays.asList("oficio", "pintar", "gestos"));
        respuestas.add(Arrays.asList("circuito", "gardenia", "comparar"));
    }

    public int getNivel() {
        return viewModel.getNivel();
    }

    public int getSecondaryActivityTag() {
        return GAMESECONDARY_ACTIVITY_TAG;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String respuesta;
        String dificultad = viewModel.getDificultad();
        int dificultadInt = -1;
        switch (dificultad) {
            case "Fácil":
                dificultadInt = 0;
                break;
            case "Intermedio":
                dificultadInt = 1;
                break;
            case "Difícil":
                dificultadInt = 2;
                break;
        }
        if ((resultCode == RESULT_CANCELED)) {
            respuesta = "canceled";
        } else {
            /////////////////////////////////////////////////////// respuesta correcta logica
            respuesta = data.getStringExtra("respuesta");
            if (respuesta.equals(respuestas.get(dificultadInt).get(viewModel.getNivel()))) {
                Toast.makeText(getApplicationContext(), "Has acertado!!", Toast.LENGTH_LONG).show();
                viewModel.setNivel(viewModel.getNivel() + 1);
                fragmentRecycler.getAdapter().disableItem(data.getIntExtra("position", -1));
                ///////////////////////////////////////////////////////// se es el nivel final una mensaje en las notificaciones del mobil
                if (viewModel.getNivel() > 2) {
                    long[] pattern = {0, 100, 200, 100, 200, 100};
                    Notification noti = new NotificationCompat.Builder(GameActivity.this, FragmentHome.CHANNEL_1_ID)
                            .setSmallIcon(R.drawable.ic_smiley)
                            .setContentTitle("Enhorabuenas!!")
                            .setContentText("Has completado todos los niveles en esta dificultad!")
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setVibrate(pattern)
                            .build();

                    notificationManager.notify(1, noti);

                    // calcula segundos totais jogados e adiciona a puntacion a la DB
                    tiempoFinal = LocalTime.now();
                    long segundosJugados = ChronoUnit.SECONDS.between(tiempoInicial, tiempoFinal);
                    tvGanarMensaje.setText("Felicidades!! Has completado la dificuldad "+viewModel.getDificultad() + " en "+ segundosJugados + " segundos!");
                    btnRegresar.setVisibility(View.VISIBLE);
                    btnRegresar.setEnabled(true);
                    addPuntuacioneDB(String.valueOf(segundosJugados));
                }
            } else {
                Toast.makeText(getApplicationContext(), "Respuesta errada intenta otra vez!", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void addPuntuacioneDB(String tiempo){
        PuntuacionesSQLiteHelper usdbh = new PuntuacionesSQLiteHelper(this, "DBPuntuaciones", null, 2);
        Puntuacion p = new Puntuacion();
        String nombre = getIntent().getStringExtra("usuario");
        String dificultad = viewModel.getDificultad();
        int dificultadInt = -1;
        switch (dificultad) {
            case "Fácil":
                dificultadInt = 0;
                break;
            case "Intermedio":
                dificultadInt = 1;
                break;
            case "Difícil":
                dificultadInt = 2;
                break;
        }
        String score = String.valueOf(Math.round((100 / Integer.parseInt(tiempo)) * 100 * ((dificultadInt + 1) * 3)));
        p.setNombre(nombre);
        p.setDificultad(dificultad);
        p.setTiempo(Integer.parseInt(tiempo));
        p.setScore(Integer.parseInt(score));
        sa.savePuntuacion(p, usdbh);
    }

    public void finishActivity(View view){
        this.finish();
    }

    ///////////////////////////////////// este metodo se repite con el anterior por el caso de tablet y los fragmentos existen todos en la misma actividad
    public void onResponder(View view){
        EditText e = findViewById(R.id.etRespuesta);
        String respuesta = e.getText().toString();

        String dificultad = viewModel.getDificultad();
        int dificultadInt = -1;
        switch (dificultad) {
            case "Fácil":
                dificultadInt = 0;
                break;
            case "Intermedio":
                dificultadInt = 1;
                break;
            case "Difícil":
                dificultadInt = 2;
                break;
        }
        if (respuesta.equals(respuestas.get(dificultadInt).get(viewModel.getNivel()))) {
            /////////////////////////////////////////////////////// respuesta correcta logica
            Toast.makeText(getApplicationContext(), "Has acertado!!", Toast.LENGTH_LONG).show();
            viewModel.setNivel(viewModel.getNivel() + 1);
            fragmentRecycler.getAdapter().disableItem(viewModel.getNivel()-1);
            ///////////////////////////////////////////////////////// se es el nivel final una mensaje en las notificaciones del mobil
            if (viewModel.getNivel() > 2) {
                long[] pattern = {0, 100, 200, 100, 200, 100};
                Notification noti = new NotificationCompat.Builder(GameActivity.this, FragmentHome.CHANNEL_1_ID)
                        .setSmallIcon(R.drawable.ic_smiley)
                        .setContentTitle("Enhorabuenas!!")
                        .setContentText("Has completado todos los niveles en esta dificultad!")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setVibrate(pattern)
                        .build();

                notificationManager.notify(1, noti);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Respuesta errada intenta otra vez!", Toast.LENGTH_LONG).show();
        }
    }

}
