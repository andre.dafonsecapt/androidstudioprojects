package com.coycama.comerciales.ui.home.clientDetails

import android.view.View
import com.coycama.comerciales.R
import com.coycama.comerciales.data.models.LastData
import com.coycama.comerciales.databinding.ItemLastDataBinding
import com.xwray.groupie.viewbinding.BindableItem

// Groupie library recycler view item, it binds data to the layout, for ease of use
class LastDataItem(
    private val lastData: LastData,
    private val listener: LastDataItemListener
) : BindableItem<ItemLastDataBinding>() {

    override fun bind(viewBinding: ItemLastDataBinding, position: Int) {
        viewBinding.lastData = lastData
        viewBinding.itemLastDataRootLayout.setOnClickListener {
            listener.onLastDataItemClicked(lastData)
        }
    }

    override fun getLayout() = R.layout.item_last_data

    override fun initializeViewBinding(view: View): ItemLastDataBinding {
        return ItemLastDataBinding.bind(view)
    }


}