package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.Good

data class GetGoodsResponse(
    val code: Int,
    val msg: String?,
    val records: List<Good>
)