package com.dafonseca.exam1.activity;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.dafonseca.exam1.fragment.FragmentPedido;
import com.dafonseca.exam1.adapter.PedidoAdapter;
import com.dafonseca.exam1.R;
import com.dafonseca.exam1.models.Pedido;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PedidoAdapter.ItemClickListener {


    private static final int SECONDARY_ACTIVITY_TAG = 1;
    private PedidoAdapter adapter;
    private List<Pedido> pedidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.rvPedidos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PedidoAdapter(dataSet());
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        addPedidos();

    }

    private void addPedidos() {
        pedidos = new ArrayList<>();
        List<String> comidas1 = new ArrayList<>();
        comidas1.add("Pizza");
        comidas1.add("Tofu");
        comidas1.add("Seitan");
        Pedido pedido1 = new Pedido(15, "Terraza", "Pedro");
        pedido1.addComidas(comidas1);
        pedidos.add(pedido1);
        List<String> comidas2 = new ArrayList<>();
        comidas2.add("Salada");
        comidas2.add("Ratattoullie");
        Pedido pedido2 = new Pedido(15, "Comedor", "Carlota");
        pedido2.addComidas(comidas2);
        pedidos.add(pedido2);
    }

    private List<String> dataSet(){
        List<String> dataSet = new ArrayList<>();
        dataSet.add("Pedido 1");
        dataSet.add("Pedido 2");
        return dataSet;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void nuevoPedido(View view){
        LocalTime lt = LocalTime.now();
        Intent i = new Intent(this, SecondaryActivity.class);
        i.putExtra("fecha", lt.toString());
        startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Pedido pedido = data.getParcelableExtra("pedido");
        pedidos.add(pedido);
        adapter.addItem("Pedido "+pedidos.size());

    }

    @Override
    public void onItemClick(View view, int position) {

        FragmentPedido fragmentPedido = (FragmentPedido) getSupportFragmentManager().findFragmentById(R.id.fragmentPedido);

        if (fragmentPedido != null) {

            fragmentPedido.showPedido("Pedido "+(position+1)+":\r\n"+pedidos.get(position).toString());
        }
    }
}
