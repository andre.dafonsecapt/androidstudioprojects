package com.dafonseca.testlepaya.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.dafonseca.testlepaya.R
import com.dafonseca.testlepaya.models.Trainer

class TrainerDetailsActivity : AppCompatActivity() {

    private lateinit var trainer: Trainer
    private lateinit var detailsPicture: ImageView
    private lateinit var etName: EditText
    private lateinit var etEmail: EditText
    private lateinit var checkBoxAvailability: CheckBox
    private lateinit var etFruit: EditText
    private lateinit var etSkills: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trainer_details)

        // find all views
        findViews()

        // get trainer for parcelable in intent
        val bundle = intent.getBundleExtra("myBundle")
        trainer = bundle?.getParcelable<Trainer>("trainer") as Trainer

        // set info in the trainer details elements
        setTrainerDetails()
    }

    private fun findViews(){
        detailsPicture = findViewById(R.id.detailsPicture)
        etName = findViewById(R.id.etName)
        etEmail = findViewById(R.id.etEmail)
        checkBoxAvailability = findViewById(R.id.checkBoxAvailability)
        etFruit = findViewById(R.id.etFruit)
        etSkills = findViewById(R.id.etSkills)
    }

    private fun setTrainerDetails() {
        Glide.with(this)
            .asBitmap()
            .load(trainer.picture)
            .fitCenter()
            .override(500, 500)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(detailsPicture)
        etName.setText(trainer.name)
        etEmail.setText(trainer.email)
        checkBoxAvailability.isChecked = trainer.isAvailable
        etFruit.setText(trainer.favoriteFruit)
        etSkills.setText(trainer.skills.toString())
    }

    fun backAction(view: View?) {
        trainer.name = etName.text.toString()
        trainer.email = etEmail.text.toString()
        trainer.isAvailable = checkBoxAvailability.isChecked
        val data = Intent()
        data.putExtra("trainer", trainer)
        setResult(Activity.RESULT_OK, data)
        finish()
    }
}