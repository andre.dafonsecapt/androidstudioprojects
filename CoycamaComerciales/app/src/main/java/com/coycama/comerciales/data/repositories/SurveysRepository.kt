package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.Good
import com.coycama.comerciales.data.models.Survey
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SurveysRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val surveys = MutableLiveData<List<Survey>>()

    suspend fun getSurveysByClient(clientId: GetByClientRequest) : LiveData<List<Survey>> {
        return withContext(Dispatchers.IO){
            fetchSurveysByClient(clientId)
            surveys
        }
    }

    private suspend fun fetchSurveysByClient(clientId: GetByClientRequest){
        val response = apiRequest { api.getSurveysByClient(clientId) }
        surveys.postValue(response.records)
    }
}