package com.dafonseca.utour.retrofit;

import com.dafonseca.utour.models.UserGetDTO;
import com.dafonseca.utour.models.UserPostDTO;
import com.dafonseca.utour.utils.ConstantsAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface UserAPI {

    @POST(ConstantsAPI.URL_LOGIN)
    Call<String> login(@Body UserPostDTO user);

    @GET(ConstantsAPI.URL_MAIL)
    Call<String> resetPwd(@Query("email") String email);

    @PUT(ConstantsAPI.URL_USER)
    Call<String> changePwd(@Body UserPostDTO user);

    @GET(ConstantsAPI.URL_USER)
    Call<List<UserGetDTO>> getUsers();

    @POST(ConstantsAPI.URL_USER)
    Call<String> addUser(@Body UserPostDTO user);

    @PUT(ConstantsAPI.URL_USER)
    Call<String> updateUser(@Body UserGetDTO user);

    @DELETE(ConstantsAPI.URL_USER)
    Call<String> deleteUser(@Query("idUser") int idUser);
}
