package com.dafonseca.utour.adapters;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.dafonseca.utour.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final Activity activity;

    public CustomInfoWindowAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = activity.getLayoutInflater().inflate(R.layout.custom_info_contents, null);

        TextView tvTitle = view.findViewById(R.id.map_info_window_title);
        TextView tvSubTitle = view.findViewById(R.id.map_info_window_subtitle);

        tvTitle.setText(marker.getTitle());
        tvSubTitle.setText(marker.getSnippet());

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }
}
