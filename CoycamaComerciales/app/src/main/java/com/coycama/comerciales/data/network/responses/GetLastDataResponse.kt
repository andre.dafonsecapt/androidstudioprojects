package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.LastData

data class GetLastDataResponse(
    val code: Int,
    val records: List<LastData>
)