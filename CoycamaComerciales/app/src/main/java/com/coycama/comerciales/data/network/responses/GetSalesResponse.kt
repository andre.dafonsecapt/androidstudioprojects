package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.Sale

data class GetSalesResponse(
    val code: Int,
    val msg: String?,
    val records: List<Sale>
)