package com.dafonseca.pratica3.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.dafonseca.pratica3.R;
import com.dafonseca.pratica3.activities.GameActivity;
import com.dafonseca.pratica3.models.Dificultad;

import java.util.ArrayList;
import java.util.List;

public class FragmentHome extends Fragment {

    public static final String CHANNEL_1_ID = "channel1";
    private Spinner spinner;
    private String opcionElegida;

    public FragmentHome() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        createNotificationChannel();

        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Selecciona...");
        spinnerArray.add("Fácil (4 letras)");
        spinnerArray.add("Intermédio (6 letras)");
        spinnerArray.add("Difícil (8 letras)");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = view.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        Button btnStartGame = view.findViewById(R.id.btnStart);
        Button btnPunctuaciones = view.findViewById(R.id.btnPuntuaciones);

        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = (String) spinner.getSelectedItem();
                int image = 0;
                if (s.equals("Selecciona...")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Selecciona una dificultad primero.", Toast.LENGTH_LONG).show();
                } else {
                    switch (s) {
                        case "Fácil (4 letras)":
                            s = "facil";
                            image = R.drawable.facil;

                            break;
                        case "Intermédio (6 letras)":
                            s = "medio";
                            image = R.drawable.medio;
                            break;
                        case "Difícil (8 letras)":
                            s = "dificil";
                            image = R.drawable.dificil;
                            break;
                    }
                    Dificultad dificultad = new Dificultad(s, image);
                    Intent i = new Intent(getActivity(), GameActivity.class);
                    i.putExtra("dificultadObject", dificultad);
                    startActivity(i);
                }
            }
        });

        btnPunctuaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] items = {"Español", "Inglés", "Francés"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getLayoutInflater();
                final View inflator = inflater.inflate(R.layout.dialog_personal, null);
                builder.setView(inflator)
                        .setPositiveButton("Enviar correo", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                AlertDialog.Builder builder2 = new AlertDialog.Builder(inflator.getContext());
                                builder2.setTitle("¿En que lengua quieres el email?")
                                        .setItems(items, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int item) {
                                                opcionElegida = items[item];
                                                EditText et = inflator.findViewById(R.id.editTextEmail);
                                                Toast.makeText(getActivity().getApplicationContext(), "Un email ha sido enviado para " + et.getText() + " en " + opcionElegida, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                Dialog dialogAlert2 = builder2.create();
                                dialogAlert2.show();

                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                Dialog dialogAlert = builder.create();
                dialogAlert.show();
            }
        });

        return view;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(CHANNEL_1_ID, "Channel 1", NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription("This is channel 1");
            long[] pattern = {0, 100, 200, 100, 200, 100};
            channel1.setVibrationPattern(pattern);
            NotificationManager manager = getActivity().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
        }
    }
}
