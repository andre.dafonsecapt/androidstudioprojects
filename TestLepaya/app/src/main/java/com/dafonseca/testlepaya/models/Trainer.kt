package com.dafonseca.testlepaya.models

import android.os.Parcel
import android.os.Parcelable

class Trainer(
    val index: Int,
    val guid: String?,
    var name: String?,
    val picture: String?,
    var email: String?,
    var isAvailable: Boolean,
    val favoriteFruit: String?,
    val skills: List<String>?
) :
    Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.createStringArrayList()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(index)
        parcel.writeString(guid)
        parcel.writeString(name)
        parcel.writeString(picture)
        parcel.writeString(email)
        parcel.writeByte(if (isAvailable) 1 else 0)
        parcel.writeString(favoriteFruit)
        parcel.writeStringList(skills)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<Trainer> {
            override fun createFromParcel(parcel: Parcel): Trainer {
                return Trainer(parcel)
            }

            override fun newArray(size: Int): Array<Trainer?> {
                return arrayOfNulls(size)
            }
        }
    }
}
