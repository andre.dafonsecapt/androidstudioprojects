package com.dafonseca.comunicaciondosfragmentos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements FragmentoSender.SendMessageListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentoSender fragmentoSender = (FragmentoSender) getSupportFragmentManager().findFragmentById(R.id.fragmentSender);
        fragmentoSender.setClickListener(this);
    }


    @Override
    public void onSendMessage(String message) {
        FragmentoReceiver fragmentoReceiver = (FragmentoReceiver) getSupportFragmentManager().findFragmentById(R.id.fragmentReceiver);

        if (fragmentoReceiver != null) {

            fragmentoReceiver.showMessage(message);
        }
    }
}
