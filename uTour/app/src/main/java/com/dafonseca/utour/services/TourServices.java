package com.dafonseca.utour.services;

import com.dafonseca.utour.dao.TourDAO;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Tour;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import retrofit2.Call;
import retrofit2.Response;

public class TourServices {

    private final TourDAO dao;

    public TourServices() {
        this.dao = new TourDAO();
    }

    public Gson getGson() {
        return dao.getGson();
    }

    public Either<ApiError, List<Tour>> getMyTours(int idUser) throws IOException {
        Call<List<Tour>> call = dao.getMyTours(idUser);
        Response<List<Tour>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, List<Tour>> getAllTours(int idUser) throws IOException {
        Call<List<Tour>> call = dao.getAllTours(idUser);
        Response<List<Tour>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addTour(String tourJSON) throws IOException {
        Call<String> call = dao.addTour(tourJSON);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateTour(String tourJSON) throws IOException {
        Call<String> call = dao.updateTour(tourJSON);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> deleteTour(int idTour) throws IOException {
        Call<String> call = dao.deleteTour(idTour);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = new ApiError();
            try {
                error = dao.getGson().fromJson(Objects.requireNonNull(resp.errorBody()).charStream(), ApiError.class);
            } catch (Exception e) {
                error.setMessage("Unexpected Server Error. Please try again later.");
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return Either.left(error);
        }
    }
}
