package com.coycama.comerciales.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.R
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.databinding.ActivityLoginBinding
import com.coycama.comerciales.myFramework.ktExtensions.hide
import com.coycama.comerciales.myFramework.ktExtensions.show
import com.coycama.comerciales.myFramework.ktExtensions.toast
import com.coycama.comerciales.ui.forgotPassword.ForgotPasswordActivity
import com.coycama.comerciales.ui.home.HomeActivity
import com.coycama.comerciales.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class LoginActivity : AppCompatActivity(), LoginListener, KodeinAware {

    override val kodein by kodein()

    // lifecycle elements
    private val factory: LoginViewModelFactory by instance()
    private lateinit var viewModel: LoginViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_login
        )
        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.loginListener = this
        checkIfLoggedIn()
    }

    private fun checkIfLoggedIn() {
        viewModel.getLoggedInUser().observe(this, Observer { user ->
            if (user != null) {
                Intent(this, HomeActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
        })
    }

    override fun onStarted() {
        login_progressBar.show()
    }

    override fun onSuccess(user: User) {
        login_progressBar.hide()
        Intent(this, HomeActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(it)
        }
    }

    override fun onFailure(message: String) {
        login_progressBar.hide()
        toast(message)
    }

    override fun onRegisterClicked() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    override fun onForgotPasswordClicked() {
        startActivity(Intent(this, ForgotPasswordActivity::class.java))
    }

}