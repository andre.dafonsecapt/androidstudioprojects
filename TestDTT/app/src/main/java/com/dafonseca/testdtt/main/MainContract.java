package com.dafonseca.testdtt.main;

public interface MainContract {

    interface View {
        void showMapScreen();

        void showPrivacyAlert();

    }

    interface Presenter {
        void handleMainButton();

        void handlePrivacyInfoButton();
    }
}
