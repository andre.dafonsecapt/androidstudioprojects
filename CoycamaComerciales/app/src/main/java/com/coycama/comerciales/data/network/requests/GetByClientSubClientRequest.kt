package com.coycama.comerciales.data.network.requests

import com.google.gson.annotations.SerializedName

data class GetByClientSubClientRequest(
    @SerializedName("Cliente") var client: Int,
    @SerializedName("SubCliente") var subClient: Int
)