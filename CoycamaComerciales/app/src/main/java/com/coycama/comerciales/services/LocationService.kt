package com.coycama.comerciales.services

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_LOW
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.utils.Constants.ACTION_PAUSE_SERVICE
import com.coycama.comerciales.utils.Constants.ACTION_START_OR_RESUME_SERVICE
import com.coycama.comerciales.utils.Constants.ACTION_STOP_SERVICE
import com.coycama.comerciales.utils.Constants.NOTIFICATION_CHANNEL_ID
import com.coycama.comerciales.utils.Constants.NOTIFICATION_CHANNEL_NAME
import com.coycama.comerciales.utils.PermissionsUtility
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

// class handles background and foreground location services
class LocationService : LifecycleService() {

    var isServiceLaunched = false

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    companion object {
        val lastLocation = MutableLiveData<LatLng>()

        suspend fun getLocationFromAddress(context: Context, strAddress: String?): LatLng? {
            return withContext(Dispatchers.IO) {
                var latLng: LatLng? = null
                if (PermissionsUtility.hasLocationPermissions(context)) {
                    val coder = Geocoder(context)
                    val address: List<Address>?
                    try {
                        // May throw an IOException
                        address = coder.getFromLocationName(strAddress, 5)
                        if (address == null) {
                            return@withContext null
                        }
                        if (!address.isNotEmpty()) {
                            return@withContext null
                        }
                        val location: Address = address[0]
                        latLng = LatLng(location.latitude, location.longitude)
                    } catch (ex: IOException) {
                        ex.printStackTrace()
                    }
                }
                return@withContext latLng
            }
        }
    }

    fun postInitialValues() {
        // Madrid Lat Long
        lastLocation.postValue(LatLng(40.416775, -3.703790))
    }


    override fun onCreate() {
        super.onCreate()
        postInitialValues()
        fusedLocationProviderClient = FusedLocationProviderClient(this)
        updateLocationTracking()
    }

    // methods for changing foreground service behaviour (not being used for now)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when (it.action) {
                ACTION_START_OR_RESUME_SERVICE -> {
                    if (!isServiceLaunched) {
                        startForegroundService()
                        isServiceLaunched = true
                    } else {
                        Log.i("LOCATION_SERVICE", "Resuming service ...")
                    }
                }
                ACTION_PAUSE_SERVICE -> {
                    Log.i("LOCATION_SERVICE", "Paused service")
                }
                ACTION_STOP_SERVICE -> {
                    Log.i("LOCATION_SERVICE", "Stoped service")
                }
                else -> {
                }
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    @SuppressLint("MissingPermission")
    private fun updateLocationTracking() {
        if (PermissionsUtility.hasLocationPermissions(this)) {
            // if needed to get current location callbacks every n seconds
//            val request = LocationRequest().apply {
//                interval = LOCATION_UPDATE_INTERVAL
//                fastestInterval = FASTEST_LOCATION_INTERVAL
//                priority = PRIORITY_HIGH_ACCURACY
//            }
//            fusedLocationProviderClient.requestLocationUpdates(
//                request,
//                locationCallback,
//                Looper.getMainLooper()
//            )
            fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                lastLocation.postValue(LatLng(it.latitude, it.longitude))
            }
        } else {
            fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        }
    }

    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(result: LocationResult?) {
            super.onLocationResult(result)
            result?.locations?.let { locations ->
                for (location in locations) {
                    lastLocation.postValue(LatLng(location.latitude, location.longitude))
                }
            }
        }
    }

    // needs a notificaction build to start foreground service (not being used for now)
    private fun startForegroundService() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(notificationManager)
        }

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setAutoCancel(true)
            .setContentTitle("Coycama Localizando")


        //startForeground(NOTIFICATION_ID, notificationBuilder.build())


    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(notificationManager: NotificationManager) {
        val channel = NotificationChannel(
            NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME,
            IMPORTANCE_LOW
        )
        notificationManager.createNotificationChannel(channel)
    }
}