package com.coycama.comerciales.data.network.responses

import com.coycama.comerciales.data.models.Visit

data class GetVisitasResponse(
    val code: Int,
    val records: List<Visit>
)