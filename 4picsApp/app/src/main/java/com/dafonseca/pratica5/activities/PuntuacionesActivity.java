package com.dafonseca.pratica5.activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.pratica5.R;
import com.dafonseca.pratica5.adapters.PuntacionesAdapter;
import com.dafonseca.pratica5.db.PuntuacionesSQLiteHelper;
import com.dafonseca.pratica5.models.Puntuacion;
import com.dafonseca.pratica5.services.ServicesApp;
import com.dafonseca.pratica5.services.ServicesServidor;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PuntuacionesActivity extends AppCompatActivity implements PuntacionesAdapter.ItemClickListener {

    private EditText etPuntNombre;
    private EditText etPuntTiempo;
    private RadioGroup rgDificultad;
    private RadioButton radioButton;
    private PuntacionesAdapter rvAdapter;
    private PuntuacionesSQLiteHelper usdbh;
    private String puntIdClicked;
    private ServicesApp sa;
    private ServicesServidor ss;
    private TaskGuardarDB taskGuardarDB;
    private TaskRecuperarDB taskRecuperarDB;
    private TextView tvLoading;
    private Button btnRecuperarDB;
    private Button btnGuardarDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puntuaciones);

        etPuntNombre = findViewById(R.id.etPuntNombre);
        etPuntTiempo = findViewById(R.id.etPuntTiempo);
        rgDificultad = findViewById(R.id.rgDificultad);
        radioButton = findViewById(R.id.radioButtonFacil);
        radioButton.setChecked(true);

        usdbh = new PuntuacionesSQLiteHelper(this, "DBPuntuaciones", null, 2);

        sa = new ServicesApp();
        ss = new ServicesServidor();

        tvLoading = findViewById(R.id.tvLoading);
        btnRecuperarDB = findViewById(R.id.btnRecuperarDB);
        btnGuardarDB = findViewById(R.id.btnGuardarDB);

        setRecyclerView();

    }

    private void setRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rvPuntuaciones);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<Puntuacion> data = sa.getPuntuaciones(usdbh);
        Collections.sort(data, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return Integer.compare(((Puntuacion)o2).getScore(), ((Puntuacion)o1).getScore());
            }
        });
        rvAdapter = new PuntacionesAdapter(data);
        rvAdapter.setClickListener(this);
        recyclerView.setAdapter(rvAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {

        Puntuacion p = rvAdapter.getItem(position);
        etPuntNombre.setText(p.getNombre());
        etPuntTiempo.setText(String.valueOf(p.getTiempo()));
        switch (p.getDificultad()){
            case "Fácil":
                radioButton.setChecked(true);
                break;
            case "Intermedio":
                radioButton = findViewById(R.id.radioButtonInter);
                radioButton.setChecked(true);
                break;
            case "Difícil":
                radioButton = findViewById(R.id.radioButtonDificil);
                radioButton.setChecked(true);
                break;

        }
        puntIdClicked = String.valueOf(p.getId());

    }

    public void btnAdd(View view) {

        Puntuacion p = getPuntuacion();
        if (sa.savePuntuacion(p, usdbh)) {
            rvAdapter.addItem(p);
        }
    }

    public void btnUpdate(View view) {

        if (puntIdClicked != null) {
            Puntuacion p = getPuntuacion();
            p.setId(Integer.parseInt(puntIdClicked));
            if (sa.updatePuntuacion(p, usdbh)) {
                rvAdapter.updateItem(p);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selecciona un item de la lista primero!", Toast.LENGTH_LONG).show();
        }
    }

    private Puntuacion getPuntuacion() {
        String nombre = etPuntNombre.getText().toString();
        String tiempo = etPuntTiempo.getText().toString();
        int radioButtonID = rgDificultad.getCheckedRadioButtonId();
        RadioButton radioButton = rgDificultad.findViewById(radioButtonID);
        String dificultad = radioButton.getText().toString();
        int idx = rgDificultad.indexOfChild(radioButton);
        String score = String.valueOf(Math.round((100 / Integer.parseInt(tiempo)) * 100 * ((idx + 1) * 3)));
        Puntuacion p = new Puntuacion();
        p.setNombre(nombre);
        p.setDificultad(dificultad);
        p.setTiempo(Integer.parseInt(tiempo));
        p.setScore(Integer.parseInt(score));
        return p;
    }

    public void btnDelete(View view) {

        if (puntIdClicked != null) {
            // delete item from recycler view
            if (sa.deletePuntuacion(puntIdClicked, usdbh)) {
                rvAdapter.deleteItem(Integer.parseInt(puntIdClicked));
                puntIdClicked = null;
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selecciona un item de la lista primero!", Toast.LENGTH_LONG).show();
        }
    }

    public void btnRecuperarDB(View view) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Las puntuaciones locales serán borradas, seguro que deseas continuar?");
                alertDialogBuilder.setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                List<Puntuacion> puntuaciones = rvAdapter.getAllItems();
                                for (Puntuacion p : puntuaciones){
                                    sa.deletePuntuacion(String.valueOf(p.getId()), usdbh);
                                }
                                rvAdapter.deleteAll();
                                taskRecuperarDB = new TaskRecuperarDB();
                                taskRecuperarDB.execute();
                            }
                        });
        alertDialogBuilder.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        closeOptionsMenu();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void btnGuardarDB(View view) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Las puntuaciones remotas serán substituidas, seguro que deseas continuar?");
        alertDialogBuilder.setPositiveButton("si",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        taskGuardarDB = new TaskGuardarDB();
                        taskGuardarDB.execute();
                    }
                });
        alertDialogBuilder.setNegativeButton("no",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        closeOptionsMenu();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    class TaskGuardarDB extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            btnGuardarDB.setEnabled(false);
            tvLoading.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... voids) {
            List<Puntuacion> puntuaciones = rvAdapter.getAllItems();
            String response = "";
            try {
                response = ss.guardarPuntuaciones(puntuaciones);
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return response;
        }

        @Override
        protected void onPostExecute(String resultado) {
            btnGuardarDB.setEnabled(true);
            Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();;
            tvLoading.setVisibility(View.INVISIBLE);
        }
    }

    class TaskRecuperarDB extends AsyncTask<Void, Void, List<Puntuacion>> {

        @Override
        protected void onPreExecute() {
            btnRecuperarDB.setEnabled(false);
            tvLoading.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Puntuacion> doInBackground(Void... voids) {
            List<Puntuacion> puntuaciones = null;
            try {
                puntuaciones = ss.recuperarPuntuaciones();
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            }
            return puntuaciones;
        }

        @Override
        protected void onPostExecute(List<Puntuacion> resultado) {

            Toast.makeText(getApplicationContext(), "Puntuaciones recuperadas con exito.", Toast.LENGTH_LONG).show();
            for(Puntuacion p : resultado){
                sa.savePuntuacion(p, usdbh);
                rvAdapter.addItem(p);
            }
            btnRecuperarDB.setEnabled(true);
            tvLoading.setVisibility(View.INVISIBLE);
        }
    }
}
