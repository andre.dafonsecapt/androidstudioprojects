package com.dafonseca.storepreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    private final static String PREFERENCIA_MELODIA = "preferenciaMelodia";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Leemos las preferencias.
        SharedPreferences preferencias;
        preferencias = getPreferences(Context.MODE_PRIVATE);
        int id = preferencias.getInt(PREFERENCIA_MELODIA,
                R.id.silencio);
        RadioButton rb;
        rb = findViewById(id);
        rb.setChecked(true);

        Resources res = getResources();
        String musicFile = res.getResourceEntryName(rb.getId());

        int soundId = res.getIdentifier(musicFile, "raw", getPackageName());
        MediaPlayer melodia = MediaPlayer.create(this, soundId);
        melodia.start();

    }

    public void onAceptar(View v) {
        SharedPreferences preferencias;
        preferencias = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = preferencias.edit();
        RadioGroup rg = findViewById(R.id.preferenciasMelodia);
        editor.putInt(PREFERENCIA_MELODIA,
                rg.getCheckedRadioButtonId());
        editor.commit();
        finish();
    }
}
