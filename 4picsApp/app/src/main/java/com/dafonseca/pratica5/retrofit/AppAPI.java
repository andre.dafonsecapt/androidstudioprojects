package com.dafonseca.pratica5.retrofit;

import com.dafonseca.pratica5.models.Puntuacion;
import com.dafonseca.pratica5.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppAPI {

    @GET(Constants.URL_Puntuaciones)
    Call<List<Puntuacion>> recuperarPuntuaciones();

    @POST(Constants.URL_Puntuaciones)
    Call<String> guardarPuntuaciones(@Body List<Puntuacion> puntuaciones);


}
