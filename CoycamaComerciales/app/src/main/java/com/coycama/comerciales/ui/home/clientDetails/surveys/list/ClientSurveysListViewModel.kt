package com.coycama.comerciales.ui.home.clientDetails.surveys.list

import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import com.coycama.comerciales.data.repositories.SurveysRepository
import com.coycama.comerciales.utils.lazyDeferred

class ClientSurveysListViewModel(
    private val surveysRepository: SurveysRepository
) : ViewModel() {

    var clientId = 0

    val surveys by lazyDeferred {
        surveysRepository.getSurveysByClient(GetByClientRequest(clientId))
    }
}