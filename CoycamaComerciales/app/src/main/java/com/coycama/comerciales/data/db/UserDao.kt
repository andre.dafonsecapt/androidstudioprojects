package com.coycama.comerciales.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.coycama.comerciales.data.db.entities.CURRENT_USER_ID
import com.coycama.comerciales.data.db.entities.User

// local database DAO, only needed for storing the user locally so app remembers logged in user
@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User) : Long

    @Query("SELECT * FROM user WHERE uid = $CURRENT_USER_ID")
    fun getUser() : LiveData<User>
}