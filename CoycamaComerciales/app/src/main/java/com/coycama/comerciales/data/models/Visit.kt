package com.coycama.comerciales.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Visit(
    val Cerrada: String,
    val Cliente: String,
    val Comercial: String,
    val Fecha: String,
    val Minutos: String,
    val SubCliente: String,
    val VecesConsultaApp: String?,
    val Visita: String
): Parcelable