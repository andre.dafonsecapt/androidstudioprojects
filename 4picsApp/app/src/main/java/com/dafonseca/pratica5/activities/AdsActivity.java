package com.dafonseca.pratica5.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.pratica5.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class AdsActivity extends AppCompatActivity {

    private AdView mAdView;
    private AdView mAdView2;
    private AdView mAdView3;
    private ImageView loadingGif;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);

        loadingGif = findViewById(R.id.loadingGif);

        MobileAds.initialize(this, "1:1020422148664:android:9831231fdeddd3e6308ccd");

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

    }

    public void loadAds(View view) {
        loadingGif.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView = findViewById(R.id.adView);
        mAdView.loadAd(adRequest);

        mAdView2 = findViewById(R.id.adView2);
        mAdView2.loadAd(adRequest);

        mAdView3 = findViewById(R.id.adView3);
        mAdView3.setAdListener(new AdListener(){

            @Override
            public void onAdLoaded() {
                loadingGif.setVisibility(View.INVISIBLE);
            }
        });
        mAdView3.loadAd(adRequest);
    }

}
