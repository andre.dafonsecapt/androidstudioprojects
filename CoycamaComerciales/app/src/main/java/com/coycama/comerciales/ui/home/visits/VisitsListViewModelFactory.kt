package com.coycama.comerciales.ui.home.visits

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.ComercialRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.home.clients.ClientsViewModel
import com.coycama.comerciales.ui.home.profile.ProfileViewModel
import com.coycama.comerciales.ui.login.LoginViewModel

@Suppress("UNCHECKED_CAST")
class VisitsListViewModelFactory (
    private val userRepository : UserRepository,
    private val comercialRepository: ComercialRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return VisitsListViewModel(comercialRepository, userRepository) as T
    }
}