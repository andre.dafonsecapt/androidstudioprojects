package com.dafonseca.utour.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.dafonseca.utour.R;
import com.dafonseca.utour.activities.ProfileEditActivity;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.BitmapCache;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Profile;
import com.dafonseca.utour.services.ProfileServices;
import com.dafonseca.utour.viewmodels.MainViewModel;
import com.hbb20.CountryCodePicker;

import de.hdodenhof.circleimageview.CircleImageView;
import fj.data.Either;
import lombok.SneakyThrows;

public class ProfileSplashFragment extends Fragment {

    private ScrollView scrollView;
    private CircleImageView profileImg;
    private TextView tvName;
    private TextView tvMyTours;
    private TextView tvToursTaken;
    private TextView tvToursRated;
    private TextView tvEmail;
    private TextView tvPhone;
    private TextView tvNoNationality;
    private CountryCodePicker ccpNationality;
    private Button btnEditProfile;
    private ProfileServices profileServices;
    private Profile profile;
    private ProgressBar progressBar;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile_splash, container, false);

        setViews(rootView);
        setTitle();
        profileServices = new ProfileServices();
        getProfile();
        setListeners();
        return rootView;
    }

    private void setViews(View rootView) {
        scrollView = rootView.findViewById(R.id.profile_splash_scrollview);
        profileImg = rootView.findViewById(R.id.profile_splash_img);
        tvName = rootView.findViewById(R.id.profile_splash_name);
        tvMyTours = rootView.findViewById(R.id.profile_splash_mytours);
        tvToursTaken = rootView.findViewById(R.id.profile_splash_tourstaken);
        tvToursRated = rootView.findViewById(R.id.profile_splash_toursrated);
        tvEmail = rootView.findViewById(R.id.profile_splash_email);
        tvPhone = rootView.findViewById(R.id.profile_splash_phone);
        tvNoNationality = rootView.findViewById(R.id.profile_splash_textview_no_nationality);
        ccpNationality = rootView.findViewById(R.id.profile_splash_nationality);
        btnEditProfile = rootView.findViewById(R.id.profile_splash_btn);
        progressBar = rootView.findViewById(R.id.profile_splash_progbar);
    }

    private void setTitle() {
        MainViewModel viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        viewModel.getAppTitle().setValue(getString(R.string.title_profile));
    }

    private void getProfile() {
        GetProfileTask task = new GetProfileTask();
        task.execute();
    }

    private void setListeners() {
        btnEditProfile.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), ProfileEditActivity.class);
            i.putExtra("profile", profile);
            if (profile.getPicture() != null && !profile.getPicture().isEmpty()) {
                BitmapCache.getInstance().getLru().put("profileImg", ((BitmapDrawable) profileImg.getDrawable()).getBitmap());
                profile.setPicture(null);
            }
            startActivity(i);
        });
        ccpNationality.overrideClickListener(view -> {
            // empty method to prevent launch of country selection dialog
        });
    }

    private void setFields() {
        //set header values
        tvMyTours.setText(String.valueOf(profile.getMyTours()));
        tvToursTaken.setText(String.valueOf(profile.getToursTaken()));
        tvToursRated.setText(String.valueOf(profile.getToursRated()));
        // set profile pic
        if (profile.getPicture() != null && !profile.getPicture().isEmpty()) {
            byte[] decodedString = Base64.decode(profile.getPicture(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            profileImg.setImageBitmap(decodedByte);
            Glide.with(this).load(decodedByte).into(profileImg);
        }
        // set name
        tvName.setText(profile.getName());
        // set email
        tvEmail.setText(DataHolder.getInstance().getEmail());
        // set nationality
        if (profile.getNationality() != null && !profile.getNationality().isEmpty()) {
            ccpNationality.setCountryForNameCode(profile.getNationality());
            tvNoNationality.setVisibility(View.GONE);
            ccpNationality.setVisibility(View.VISIBLE);
        } else {
            tvNoNationality.setVisibility(View.VISIBLE);
            tvNoNationality.setText(R.string.profile_splash_editinfo);
            ccpNationality.setVisibility(View.GONE);
        }
        // set phone
        if (profile.getPhone() != null && !profile.getPhone().isEmpty()) {
            tvPhone.setText(profile.getPhone());
        } else {
            tvPhone.setText(R.string.profile_splash_editinfo);
        }
    }

    @Override
    public void onResume() {
        getProfile();
        super.onResume();
    }

    @SuppressLint("StaticFieldLeak")
    class GetProfileTask extends AsyncTask<Void, Void, Either<ApiError, Profile>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            scrollView.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, Profile> doInBackground(Void... voids) {
            return profileServices.getProfile(DataHolder.getInstance().getIdUser());
        }

        @Override
        protected void onPostExecute(Either<ApiError, Profile> result) {
            progressBar.setVisibility(View.INVISIBLE);
            scrollView.setAlpha(1f);
            if (result.isRight()) {
                profile = result.right().value();
                setFields();
            } else {
                Toast.makeText(getContext(), result.left().value().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
