package com.dafonseca.praticafragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class FragmentoImage extends Fragment {
    /* Nueva instancia del fragmento*/
    public static FragmentoImage newInstance() {
        return new FragmentoImage();
    }
    /*La interfaz del fragmento*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragmentoimage, container,false);
    }
}
