package com.dafonseca.exam1.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.exam1.R;

import java.util.List;

public class PedidoAdapter extends RecyclerView.Adapter<PedidoAdapter.ViewHolder> {

    private List<String> data;
    private ItemClickListener mClickListener;

    public PedidoAdapter(List<String>data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.recyclerview_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String pedido = data.get(position);
        holder.myTextView.setText(pedido);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tvPedido);
            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View itemView) {
                    if (mClickListener != null)
                        mClickListener.onItemClick(itemView,
                                getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    protected String getItem(int id) {
        return data.get(id);
    }

    public void addItem(String item){
        data.add(item);
        notifyDataSetChanged();
    }



}
