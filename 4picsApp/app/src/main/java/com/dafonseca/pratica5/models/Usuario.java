package com.dafonseca.pratica5.models;

import java.sql.Timestamp;
import java.util.Objects;

public class Usuario {

    private String nombre;
    private int visitas;
    private String tiempoUso;
    private Timestamp time;

    public Usuario(String nombre, int visitas, String tiempoUso) {
        this.nombre = nombre;
        this.visitas = visitas;
        this.tiempoUso = tiempoUso;
    }

    public Usuario() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getVisitas() {
        return visitas;
    }

    public void setVisitas(int visitas) {
        this.visitas = visitas;
    }

    public String getTiempoUso() {
        return tiempoUso;
    }

    public void setTiempoUso(String tiempoUso) {
        this.tiempoUso = tiempoUso;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return nombre.equals(usuario.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre);
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre +
                " - Visitas: " + visitas + '\n' +
                "Tiempo de Uso: " + tiempoUso;
    }
}
