package com.dafonseca.writetoxml;

import android.content.Context;
import android.os.Bundle;
import android.util.Xml;

import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.writetoxml.models.Employee;

import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.io.StringWriter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            // Declarar y abrir el fichero externo
            FileOutputStream fileos = this.openFileOutput("employees.xml",
                    Context.MODE_PRIVATE);

            // Crear un objeto de la clase XmlSerializer:
            XmlSerializer xmlSerializer = Xml.newSerializer();

            // Utilizaremos un objeto StringWriter para editar el serializador:
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);

            // Construir el documento

            Employee e = new Employee();
            e.setId(1);
            e.setName("André");
            e.setSalary(50000);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, "employees");
            xmlSerializer.startTag(null, "employee");
            xmlSerializer.startTag(null, "id");
            xmlSerializer.text(String.valueOf(e.getId()));
            xmlSerializer.endTag(null, "id");
            xmlSerializer.startTag(null, "name");
            xmlSerializer.text(e.getName());
            xmlSerializer.endTag(null, "name");
            xmlSerializer.startTag(null, "salary");
            xmlSerializer.text(String.valueOf(e.getSalary()));
            xmlSerializer.endTag(null, "salary");
            xmlSerializer.endTag(null, "employee");
            xmlSerializer.endTag(null, "employees");
            xmlSerializer.endDocument();

            // Escribir resultado en el fichero y cerrar stream:
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
