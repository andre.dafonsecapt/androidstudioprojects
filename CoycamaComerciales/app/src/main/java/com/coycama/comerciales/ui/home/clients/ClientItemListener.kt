package com.coycama.comerciales.ui.home.clients

import com.coycama.comerciales.data.models.Client

interface ClientItemListener {

    fun onClientsInfoLayoutClicked(client: Client)
    fun onCallClicked(client: Client)
    fun onMapClicked(client: Client)
    fun onDetailsClicked(client: Client)
}