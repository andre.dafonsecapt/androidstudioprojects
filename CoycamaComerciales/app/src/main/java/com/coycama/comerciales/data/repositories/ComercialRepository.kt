package com.coycama.comerciales.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.coycama.comerciales.data.models.Visit
import com.coycama.comerciales.data.network.MyApi
import com.coycama.comerciales.data.network.SafeApiRequest
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ComercialRepository(
    private val api: MyApi
) : SafeApiRequest() {

    private val visits = MutableLiveData<List<Visit>>()

    suspend fun getVisitsByComercial(comercialId: GetByComercialRequest): LiveData<List<Visit>> {
        return withContext(Dispatchers.IO) {
            fetchVisitsByComercial(comercialId)
            visits
        }
    }

    private suspend fun fetchVisitsByComercial(comercialId: GetByComercialRequest) {
        val response = apiRequest { api.getVisitsByComercial(comercialId) }
        visits.postValue(response.records)
    }
}