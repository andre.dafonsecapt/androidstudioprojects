package com.dafonseca.pratica5.fragments;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.dafonseca.pratica5.R;
import com.dafonseca.pratica5.activities.GameActivity;
import com.dafonseca.pratica5.activities.PuntuacionesActivity;
import com.dafonseca.pratica5.models.Dificultad;
import com.dafonseca.pratica5.models.Usuario;
import com.dafonseca.pratica5.services.ServicesApp;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class FragmentHome extends Fragment {

    public static final String CHANNEL_1_ID = "channel1";
    private static final String PREFERENCIAS_DIFICULDAD = "preferenciasDificuldad";
    private Spinner spinner;
    private EditText etNombre;
    private LocalDateTime fechaStartApp;
    private ServicesApp sa;

    public FragmentHome() {
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        createNotificationChannel();

        setSpinner(view);

        Button btnStartGame = view.findViewById(R.id.btnStart);
        Button btnPunctuaciones = view.findViewById(R.id.btnPuntuaciones);
        etNombre = view.findViewById(R.id.etNombre);

        setBtnListeners(btnStartGame, btnPunctuaciones);

        checkPreferences();

        fechaStartApp = LocalDateTime.now();

        sa = new ServicesApp();

        return view;
    }


    private void checkPreferences() {
        SharedPreferences preferencias;
        preferencias = getActivity().getPreferences(Context.MODE_PRIVATE);
        int posicionSpinner = preferencias.getInt(PREFERENCIAS_DIFICULDAD,
                0);
        spinner.setSelection(posicionSpinner);
    }

    private void setBtnListeners(Button btnStartGame, Button btnPunctuaciones) {
        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = (String) spinner.getSelectedItem();
                int image = 0;
                if (etNombre.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Introduz un Nombre primero.", Toast.LENGTH_LONG).show();
                } else if (s.equals("Selecciona...")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Selecciona una dificultad primero.", Toast.LENGTH_LONG).show();
                } else {
                    switch (s) {
                        case "Fácil (4 letras)":
                            s = "Fácil";
                            image = R.drawable.facil;

                            break;
                        case "Intermédio (6 letras)":
                            s = "Intermedio";
                            image = R.drawable.medio;
                            break;
                        case "Difícil (8 letras)":
                            s = "Difícil";
                            image = R.drawable.dificil;
                            break;
                    }

                    // creates the Dificultad Object to be passed to the Game Activity and adds a visit to the user if he exists
                    Dificultad dificultad = new Dificultad(s, image);
                    Intent i = new Intent(getActivity(), GameActivity.class);
                    i.putExtra("dificultadObject", dificultad);
                    i.putExtra("usuario", etNombre.getText().toString());
                    startActivity(i);
                }
            }
        });

        btnPunctuaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), PuntuacionesActivity.class);
                startActivity(i);
            }
        });
    }

    private void setSpinner(View view) {
        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Selecciona...");
        spinnerArray.add("Fácil (4 letras)");
        spinnerArray.add("Intermédio (6 letras)");
        spinnerArray.add("Difícil (8 letras)");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = view.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(CHANNEL_1_ID, "Channel 1", NotificationManager.IMPORTANCE_HIGH);
            channel1.setDescription("This is channel 1");
            long[] pattern = {0, 100, 200, 100, 200, 100};
            channel1.setVibrationPattern(pattern);
            NotificationManager manager = getActivity().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String calculateTimePassed(){
        LocalDateTime fechaFinishApp = LocalDateTime.now();

        LocalDateTime tempDateTime = LocalDateTime.from(fechaStartApp);

        long horas = tempDateTime.until( fechaFinishApp, ChronoUnit.HOURS );
        tempDateTime = tempDateTime.plusHours( horas );

        long minutos = tempDateTime.until( fechaFinishApp, ChronoUnit.MINUTES );
        tempDateTime = tempDateTime.plusMinutes( minutos );

        long segundos = tempDateTime.until( fechaFinishApp, ChronoUnit.SECONDS );

        StringBuffer tiempoOcurrido = new StringBuffer();

        tiempoOcurrido.append("Horas: ").append(horas);
        tiempoOcurrido.append(" - Minutos: ").append(minutos);
        tiempoOcurrido.append(" - Segundos: ").append(segundos);

        return tiempoOcurrido.toString();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String addTimePassed(String tiempoInicial){

        String[] tiempoPartes = tiempoInicial.split(" - ");
        long horas = Long.parseLong(tiempoPartes[0].substring(7));
        long minutos = Long.parseLong(tiempoPartes[1].substring(9));
        long segundos = Long.parseLong(tiempoPartes[2].substring(10));

        LocalDateTime fechaFinishApp = LocalDateTime.now();

        LocalDateTime tempDateTime = LocalDateTime.from(fechaStartApp);

        long horasDiff = tempDateTime.until( fechaFinishApp, ChronoUnit.HOURS );
        tempDateTime = tempDateTime.plusHours( horasDiff );

        long minutosDiff = tempDateTime.until( fechaFinishApp, ChronoUnit.MINUTES );
        tempDateTime = tempDateTime.plusMinutes( minutosDiff );

        long segundosDiff = tempDateTime.until( fechaFinishApp, ChronoUnit.SECONDS );

        horas += horasDiff;
        minutos += minutosDiff;
        segundos += segundosDiff;

        StringBuffer tiempoOcurrido = new StringBuffer();

        tiempoOcurrido.append("Horas: ").append(horas);
        tiempoOcurrido.append(" - Minutos: ").append(minutos);
        tiempoOcurrido.append(" - Segundos: ").append(segundos);

        return tiempoOcurrido.toString();

    }

    @Override
    public void onStop() {

        SharedPreferences preferencias;
        preferencias = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = preferencias.edit();
        editor.putInt(PREFERENCIAS_DIFICULDAD,
                spinner.getSelectedItemPosition());
        editor.commit();

        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDestroy() {

        if (getActivity().isFinishing()) {

            List<Usuario> usuarios = sa.getUsuariosFromXML(getActivity());
            Usuario usuarioApp = new Usuario();
            usuarioApp.setNombre(etNombre.getText().toString());
            // find and add a visit to the active user else create a new User
            if (!etNombre.getText().toString().equals("")) {
                String tiempoOcurrido = calculateTimePassed();
                if (usuarios.contains(usuarioApp)) {
                    for (Usuario u : usuarios) {
                        if (u.getNombre().equals(usuarioApp.getNombre())) {
                            u.setVisitas(u.getVisitas() + 1);
                            u.setTiempoUso(addTimePassed(u.getTiempoUso()));
                        }
                    }
                } else {
                    usuarioApp.setVisitas(1);
                    usuarioApp.setTiempoUso(tiempoOcurrido);
                    usuarios.add(usuarioApp);
                }
            }
            sa.writeUsuariosToXML(usuarios, getActivity());
        }

        super.onDestroy();
    }

}
