package com.dafonseca.utour.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lombok.Getter;

@Getter
public class UserViewModel extends ViewModel {

    private MutableLiveData<String> usernameLive;
    private MutableLiveData<String> emailLive;

    public UserViewModel() {
        usernameLive = new MutableLiveData<>();
        emailLive = new MutableLiveData<>();
    }
}
