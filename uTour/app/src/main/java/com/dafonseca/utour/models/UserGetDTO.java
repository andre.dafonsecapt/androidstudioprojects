package com.dafonseca.utour.models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserGetDTO implements Parcelable {

    public static final Creator<UserGetDTO> CREATOR = new Creator<UserGetDTO>() {
        @Override
        public UserGetDTO createFromParcel(Parcel in) {
            return new UserGetDTO(in);
        }

        @Override
        public UserGetDTO[] newArray(int size) {
            return new UserGetDTO[size];
        }
    };
    private int idUser;
    private String email;
    private int isActivated;
    private int isAdmin;

    private UserGetDTO(Parcel in) {
        idUser = in.readInt();
        email = in.readString();
        isActivated = in.readInt();
        isAdmin = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idUser);
        dest.writeString(email);
        dest.writeInt(isActivated);
        dest.writeInt(isAdmin);
    }
}
