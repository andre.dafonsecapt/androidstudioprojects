package com.dafonseca.testdtt.managers;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dafonseca.testdtt.R;
import com.dafonseca.testdtt.map.MapPresenter;
import com.dafonseca.testdtt.models.MyAddress;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MapManager extends AppCompatActivity implements LocationListener {

    private static final float DEFAULT_ZOOM = 15f;

    protected MapPresenter mapPresenter;
    protected Boolean locationPermissionGranted;
    protected GoogleMap mMap;
    protected Marker marker;
    protected FusedLocationProviderClient fusedLocationClient;
    protected LocationManager locationManager;
    protected boolean activityStarted;

    /////       Map Methods       //////

    protected void getDeviceLocation() {
        try {
            activityStarted = true;
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                updateLocation(location);
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.error_location_message), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });


        } catch (SecurityException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    private void updateLocation(Location location) {
        // Logic to handle location object
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        //Set Marker Options
        MyAddress currentAddress = new MyAddress();
        setMyAddressFields(latLng, currentAddress);
        String stringAddress = currentAddress.toString();
        if (marker != null) {
            marker.remove();
        }
        mapPresenter.setMarker(latLng, stringAddress, DEFAULT_ZOOM, this);
    }

    private void setMyAddressFields(LatLng latLng, MyAddress myAddress) {
        try {
            // get location using GeoCoder
            Geocoder geo = new Geocoder(this.getApplicationContext(), Locale.getDefault());
            List<Address> addressList = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addressList.size() > 0) {
                Address address = addressList.get(0);
                // build address string
                StringBuilder addressLine = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressLine.append(address.getAddressLine(i)).append(", ");
                }
                myAddress.setFirstLine(addressLine.toString());
                myAddress.setLocality(address.getLocality());
                myAddress.setPostalCode(address.getPostalCode());
                myAddress.setAdminArea(address.getAdminArea());
                myAddress.setSubAdminArea(address.getSubAdminArea());
                myAddress.setCountryName(address.getCountryName());
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // unused implemented method
    }

    @Override
    public void onProviderEnabled(String provider) {
        // unused implemented method
    }

    @Override
    public void onProviderDisabled(String provider) {
        // unused implemented method
    }
}
