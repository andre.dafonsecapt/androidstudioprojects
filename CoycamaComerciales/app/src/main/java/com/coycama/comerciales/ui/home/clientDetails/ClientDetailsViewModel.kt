package com.coycama.comerciales.ui.home.clientDetails

import android.util.Log
import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.db.entities.User
import com.coycama.comerciales.data.models.Client
import com.coycama.comerciales.data.network.requests.GetByClientRequest
import com.coycama.comerciales.data.network.requests.GetByClientSubClientRequest
import com.coycama.comerciales.data.network.requests.GetByComercialRequest
import com.coycama.comerciales.data.network.requests.SetConsultaRequest
import com.coycama.comerciales.data.repositories.ClientsRepository
import com.coycama.comerciales.data.repositories.LastDataRepository
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.utils.ApiException
import com.coycama.comerciales.utils.Constants.LAST_INCIDENTS_TABLE_NAME
import com.coycama.comerciales.utils.Constants.LAST_ORDERS_TABLE_NAME
import com.coycama.comerciales.utils.Constants.LAST_SHIPMENTS_TABLE_NAME
import com.coycama.comerciales.utils.Coroutines
import com.coycama.comerciales.utils.NoInternetException
import com.coycama.comerciales.utils.lazyDeferred

// gateway between view and repositories, loads objects only when asked for them (lazyDeferred)
class ClientDetailsViewModel(
    private val lastDataRepository: LastDataRepository
) : ViewModel() {

    var clientId = 0
    var subClientId = 0

    val lastOrders by lazyDeferred {
        lastDataRepository.getLastDataByClientSubClient(GetByClientSubClientRequest(clientId, subClientId), LAST_ORDERS_TABLE_NAME)
    }

    val lastShipments by lazyDeferred {
        lastDataRepository.getLastDataByClientSubClient(GetByClientSubClientRequest(clientId, subClientId), LAST_SHIPMENTS_TABLE_NAME)
    }

    val lastIncidents by lazyDeferred {
        lastDataRepository.getLastDataByClientSubClient(GetByClientSubClientRequest(clientId, subClientId), LAST_INCIDENTS_TABLE_NAME)
    }
}