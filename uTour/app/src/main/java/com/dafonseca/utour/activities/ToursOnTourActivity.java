package com.dafonseca.utour.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.adapters.StopAdapter;
import com.dafonseca.utour.alertDialogs.CustomFeedbackAlertDialog;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.DataHolder;
import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.models.Tour;
import com.dafonseca.utour.services.ProfileServices;
import com.dafonseca.utour.services.StopServices;
import com.dafonseca.utour.utils.ImageTools;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import fj.data.Either;
import lombok.SneakyThrows;

import static com.dafonseca.utour.utils.ConstantsClient.DEFAULT_ZOOM;
import static com.dafonseca.utour.utils.ConstantsClient.LOCATION_PERMISSION_REQUEST_CODE;

public class ToursOnTourActivity extends AppCompatActivity implements OnMapReadyCallback, StopAdapter.ItemClickListener {

    private boolean activityStarted;
    private ScrollView scrollView;
    private CardView cardViewStops;
    private List<Marker> markers;
    private StopAdapter adapterStops;
    private ProgressBar progressBarStops;
    private StopServices stopServices;
    private ProfileServices profileServices;
    private Tour tour;
    private ImageTools imageTools;
    private GoogleMap googleMap;
    private FrameLayout transparentMapFrame;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;
    private AlertDialog alertDialog;
    private NetworkReceiver networkReceiver;
    private GPSReceiver gpsReceiver;
    private boolean locationPermissionGranted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tours_ontour);
        setViews();
        initObjects();
        setListeners();
        initRecyclerView();
        executeTasks();
    }

    private void setViews() {
        scrollView = findViewById(R.id.tours_ontour_scrollview);
        transparentMapFrame = findViewById(R.id.tours_ontour_map_frame);
        cardViewStops = findViewById(R.id.tours_ontour_stops_cardview);
        progressBarStops = findViewById(R.id.tours_ontour_stops_progbar);
    }

    private void initObjects() {
        stopServices = new StopServices();
        profileServices = new ProfileServices();
        markers = new ArrayList<>();
        adapterStops = new StopAdapter(new ArrayList<>(), this);
        imageTools = new ImageTools();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        networkReceiver = new NetworkReceiver();
        gpsReceiver = new GPSReceiver();
        tour = getIntent().getParcelableExtra("tourSelected");
    }

    private void setListeners() {
        transparentMapFrame.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ScrollView to intercept touch events.
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    // Disable touch on transparent view
                    return false;

                case MotionEvent.ACTION_UP:
                    // Allow ScrollView to intercept touch events.
                    scrollView.requestDisallowInterceptTouchEvent(false);
                    return true;

                case MotionEvent.ACTION_MOVE:
                    scrollView.requestDisallowInterceptTouchEvent(true);
                    return false;

                default:
                    return true;
            }
        });
    }

    private void initRecyclerView() {
        // stops recycler view
        LinearLayoutManager stopsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView stopsRecyclerView = findViewById(R.id.tours_ontour_stops_recyclerview);
        stopsRecyclerView.setLayoutManager(stopsLayoutManager);
        stopsRecyclerView.setAdapter(adapterStops);
        adapterStops.setClickListener(this);
        adapterStops.setDrawablesEnd(true);
    }

    private void executeTasks() {
        GetStopsTask getStopsTask = new GetStopsTask();
        getStopsTask.execute();
        IncrementToursTakenTask addTourTakenTask = new IncrementToursTakenTask();
        addTourTakenTask.execute();
    }

    /////       GoogleMaps Methods       /////

    private void initializeMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.tours_ontour_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        if (locationPermissionGranted) {
            setMarkers();
            getDeviceLocation();
        }
    }

    private void setMarkers() {
        for (Stop s : adapterStops.getData()) {
            LatLng latLng = new LatLng(s.getLatitude(), s.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(s.getStopname())
                    .icon(BitmapDescriptorFactory.fromBitmap(imageTools.getResizedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.map_marker), 200)));
            Marker marker = googleMap.addMarker(markerOptions);
            markers.add(marker);
        }
    }

    private void getDeviceLocation() {
        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM));
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.error_location_message), Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (SecurityException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    /////       Permissions Methods       //////

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationPermissionGranted = true;
                initializeMap();
            } else {
                requestPermission();
            }
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        ActivityCompat.requestPermissions(this,
                permissions,
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults.length > 0) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), R.string.label_location_denied, Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
            }
            locationPermissionGranted = true;
            //initialize our map
            initializeMap();
        }
    }

    private void showEnableAlert(final String propertyToActivate) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        alertDialogBuilder.setTitle(propertyToActivate + " " + getString(R.string.alert_dialog_title));
        alertDialogBuilder.setMessage(getString(R.string.alert_dialog_part1) + " " + propertyToActivate + " " + getString(R.string.alert_dialog_part2));
        alertDialogBuilder.setPositiveButton(getString(R.string.btn_turnon), (arg0, arg1) -> {
            if (propertyToActivate.equals(getString(R.string.gps))) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            } else {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.btn_cancel), (arg0, arg1) -> finish());
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /////    Click Events Methods      //////

    @Override
    public void onItemClick(View view, int position) {
        Stop stopSelected = adapterStops.getItem(position);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(stopSelected.getLatitude(), stopSelected.getLongitude()), DEFAULT_ZOOM));
        markers.get(position).showInfoWindow();
        adapterStops.expandView(position);
    }

    public void onFeedbackBtnClick(View view) {
        CustomFeedbackAlertDialog feedbackAlertDialog = new CustomFeedbackAlertDialog(this, tour.getIdTour());
        feedbackAlertDialog.show();
    }

    /////       LifeCycle Methods       //////
    @Override
    protected void onResume() {
        //register broadcast receivers to detect any changes made
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        registerReceiver(gpsReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        super.onResume();
    }

    @Override
    protected void onStop() {
        // unregister broadcast receivers to save battery and not activate app
        unregisterReceiver(networkReceiver);
        unregisterReceiver(gpsReceiver);
        super.onStop();
    }

    /////    AsyncTasks Methods      //////

    @SuppressLint("StaticFieldLeak")
    class GetStopsTask extends AsyncTask<Void, Void, Either<ApiError, List<Stop>>> {

        @Override
        protected void onPreExecute() {
            progressBarStops.setVisibility(View.VISIBLE);
            cardViewStops.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, List<Stop>> doInBackground(Void... voids) {
            return stopServices.getStopsByIdTour(tour.getIdTour());
        }

        @Override
        protected void onPostExecute(Either<ApiError, List<Stop>> result) {
            progressBarStops.setVisibility(View.INVISIBLE);
            cardViewStops.setAlpha(1f);
            if (result.isRight()) {
                adapterStops.setItems(result.right().value());
                if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showEnableAlert(getString(R.string.gps));
                } else {
                    checkPermissions();
                }
            } else {
                Toast.makeText(getApplicationContext(), result.left().value().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class IncrementToursTakenTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            // empty method
        }

        @SneakyThrows
        @Override
        protected Void doInBackground(Void... voids) {
            return profileServices.incrementToursTaken(DataHolder.getInstance().getIdUser());
        }

        @Override
        protected void onPostExecute(Void result) {
            // empty method
        }
    }

    /////       Broadcast Listeners Classes       //////

    // receiver detects changes to network
    public class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {

                boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                if (noConnectivity) {
                    showEnableAlert(getString(R.string.network));
                } else {
                    if (alertDialog != null && alertDialog.isShowing() && activityStarted) {
                        alertDialog.cancel();
                    }
                    checkPermissions();
                }
            }
        }
    }

    // receiver detects changes to gps
    public class GPSReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(intent.getAction())) {
                if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    if (alertDialog == null || !alertDialog.isShowing()) {
                        showEnableAlert(getString(R.string.gps));
                    }
                } else {
                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.cancel();
                    }
                    checkPermissions();
                }
            }
        }
    }
}
