package com.coycama.comerciales.ui.forgotPassword

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.UserRepository
import com.coycama.comerciales.ui.register.RegisterViewModel

@Suppress("UNCHECKED_CAST")
class ForgotPasswordViewModelFactory(
    private val userRepository: UserRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ForgotPasswordViewModel(userRepository) as T
    }
}