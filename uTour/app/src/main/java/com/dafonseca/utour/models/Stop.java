package com.dafonseca.utour.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Stop implements Parcelable {

    public static final Creator<Stop> CREATOR = new Creator<Stop>() {
        @Override
        public Stop createFromParcel(Parcel in) {
            return new Stop(in);
        }

        @Override
        public Stop[] newArray(int size) {
            return new Stop[size];
        }
    };

    private List<String> images;
    private String stopname;
    private float latitude;
    private float longitude;
    private String text;
    private String audio;
    private boolean expanded;
    private boolean endDrawable;
    private boolean deleteVisible;

    public Stop() {
        images = new ArrayList<>();
        deleteVisible = false;
    }

    protected Stop(Parcel in) {
        images = in.createStringArrayList();
        stopname = in.readString();
        latitude = in.readFloat();
        longitude = in.readFloat();
        text = in.readString();
        audio = in.readString();
        deleteVisible = true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(images);
        dest.writeString(stopname);
        dest.writeFloat(latitude);
        dest.writeFloat(longitude);
        dest.writeString(text);
        dest.writeString(audio);
    }
}
