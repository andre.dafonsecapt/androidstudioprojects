package com.coycama.comerciales.ui.home.profile

import android.view.View
import androidx.lifecycle.ViewModel
import com.coycama.comerciales.data.repositories.UserRepository

class ProfileViewModel(
    repository: UserRepository
) : ViewModel() {

    val user = repository.getUser()

    var profileListener : ProfileListener? = null

    fun onClientsCardClicked(view: View) {
        profileListener?.onClientsClicked()
    }

    fun onNearbyClientsCardClicked(view: View) {
        profileListener?.onNearbyClientsClicked()
    }
}