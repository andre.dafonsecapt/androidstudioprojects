package com.dafonseca.utour.alertDialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dafonseca.utour.R;
import com.dafonseca.utour.fragments.FeedbackSearchSplashFragment;
import com.dafonseca.utour.models.ApiError;
import com.dafonseca.utour.models.Feedback;
import com.dafonseca.utour.services.FeedbackServices;

import fj.data.Either;
import lombok.SneakyThrows;

public class CustomFeedbackEditAlertDialog extends Dialog implements View.OnClickListener {

    private final FeedbackSearchSplashFragment fragment;
    private RelativeLayout relativeLayout;
    private RatingBar ratingBar;
    private EditText etOpinion;
    private Button btnCancel;
    private Button btnUpdate;
    private Button btnDelete;
    private ProgressBar progressBar;
    private FeedbackServices feedbackServices;
    private final Feedback feedbackSelected;

    public CustomFeedbackEditAlertDialog(FeedbackSearchSplashFragment fragment, Feedback feedbackSelected) {
        super(fragment.getContext());
        this.fragment = fragment;
        this.feedbackSelected = feedbackSelected;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_feedback_edit_alert_dialog);
        setViews();
        setInitFields();
        setListeners();
        feedbackServices = new FeedbackServices();
    }

    private void setViews() {
        relativeLayout = findViewById(R.id.custom_feedback_edit_alert_layout);
        ratingBar = findViewById(R.id.custom_feedback_edit_alert_ratingBar);
        etOpinion = findViewById(R.id.custom_feedback_edit_alert_opinion);
        btnCancel = findViewById(R.id.custom_feedback_edit_alert_cancel_btn);
        btnUpdate = findViewById(R.id.custom_feedback_edit_alert_update_btn);
        btnDelete = findViewById(R.id.custom_feedback_edit_alert_delete_btn);
        progressBar = findViewById(R.id.custom_feedback_edit_alert_progbar);
    }

    private void setInitFields() {
        ratingBar.setRating(feedbackSelected.getRating());
        cancel();
        etOpinion.setText(feedbackSelected.getOpinion());
    }

    private void setListeners() {
        btnCancel.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.custom_feedback_edit_alert_cancel_btn:
                dismiss();
                break;
            case R.id.custom_feedback_edit_alert_update_btn:
                feedbackSelected.setRating((int) ratingBar.getRating());
                feedbackSelected.setOpinion(etOpinion.getText().toString());
                UpdateFeedbackTask updateTask = new UpdateFeedbackTask();
                updateTask.execute();
                break;
            case R.id.custom_feedback_edit_alert_delete_btn:
                DeleteFeedbackTask deleteTask = new DeleteFeedbackTask();
                deleteTask.execute();
                break;
            default:
                break;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class UpdateFeedbackTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            relativeLayout.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return feedbackServices.updateFeedback(feedbackSelected);
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            relativeLayout.setAlpha(1f);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
                fragment.launchGetToursTask();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(fragment.getContext(), resultString, Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DeleteFeedbackTask extends AsyncTask<Void, Void, Either<ApiError, String>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            relativeLayout.setAlpha(0.5f);
        }

        @SneakyThrows
        @Override
        protected Either<ApiError, String> doInBackground(Void... voids) {
            return feedbackServices.deleteFeedback(feedbackSelected.getIdFeedback());
        }

        @Override
        protected void onPostExecute(Either<ApiError, String> result) {
            progressBar.setVisibility(View.INVISIBLE);
            relativeLayout.setAlpha(1f);
            String resultString;
            if (result.isRight()) {
                resultString = result.right().value();
                fragment.launchGetToursTask();
            } else {
                resultString = result.left().value().getMessage();
            }
            Toast.makeText(fragment.getContext(), resultString, Toast.LENGTH_SHORT).show();
            dismiss();
        }
    }
}
