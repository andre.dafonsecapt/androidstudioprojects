package com.dafonseca.testdtt.services;

import android.content.res.Configuration;

import androidx.appcompat.widget.Toolbar;

import com.dafonseca.testdtt.R;
import com.dafonseca.testdtt.managers.PermissionsManager;

public class AppServices extends PermissionsManager {

    public AppServices() {
    }

    private boolean isTablet() {
        boolean xlarge = ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        boolean large = ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    /////       Toolbar Methods       //////
    protected void setMainToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.toolbarMainTitle);
        if (!isTablet()) {
            setSupportActionBar(toolbar);
        }
    }

    protected void setMapToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.toolbarMapTitle);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_arrow);
            invalidateOptionsMenu();
        }
    }

}
