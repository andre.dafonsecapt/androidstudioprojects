package com.dafonseca.exam2eva.activities;

import android.app.Activity;
import android.app.Service;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.dafonseca.exam2eva.R;
import com.dafonseca.exam2eva.db.PuntuacionesSQLiteHelper;
import com.dafonseca.exam2eva.models.Socio;
import com.dafonseca.exam2eva.services.ServicesApp;

import java.util.ArrayList;
import java.util.List;

public class ThirdActivity extends Activity {

    private CheckBox cbAdulto;
    private CheckBox cbInfantil;
    private CheckBox cbParado;
    private CheckBox cbPensionista;
    private TextView tvList;
    private PuntuacionesSQLiteHelper usdbh;
    private ServicesApp sa = new ServicesApp();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        cbAdulto = findViewById(R.id.cbAdulto);
        cbInfantil = findViewById(R.id.cbInfantil);
        cbParado = findViewById(R.id.cbParado);
        cbPensionista = findViewById(R.id.cbPensionista);
        tvList = findViewById(R.id.tvList);

        usdbh = new PuntuacionesSQLiteHelper(this, getString(R.string.dbsocios), null, 1);

        cbAdulto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxListener();
            }
        });

        cbInfantil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxListener();
            }
        });

        cbParado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxListener();
            }
        });

        cbPensionista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxListener();
            }
        });
    }

    public void checkBoxListener(){
        List<String> tipoCuotas = new ArrayList<>();
        if (cbAdulto.isChecked()){
            tipoCuotas.add(getString(R.string.adulto));
        }
        if (cbInfantil.isChecked()){
            tipoCuotas.add(getString(R.string.infantil));
        }
        if (cbParado.isChecked()){
            tipoCuotas.add(getString(R.string.parado));
        }
        if (cbPensionista.isChecked()){
            tipoCuotas.add(getString(R.string.pensionista));
        }
        List<Socio> socios = sa.getSocios(tipoCuotas, usdbh);

        StringBuilder listSocios = new StringBuilder();
        for (Socio s : socios){
            listSocios.append(s).append("\n");
        }
        if (!tipoCuotas.isEmpty()){
            tvList.setText(listSocios.toString());
        } else {
            tvList.setText("");
        }
    }

    public void volver(View view){
        finish();
    }
}
