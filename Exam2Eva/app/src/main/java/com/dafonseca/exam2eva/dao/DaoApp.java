package com.dafonseca.exam2eva.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Xml;

import com.dafonseca.exam2eva.db.PuntuacionesSQLiteHelper;
import com.dafonseca.exam2eva.models.Socio;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoApp {

    public boolean addSocio(Socio s, PuntuacionesSQLiteHelper usdbh) {
        SQLiteDatabase db = null;
        try {
            String[] args = new String[]{s.getNombre(), s.getTipoCuota(), s.getAlergias(), s.getTelefono()};
            db = usdbh.getWritableDatabase();
            db.execSQL("INSERT INTO Socios (nombre, tipocuota, alergias, telefono) VALUES (?,?,?,?)", args);
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public boolean addAllSocios(List<Socio> socios, PuntuacionesSQLiteHelper usdbh) {
        SQLiteDatabase db = null;
        try {
            for (Socio s : socios) {
                String[] args = new String[]{s.getNombre(), s.getTipoCuota(), s.getAlergias(), s.getTelefono()};
                db = usdbh.getWritableDatabase();
                db.execSQL("INSERT INTO Socios (nombre, tipocuota, alergias, telefono) VALUES (?,?,?,?)", args);
            }
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public List<Socio> getSocios(List<String> tipoCuotas, PuntuacionesSQLiteHelper usdbh) {

        SQLiteDatabase db = usdbh.getReadableDatabase();
        StringBuilder query = new StringBuilder();
        query.append("SELECT nombre, tipocuota, alergias, telefono FROM Socios");
        for (int i = 0; i < tipoCuotas.size(); i++) {
            if (i == 0) {
                query.append(" WHERE ");
            }
            query.append("tipocuota = " + "\"" + tipoCuotas.get(i) + "\"");
            if (i != tipoCuotas.size() - 1) {
                query.append(" OR ");
            }
        }
        query.append(";");
        Cursor c = db.rawQuery(query.toString(), null);
        List<Socio> socios = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Socio s = new Socio();
                s.setNombre(c.getString(c.getColumnIndex("nombre")));
                s.setTipoCuota(c.getString(c.getColumnIndex("tipocuota")));
                s.setAlergias(c.getString(c.getColumnIndex("alergias")));
                s.setTelefono(c.getString(c.getColumnIndex("telefono")));
                socios.add(s);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return socios;
    }

    public List<Socio> getAllSocios(PuntuacionesSQLiteHelper usdbh) {
        SQLiteDatabase db = usdbh.getReadableDatabase();
        StringBuilder query = new StringBuilder();
        query.append("SELECT nombre, tipocuota, alergias, telefono FROM Socios;");
        Cursor c = db.rawQuery(query.toString(), null);
        List<Socio> socios = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Socio s = new Socio();
                s.setNombre(c.getString(c.getColumnIndex("nombre")));
                s.setTipoCuota(c.getString(c.getColumnIndex("tipocuota")));
                s.setAlergias(c.getString(c.getColumnIndex("alergias")));
                s.setTelefono(c.getString(c.getColumnIndex("telefono")));
                socios.add(s);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return socios;
    }

    public boolean deleteSocios(PuntuacionesSQLiteHelper usdbh) {

        List<Socio> socios = getAllSocios(usdbh);

        SQLiteDatabase db = null;
        try {
            for (Socio s : socios) {
                // delete Socios from db
                db = usdbh.getWritableDatabase();
                db.delete("Socios", "nombre=?", new String[]{s.getNombre()});
            }
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public boolean writeSociosToXML(Activity activity) {
        try {
            PuntuacionesSQLiteHelper usdbh = new PuntuacionesSQLiteHelper(activity.getApplicationContext(), "DBSocios", null, 1);
            // Declarar y abrir el fichero externo
            FileOutputStream fileos = activity.openFileOutput("Socios.xml",
                    Context.MODE_PRIVATE);
            // Crear un objeto de la clase XmlSerializer:
            XmlSerializer xmlSerializer = Xml.newSerializer();

            // Utilizaremos un objeto StringWriter para editar el serializador:
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);

            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, "socios");
            for (Socio s : getAllSocios(usdbh)) {
                xmlSerializer.startTag(null, "socio");
                xmlSerializer.startTag(null, "nombre");
                xmlSerializer.text(s.getNombre());
                xmlSerializer.endTag(null, "nombre");
                xmlSerializer.startTag(null, "tipocuota");
                xmlSerializer.text(s.getTipoCuota());
                xmlSerializer.endTag(null, "tipocuota");
                xmlSerializer.startTag(null, "alergias");
                xmlSerializer.text(s.getAlergias());
                xmlSerializer.endTag(null, "alergias");
                xmlSerializer.startTag(null, "telefono");
                xmlSerializer.text(s.getTelefono());
                xmlSerializer.endTag(null, "telefono");
                xmlSerializer.endTag(null, "socio");
            }
            xmlSerializer.endTag(null, "socios");
            xmlSerializer.endDocument();

            // Escribir resultado en el fichero y cerrar stream:
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            return false;
        }
        return true;
    }

    public boolean restaurarSociosFromXML(Activity activity) {
        PuntuacionesSQLiteHelper usdbh = new PuntuacionesSQLiteHelper(activity.getApplicationContext(), "DBSocios", null, 1);
        List<Socio> socios = new ArrayList<>();
        // borrar todos los socios de la DB
        if (deleteSocios(usdbh)) {
            try {
                // crear lista de socios desde XML usando XMLPullParser
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = factory.newPullParser();
                InputStream is = activity.openFileInput("Socios.xml");
                parser.setInput(is, null);

                // escribir a objectos cada elemento del XML
                Socio socioActivo = null;
                while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                    if (parser.getEventType() == XmlPullParser.START_TAG) {
                        switch (parser.getName()) {
                            case "socio":
                                socioActivo = new Socio();
                                socios.add(socioActivo);
                                break;
                            case "nombre":
                                socioActivo.setNombre(parser.nextText());
                                break;
                            case "tipocuota":
                                socioActivo.setTipoCuota(parser.nextText());
                                break;
                            case "alergias":
                                socioActivo.setAlergias(parser.nextText());
                                break;
                            case "telefono":
                                socioActivo.setTelefono(parser.nextText());
                                break;
                        }
                    }
                    parser.next();
                }
                is.close();

                // añadir socios de XML a DB
                addAllSocios(socios, usdbh);

            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                return false;
            }
        } else {
            return false;
        }
        return true;
    }


}
