package com.dafonseca.utour.retrofit;

import com.dafonseca.utour.models.Stop;
import com.dafonseca.utour.utils.ConstantsAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StopAPI {

    @GET(ConstantsAPI.URL_STOPS)
    Call<List<Stop>> getStopsByIdTour(@Query("idTour") int idTour);
}
