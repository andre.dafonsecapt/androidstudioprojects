package com.dafonseca.utour.utils;

import android.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AudioTools {

    public String encodeAudioFileToString(String filePath) {
        String base64File = "";
        File file = new File(filePath);
        try {
            FileInputStream imageInFile = new FileInputStream(file);
            // Reading a file from file system
            byte[] fileData = new byte[(int) file.length()];
            imageInFile.read(fileData);
            base64File = Base64.encodeToString(fileData, Base64.NO_WRAP);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return base64File;
    }

    public void writeAudioStringToFile(String encodedString, String pathFile) {
        try {
            FileOutputStream audioOutFile = new FileOutputStream(pathFile);
            // Converting a Base64 String into Image byte array
            byte[] imageByteArray = Base64.decode(encodedString, Base64.DEFAULT);
            audioOutFile.write(imageByteArray);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

}
