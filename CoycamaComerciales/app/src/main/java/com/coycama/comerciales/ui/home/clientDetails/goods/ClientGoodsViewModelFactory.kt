package com.coycama.comerciales.ui.home.clientDetails.goods

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.coycama.comerciales.data.repositories.GoodsRepository
import com.coycama.comerciales.data.repositories.SalesRepository

@Suppress("UNCHECKED_CAST")
class ClientGoodsViewModelFactory (
    private val goodsRepository: GoodsRepository
        ) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClientGoodsViewModel(goodsRepository) as T
    }
}