package com.dafonseca.utour.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dafonseca.utour.R;
import com.dafonseca.utour.models.UserGetDTO;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<UserGetDTO> data;
    private final Context context;
    private ItemClickListener clickListener;

    public UserAdapter(Context context) {
        this.data = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_users, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.image.setImageDrawable(context.getDrawable(R.drawable.ic_profile_person));
        holder.name.setText(data.get(position).getEmail());
        if (data.get(position).getIsAdmin() == 1) {
            holder.role.setText(R.string.row_users_role_admin);
        } else {
            holder.role.setText(R.string.row_users_role_normal);
        }
        if (data.get(position).getIsActivated() == 1) {
            holder.status.setText(R.string.row_users_status_activated);
        } else {
            holder.status.setText(R.string.row_users_status_not_active);
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public UserGetDTO getItem(int position) {
        return data.get(position);
    }

    public void setItems(List<UserGetDTO> items) {
        data = items;
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView image;
        private final TextView name;
        private final TextView role;
        private final TextView status;

        ViewHolder(final View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.row_users_image);
            name = itemView.findViewById(R.id.row_users_name);
            role = itemView.findViewById(R.id.row_users_role);
            status = itemView.findViewById(R.id.row_users_status);
            itemView.setOnClickListener(itemView1 -> {
                if (clickListener != null)
                    clickListener.onItemClick(itemView1,
                            getAdapterPosition());
            });
        }

    }
}
