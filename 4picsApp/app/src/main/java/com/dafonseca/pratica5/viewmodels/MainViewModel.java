package com.dafonseca.pratica5.viewmodels;

import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {

    private boolean appStart = true;

    public boolean isAppStart() {
        return appStart;
    }

    public void setAppStart(boolean appStart) {
        this.appStart = appStart;
    }

}
