package com.dafonseca.utour.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lombok.Getter;

@Getter
public class MainViewModel extends ViewModel {

    private MutableLiveData<String> appTitle;
    private MutableLiveData<Integer> menuImageVisibility;

    public MainViewModel() {
        appTitle = new MutableLiveData<>();
        menuImageVisibility = new MutableLiveData<>();
    }
}
